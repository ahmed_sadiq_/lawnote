//
//  Category.h
//  LawNote
//
//  Created by Apple Txlabz on 15/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseEntity.h"

@interface Category : BaseEntity


@property (nonatomic, strong) NSString * catID;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * category_description;
@property (nonatomic, strong) NSString * catImg;

@property (nonatomic, strong) NSArray * templatesArray;



- (id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapCategoryFromArray:(NSArray *) arrlist;
@end
