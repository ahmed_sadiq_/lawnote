//
//  ReceiveTemplateViewController.h
//  LawNote
//
//  Created by Samreen Noor on 03/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiveTemplateViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *templateArray;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UILabel *lblCatName;

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@end
