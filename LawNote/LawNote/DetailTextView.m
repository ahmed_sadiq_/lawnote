//
//  DetailTextView.m
//  LawNote
//
//  Created by Samreen Noor on 02/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "DetailTextView.h"
#import "Questions.h"
#import "DataManager.h"
@implementation DetailTextView
{
    Questions *question;


}

- (IBAction)btnClose:(id)sender {
    
    [self hide];
}


- (IBAction)btnDone:(id)sender {
    
    if (![_txtDetailView.text isEqualToString:@""]) {
        [question setAnswer:_txtDetailView.text];
        [question setIsFilled:YES];
        [self.delegate returnDetail:question];
        
    }
    else{
        [question setAnswer:_txtDetailView.text];
        [question setIsFilled:NO];
        [self.delegate returnDetail:question];
        
    }
    [self hide];
    
}
-(void) setupView{
    CALayer *viewLayer = self.txtDetailView.layer;
    [viewLayer setCornerRadius:5.0];
    [viewLayer setBorderWidth:1];
    viewLayer.borderColor=[[UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0]CGColor];
    _lblQuestion.text = question.question_text;
    _txtDetailView.text = question.answer;
    
    [DataManager sharedManager].selectedView = _txtDetailView;
    
}
+ (id) loadWithNibForDetailText:(Questions *)question andQuestionNumber:(NSString *)questionNumber
{
    // int device = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)?0:1;
    
    
    DetailTextView *view = (DetailTextView *)[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([DetailTextView class]) owner:nil options:nil][0];
    
    [view setFrame:[UIScreen mainScreen].bounds];
    
    view.alpha = 0.0;
    
    view->question = question;
    [view setupView];

    return view;
}

-(void) show{
    self.alpha = 1.0;

    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        
        self.boxViewTopConstrain.constant = 0;
        [self.txtDetailView becomeFirstResponder];

    
        
    } completion:^(BOOL finished) {
        
        self.alpha = 1.0;
        
        
    }];
}
-(void) hide{
    [self endEditing:YES];
    
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        self.boxViewTopConstrain.constant = 280;
        [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
            
            
        } completion:^(BOOL finished) {
            
            
            
        }];
        self.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        
        self.alpha = 0.0;
        
        
    }];
}

#pragma mark - Text VIEW Delegate Methods
-(void)textViewDidBeginEditing:(UITextView *)textView
{

}
-(void)textViewDidChange:(UITextView *)textView
{
  
}
-(void)textViewDidChangeSelection:(UITextView *)textView
{
    
    /*YOUR CODE HERE*/
}
-(void)textViewDidEndEditing:(UITextView *)textView{

    //////////////// to adjust view movement on keyboard show/////////
    UITextView *utxt = [[UITextView alloc]init];
    [utxt setTag:0];
    [DataManager sharedManager].selectedView  = utxt;

//    if (![textView.text isEqualToString:@""]) {
//        [question setAnswer:_editTxtView.text];
//        [question setIsFilled:YES];
//        [self.delegate fillQuestion:question];
//        
//    }
//    else{
//        [question setAnswer:_editTxtView.text];
//        [question setIsFilled:NO];
//        [self.delegate fillQuestion:question];
//        
//    }

    /*YOUR CODE HERE*/
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];

       
    }
   
    
    return YES;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
