//
//  UIView+AlertView.m
//  LawNote
//
//  Created by Samreen Noor on 24/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "UIView+AlertView.h"

@implementation UIView (AlertView)
- (UIView *)createDemoView:(NSString *)title message:(NSString *)message{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 100)];
    UILabel *headingView = [[UILabel alloc] initWithFrame:CGRectMake(10, 15, 270, 20)];
    UILabel *descriptionView = [[UILabel alloc] initWithFrame:CGRectMake(10, 23, 270, 90)];
    headingView.text = title;
    descriptionView.text = message;
    descriptionView.numberOfLines = 0;
    descriptionView.textColor = [UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
    headingView.textColor = [UIColor blackColor];//colorWithRed:253.0/255.0 green:156.0/255.0 blue:62.0/255.0 alpha:1.0];
    descriptionView.textAlignment = NSTextAlignmentCenter;
    headingView.textAlignment = NSTextAlignmentCenter;
    headingView.font = [UIFont fontWithName:@"PassionOne-Bold" size:16];
    descriptionView.font = [UIFont fontWithName:@"ArchivoNarrow-Bold" size:10];
    [demoView addSubview:headingView];
    [demoView addSubview:descriptionView];
    return demoView;
}

@end
