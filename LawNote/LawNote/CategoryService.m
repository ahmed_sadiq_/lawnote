//
//  CategoryService.m
//  LawNote
//
//  Created by Apple Txlabz on 18/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CategoryService.h"
#import "Category.h"
#import "Templates.h"
#import "NotificationObject.h"
@implementation CategoryService
+(void) getCategoriesWithsuccess:(serviceSuccess) success
                         failure:(serviceFailure) failure{
    User *user = [DataManager sharedManager].currentUser;
    NSDictionary *param = @{
                            @"user_id": user.userID
                            };
    [NetworkManager getURI:URL_HOME parameters:param
                   success:^(id data) {
                       
                       NSMutableArray *categoryList = [Category mapCategoryFromArray:data[@"categories"]].mutableCopy;
                       
                       NSMutableArray *notificationArray = [NotificationObject mapNotificationFromArray:data[@"notifications_data"][@"notification"]].mutableCopy;
                       
                       [DataManager sharedManager].notificationArray = notificationArray.mutableCopy;
                       NSString *receiveCount =   data[@"notifications_data"][@"received_notification_count"];
                       NSString *completeCount =   data[@"notifications_data"][@"completed_notification_count"];
                         NSString *pendingCount =   data[@"notifications_data"][@"pending_notification_count"];
                       [DataManager sharedManager].notifReceiveCount = [receiveCount intValue];
                       [DataManager sharedManager].notifCompleteCount = [completeCount intValue] + [pendingCount intValue];
                       
                       
                       [DataManager sharedManager].notificationToTalCount = [completeCount intValue] + [receiveCount intValue]+ [pendingCount intValue];
                       
                       


                       
                       success(categoryList);

                   }
                   failure:^(NSError *error) {
                       failure(error);
                   }];

}

+(void) getOnlyCategoriesWithsuccess:(serviceSuccess) success
                         failure:(serviceFailure) failure{
    User *user = [DataManager sharedManager].currentUser;
    NSDictionary *param = @{
                            @"user_id": user.userID
                            };
    [NetworkManager getURI:URL_GET_CATEGORIES parameters:param
                   success:^(id data) {
                       
                       NSMutableArray *categoryList = [Category mapCategoryFromArray:data[@"categories"]].mutableCopy;
                       success(categoryList);
                       
                   }
                   failure:^(NSError *error) {
                       failure(error);
                   }];
    
}
+(void) getTemplateWithCategoryID:(NSString *)categoryId
                          success:(serviceSuccess) success
                             failure:(serviceFailure) failure{
    User *user = [DataManager sharedManager].currentUser;
    NSDictionary *param = @{
                            @"user_id": user.userID,
                            @"category_id":categoryId
                            };
    [NetworkManager getURI:URL_GET_TEMPLATE parameters:param
                   success:^(id data) {
                       
                       NSMutableArray *templateList = [Templates mapTemplatesFromArray:data[@"templates"]].mutableCopy;
                       success(templateList);
                       
                   }
                   failure:^(NSError *error) {
                       failure(error);
                   }];
    
}
+(void) getTemplateForReceiverSignatureWithTemplateId:(NSString *)tempId
                                               userID:(NSString *)userId
                                          isCreatedBy:(NSString *)isCreated
                                          success:(serviceSuccess) success
                                         failure:(serviceFailure) failure{
    
    User *user = [DataManager sharedManager].currentUser;

    NSDictionary *param = @{
                            @"user_id": user.userID,
                            @"template_id":tempId
                            //@"is_created_by_user":isCreated
                            };
    [NetworkManager postURI:URL_GET_RECEIVER_TEMPLATE parameters:param success:^(id data) {
        
        
        Templates *template = [[Templates alloc] initWithDictionary:data[@"template"]];
        NSString *receiveCount =   data[@"count"][@"received_notification_count"];
        NSString *completeCount =   data[@"count"][@"completed_notification_count"];
        NSString *pendingCount =   data[@"count"][@"pending_notification_count"];

        [DataManager sharedManager].notifReceiveCount = [receiveCount intValue];
        [DataManager sharedManager].notifCompleteCount = [completeCount intValue]+[pendingCount intValue];
        
        
        [DataManager sharedManager].notificationToTalCount = [completeCount intValue] + [receiveCount intValue]+ [pendingCount intValue];
        
        success(template);
        
    } failure:^(NSError *error) {
        
    }];
    
}


+(void) getReceiveTemplatesWithsuccess:(serviceSuccess) success
                          failure:(serviceFailure) failure{
    User *user = [DataManager sharedManager].currentUser;
    NSDictionary *param = @{
                            @"user_id": user.userID,
                            };
    [NetworkManager getURI:URL_GET_RECEIVED parameters:param
                   success:^(id data) {
                       
                       NSMutableArray *templateList = [Templates mapTemplatesFromArray:data[@"received"]].mutableCopy;
                       success(templateList);
                       
                   }
                   failure:^(NSError *error) {
                       failure(error);
                   }];
    
}
@end
