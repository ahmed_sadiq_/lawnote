//
//  EditText.h
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Questions.h"
#import "RCEasyTipView.h"

@protocol EditTextViewDelegate

@optional
-(void) fillQuestion:(Questions *)ques;
//- delegate method;

@end
@interface EditText : UIView <UITextViewDelegate,RCEasyTipViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnInfo;
@property (weak, nonatomic) IBOutlet UIImageView *infoImg;
@property (assign)id<EditTextViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextView *editTxtView;
@property (weak, nonatomic) IBOutlet UILabel *lblCharacterCount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxViewTopConstrain;

@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
+ (id) loadWithNibForEditText:(Questions *)question andQuestionNumber:(NSString *)questionNumber;
-(void) show;

-(void) hide;

@end
