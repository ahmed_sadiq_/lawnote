//
//  QuestionHeaderCell.h
//  LawNote
//
//  Created by Samreen Noor on 23/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet UIButton *btnShowDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnInfo;

@end
