//
//  CheckBoxView.m
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CheckBoxView.h"
#import "CheckBoxCell.h"
#import "Options.h"
#import "RCEasyTipView.h"
@implementation CheckBoxView

{
    NSString *returnStr;
    NSUInteger currentSelectedIndex;
    NSMutableArray *items;
}


- (IBAction)btnChangeBGView:(id)sender {
    self.superview.layer.borderWidth = 1.5;
    self.superview.layer.borderColor = [[UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0] CGColor];
}
- (IBAction)btnInfo:(id)sender {
    
    RCEasyTipPreferences *preferences = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferences.drawing.backgroundColor = [UIColor  colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
    preferences.animating.showDuration = 1.5;
    preferences.animating.dismissDuration = 1.5;
    preferences.animating.dismissTransform = CGAffineTransformMakeTranslation(0, 100);
    preferences.animating.showInitialTransform = CGAffineTransformMakeTranslation(0, -100);
    preferences.shouldDismissOnTouchOutside = YES;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferences];
    tipView.text =_question.question_description;
    tipView.delegate = self;
    [tipView showAnimated:YES forView:sender withinSuperView:nil];
    
    [self btnChangeBGView:nil];

    
    
}

-(void) setupView{
    items=[[NSMutableArray alloc]init];
    _lblTitle.text = _question.question_text;
    
    if ([_question.question_description isEqualToString:@""]||[_question.question_description isEqual:nil]) {
        _btnInfo.hidden = YES;
        _infoImg.hidden = YES;
    }
   }
+ (id) loadWithNibForCheckBox:(Questions *)question andQuestionNumber:(NSString *)questionNumber{
   // int device = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)?0:1;
    
    
    CheckBoxView *view = (CheckBoxView *)[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CheckBoxView class]) owner:nil options:nil][0];
    
    [view setFrame:[UIScreen mainScreen].bounds];
    
    view.alpha = 0.0;
    
    view.question = question;
    view.lblQuestionCount.text = questionNumber;
    [view setupView];
    return view;
}

-(void) show{

        self.alpha = 1.0;

}

#pragma mark - TableView Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Options *option = [_question.optionArray objectAtIndex:indexPath.row];

    CGSize size = [option.option_text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    
    return 40+size.height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return _question.optionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   Options *option = [_question.optionArray objectAtIndex:indexPath.row];
    
    CheckBoxCell * cell = (CheckBoxCell *)[_tblView dequeueReusableCellWithIdentifier:@"CheckBoxCell"];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"CheckBoxCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }

    cell.lblDes.text = option.option_text;
   [cell.btnCheck addTarget:self action:@selector(selectedOption:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnCheck.tag = indexPath.row;
    if (option.isSelected) {
        [cell.btnCheck setBackgroundImage:[UIImage imageNamed:@"Checked Checkbox"] forState:UIControlStateNormal];
        
    }
    else{
        [cell.btnCheck setBackgroundImage:[UIImage imageNamed:@"Unchecked Checkbox"] forState:UIControlStateNormal];
        
    }
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void) selectedOption : (UIButton *) sender{
    [self btnChangeBGView:nil];
    currentSelectedIndex = sender.tag;
    Options *option = [_question.optionArray objectAtIndex:currentSelectedIndex];
    if (option.isSelected==NO) {
        option.isSelected = YES;
        NSString *oId=option.option_id;
        [items addObject:oId];
        [_question setCheckeBoxOptionAnswerIdArray:items.copy];
    }
    else{

        for (int i=0;i<items.count;i++) {
            if ([option.option_id isEqualToString:items[i]]) {
                [items removeObjectAtIndex:i];

            }
        }
        option.isSelected = NO;
        [_question setCheckeBoxOptionAnswerIdArray:items.copy];
    }
//    for (int i=0;i< _question.optionArray.count;i++) {
//        Options *o=_question.optionArray[i];
//
//        if ( [items[i] isEqualToString:o.option_id]) {
//            o.isSelected = YES;
//            
//        }
//    }
    [_tblView reloadData];
   // [_question setOptionAnswerValue:option.option_value];
    NSString *answesStr = [items componentsJoinedByString:@","];
    if (_question.checkeBoxOptionAnswerIdArray.count>0) {
        [_question setIsFilled:YES];

    }
    else{
        [_question setIsFilled:NO];

    }
    [_question setAnswer:answesStr];

    [_question setOptionAnswerId:answesStr];

    [self.delegate filledCheckBox:_question];
}

@end
