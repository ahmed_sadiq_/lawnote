//
//  SettingViewController.m
//  LawNote
//
//  Created by Samreen Noor on 07/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "SettingViewController.h"
#import "DataManager.h"
@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];

}
- (IBAction)btnEditProfile:(id)sender {
    [DataManager sharedManager].ischnagePasswordView = NO;
    [self performSegueWithIdentifier:@"profile" sender:nil];

}
- (IBAction)btnChangePassword:(id)sender {
    [DataManager sharedManager].ischnagePasswordView = YES;

    [self performSegueWithIdentifier:@"profile" sender:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
