//
//  CatalogCell.h
//  LawNote
//
//  Created by Apple Txlabz on 14/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UIImageView *catImg;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@end
