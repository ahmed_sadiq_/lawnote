//
//  AppDelegate.m
//  LawNote
//
//  Created by Ahmed Sadiq on 02/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AppDelegate.h"
#import "Branch.h"
#import "Templates.h"
#import "Alert.h"
#import "NavigationHandler.h"
#import "DataManager.h"
#import "NotificationObject.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

static NSString *_deviceToken = NULL;

@interface AppDelegate ()
{
    Alert *alert;

}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
        [Fabric with:@[[Crashlytics class]]];
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
        if (!error && params) {
            
            /*
             ######### INSTRUCTIONS FOR DEEPLINKING #############
             
             // Parse "Params here, get all keys that are related to video model. If keys exist than create video model and do this [[NavigationHandler getInstance] MoveToLikeBeam:videomodel];"
             */
            
            if (params[@"template_id"] && params[@"user_id"]&& params[@"is_created_by_user"]) {
                NSString *templateId = [NSString stringWithFormat:@"%@",params[@"template_id"]];
                NSString *userId = [NSString stringWithFormat:@"%@",params[@"user_id"]];
                NSString *isCreatedByUser = [NSString stringWithFormat:@"%@",params[@"is_created_by_user"]];
                NSString *tempUrl = [NSString stringWithFormat:@"%@",params[@"template_url"]];
                NSString *tempviewUrl = [NSString stringWithFormat:@"%@",params[@"template_url_view"]];

                
                Templates *temp = [[Templates alloc]init];
                [temp setIsReceiver:YES];
                [temp setTemplate_id:templateId];
                [temp setUserId:userId];
                [temp setIs_created_by_user:isCreatedByUser];
                [temp setTemplate_url:tempUrl];
                [temp setTemplate_url_view:tempviewUrl];

               [DataManager sharedManager].receiverTemplate = temp;
                BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"];

                if (isLogin) {
                    if ([DataManager sharedManager].receiverTemplate.isReceiver) {
                        UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UITabBarController *rootViewController1 = [storyboard1 instantiateViewControllerWithIdentifier:@"navHome"];
                        [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController1];
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"navReciverTemplate"];
                        [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
                    }

                }
                
            }
            
            
            
            NSLog(@"params: %@", params);
        }
    }];

    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }
    }
    NSDictionary *tempInfo = [launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
    if (tempInfo && application.applicationState != UIApplicationStateActive) {
       // NSString *type = [NSString stringWithFormat:@"%@", [tempInfo objectForKey:@"flag"]];
            NotificationObject *template = [[NotificationObject alloc] initWithDictionary:tempInfo];
            [DataManager sharedManager].isChangeInReceived = NO;

        NSString *receiveCount =   tempInfo[@"count"][@"received_notification_count"];
        NSString *completeCount =   tempInfo[@"count"][@"completed_notification_count"];
        [DataManager sharedManager].notifReceiveCount = [receiveCount intValue];
        [DataManager sharedManager].notifCompleteCount = [completeCount intValue];
        
        
        [DataManager sharedManager].notificationToTalCount = [completeCount intValue] + [receiveCount intValue];
        
            [[NavigationHandler getInstance] addReceivedTemplate:template];
            
            

    }
    self.restrictRotation = @"0";

    // Override point for customization after application launch.
    return YES;
}


+(NSString *)getDeviceToken{
    
    return _deviceToken;
}
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    if (notificationSettings.types != UIUserNotificationTypeNone) {
        
        [application registerForRemoteNotifications];
    }
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *deviceToken1 = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceToken1 = [deviceToken1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    _deviceToken = deviceToken1;
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Did Fail to Register for Remote Notifications");
    NSLog(@"%@, %@", error, error.localizedDescription);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)tempInfo {
    NSLog(@"Push received: %@", tempInfo);
    NSString *message = [tempInfo objectForKey:@"message"];
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateInactive) {
            NotificationObject *notiftemplate = [[NotificationObject alloc] initWithDictionary:tempInfo];

            [DataManager sharedManager].isChangeInReceived = YES;

        NSString *receiveCount =   tempInfo[@"count"][@"received_notification_count"];
        NSString *completeCount =   tempInfo[@"count"][@"completed_notification_count"];
        NSString *pendingCount =   tempInfo[@"count"][@"pending_notification_count"];

        [DataManager sharedManager].notifReceiveCount = [receiveCount intValue];
        [DataManager sharedManager].notifCompleteCount = [completeCount intValue]+[pendingCount intValue];
        
        
        [DataManager sharedManager].notificationToTalCount = [completeCount intValue] + [receiveCount intValue]+[pendingCount intValue];
        [[NavigationHandler getInstance] addReceivedTemplate:notiftemplate];

            [self showAlert:message];

        }
    
    else{
        
        NotificationObject *notiftemplate = [[NotificationObject alloc] initWithDictionary:tempInfo];

            [DataManager sharedManager].isChangeInReceived = YES;


        NSString *receiveCount =   tempInfo[@"count"][@"received_notification_count"];
        NSString *completeCount =   tempInfo[@"count"][@"completed_notification_count"];
        NSString *pendingCount =   tempInfo[@"count"][@"pending_notification_count"];
        [DataManager sharedManager].notifReceiveCount = [receiveCount intValue];
        [DataManager sharedManager].notifCompleteCount = [completeCount intValue] + [pendingCount intValue] ;
        
        
        [DataManager sharedManager].notificationToTalCount = [completeCount intValue] + [receiveCount intValue]+  [pendingCount intValue];
        [[NavigationHandler getInstance] addReceivedTemplate:notiftemplate];

       [self showAlert:message];

    }
}
-(void) showAlert:(NSString *)message{
    alert = [[Alert alloc] initWithTitle:message duration:(float)2.0f completion:^{
        //
    }];
    [alert setDelegate:self];
    [alert setShowStatusBar:YES];
    [alert setAlertType:AlertTypeSuccess];
    [alert setIncomingTransition:AlertIncomingTransitionTypeSlideFromTop];
    [alert setOutgoingTransition:AlertOutgoingTransitionTypeSlideToTop];
    [alert setBounces:YES];
    [alert showAlert];
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if([self.restrictRotation isEqualToString:@"0"]){
        return UIInterfaceOrientationMaskPortrait;
    }
    else  if([self.restrictRotation isEqualToString:@"1"])
    {
        return UIInterfaceOrientationMaskLandscapeRight;
    }
    else{
        return UIInterfaceOrientationMaskAll;

    }
    
}

@end
