//
//  DateView.h
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Questions.h"

@protocol DateViewDelegate

@optional
-(void) selectedDate:(Questions *)question;
//- delegate method;

@end
@interface DateView : UIView<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnInfo;
@property (weak, nonatomic) IBOutlet UIImageView *infoImg;
@property (weak, nonatomic) IBOutlet UITextField *tfDate;
@property (strong, nonatomic) UIDatePicker *dataPicker;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestionCount;

@property (assign)id<DateViewDelegate> delegate;

+ (id) loadWithNibForDate:(Questions *)question andQuestionNumber:(NSString *)questionNumber;
-(void) show;

-(void) hide;
@end
