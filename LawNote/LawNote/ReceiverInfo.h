//
//  ReceiverInfo.h
//  LawNote
//
//  Created by Samreen on 02/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReceiverInfo.h"
#import "PPSSignatureView.h"

@protocol ReceiverInfoDelegate

@optional
//- delegate method;
-(void) userInfo:(NSArray *)infoArray withSignature:(UIImage *)signImg;
-(void) cancelUserInfo:(NSMutableArray *)infoArray;
@end
@interface ReceiverInfo : UIView<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

{
    int count;
    NSMutableArray *infoArray;
    BOOL isWithSign;
}
@property (assign)id<ReceiverInfoDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *signatureBox;
@property (weak, nonatomic) IBOutlet UIView *receiverInfoBox;

@property (weak, nonatomic) IBOutlet PPSSignatureView *signatureView;
@property (weak, nonatomic) IBOutlet UITableView *tblView;

+ (id) loadWithReceiverInfoViewWithSignature:(BOOL)withSIgn andRecInfoArray:(NSMutableArray *)recInfoArray;
-(void) show;

-(void) hide;
@end
