//
//  WalkThroughVC.m
//  LawNote
//
//  Created by Apple Txlabz on 03/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "WalkThroughVC.h"
#import "SwipeView.h"
#import "CustomOverlayView.h"
#import "POPPropertyAnimation.h"
#import "CategoryService.h"
#import "HomeViewController.h"
@interface WalkThroughVC () <SwipeDelegate,SwipeViewDataSource>
@property (weak, nonatomic) IBOutlet SwipeView *swipeView;
@property (assign, nonatomic) NSInteger count;


@end

@implementation WalkThroughVC

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
//    if ([segue.identifier isEqualToString:@"home" ]) {
//        HomeViewController  *homeVC = segue.destinationViewController;
//        homeVC.categoryArray = sender;
//        
//    }
    
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.swipeView.layer.cornerRadius = 5.0;
    self.swipeView.layer.masksToBounds = true;
    self.count = 5;
    self.swipeView.dataSource = self;
    self.swipeView.delegate = self;
    _pager.numberOfPages = 5;
    _pager.currentPage = 0;
    
    [self.customPageControl setPageIndicatorImage:[UIImage imageNamed:@"pageController_unselected"]];
    [self.customPageControl setCurrentPageIndicatorImage:[UIImage imageNamed:@"pageController_selected"]];
    
    self.customPageControl.numberOfPages = 5;
    self.customPageControl.currentPage = 0;
    
    [self.customPageControl addTarget:self action:@selector(spacePageControl:) forControlEvents:UIControlEventValueChanged];

}
- (void)spacePageControl:(SMPageControl *)sender
{
    NSLog(@"Current Page (SMPageControl): %li", (long)sender.currentPage);
}
- (void)viewDidLayoutSubviews{
    self.swipeView.hidden = false;
    [self.swipeView reloadData];
}

- (IBAction)btnCross:(id)sender {
    [self performSegueWithIdentifier:@"login" sender:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSUInteger)swipeViewNumberOfCards:(SwipeView *)swipeView
{
    return self.count;
}

- (UIView *)swipeView:(SwipeView *)swipeView
          cardAtIndex:(NSUInteger)index
{
      NSString *imageName = [NSString stringWithFormat:@"Rectangle"];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,swipeView.frame.size.width,swipeView.frame.size.height)];
    view.backgroundColor = [UIColor clearColor];

    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    imageView.frame = CGRectMake(0,0,swipeView.frame.size.width,view.frame.size.height-100);
    NSString *imgIcon = [NSString stringWithFormat:@"tick"];
    CGSize size = imageView.frame.size;

    UIImageView *iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgIcon]];
    [iconImageView setCenter:CGPointMake(size.width/2, size.height/2)];
        UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0,imageView.frame.size.height,swipeView.frame.size.width,100)];
    view2.backgroundColor = [UIColor whiteColor];
    CGSize size2 = view2.frame.size;
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 200, 200)];
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    lbl.numberOfLines = 2;
    lbl.textAlignment = NSTextAlignmentCenter;
    [lbl setFont:[UIFont fontWithName:@"Avenir" size:15.0f]];
    
    lbl.textColor=[UIColor colorWithRed:0.278 green:0.278 blue:0.278 alpha:1.0];
    lbl.userInteractionEnabled=NO;
   [lbl setCenter:CGPointMake(size2.width/2, size2.height/2)];
    
    lbl.text= @"Get your contracts done in no minute...";

    [view addSubview:imageView];
    [view addSubview:iconImageView];
   [view2 addSubview:lbl];
    [view addSubview:view2];

    return view;
}

- (OverlayView *)swipeView:(SwipeView *)swipeView
        cardOverlayAtIndex:(NSUInteger)index
{
    CustomOverlayView *overlay = [[NSBundle mainBundle] loadNibNamed:@"OverlayView" owner:self options:nil][0];
    return overlay;
}

- (void)swipeView:(SwipeView *)swipeView didSwipeCardAtIndex:(NSUInteger)index inDirection:(SwipeDirection)direction
{
    _pager.currentPage = index+1;
    _customPageControl.currentPage = index+1;
    if (index >= 4) {
   
            [self performSegueWithIdentifier:@"login" sender:nil];
    }
}

- (void)swipeViewDidRunOutOfCards:(SwipeView *)swipeView
{
    [swipeView resetCurrentCardNumber];
}

- (void)swipeView:(SwipeView *)swipeView didSelectCardAtIndex:(NSUInteger)index
{
    NSLog(@"点击");
}

- (BOOL)swipeViewShouldApplyAppearAnimation:(SwipeView *)swipeView
{
    return YES;
}

- (BOOL)swipeViewShouldMoveBackgroundCard:(SwipeView *)swipeView
{
    return YES;
}

- (BOOL)swipeViewShouldTransparentizeNextCard:(SwipeView *)swipeView
{
    return YES;
}

- (POPPropertyAnimation *)swipeViewBackgroundCardAnimation:(SwipeView *)swipeView
{
    return nil;
}

@end
