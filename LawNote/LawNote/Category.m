//
//  Category.m
//  LawNote
//
//  Created by Apple Txlabz on 15/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Category.h"
#import "Templates.h"
@implementation Category
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.catID = [NSString stringWithFormat:@"%@",[self validStringForObject:responseData[@"category_id"]]];
        self.title = [self validStringForObject:responseData[@"category_title"]];
        self.category_description =[self validStringForObject:responseData[@"category_description"]];
        self.catImg = [self completeImageURL:responseData[@"category_image"]];
        self.templatesArray = [Templates mapTemplatesFromArray:responseData[@"templates"]];

    }
    
    return self;
}




+ (NSArray *)mapCategoryFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Category alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;

}


@end
