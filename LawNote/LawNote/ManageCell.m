//
//  ManageCell.m
//  LawNote
//
//  Created by Samreen Noor on 03/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ManageCell.h"

@implementation ManageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
