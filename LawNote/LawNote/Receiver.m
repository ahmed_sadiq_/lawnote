//
//  Receiver.m
//  LawNote
//
//  Created by Samreen on 02/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "Receiver.h"

@implementation Receiver
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
     
        self.fName = [self validStringForObject:responseData[@"receiver_first_name"]];
        self.lName =[self validStringForObject:responseData[@"receiver_last_name"]];
        self.email = [self validStringForObject:responseData[@"receiver_email"]];
        self.receiver_signature_image =[self validStringForObject:responseData[@"receiver_signature_image"]];

        
        
        
    }
    
    return self;
}




+ (NSArray *)mapReceiverInfoFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Receiver alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}

@end
