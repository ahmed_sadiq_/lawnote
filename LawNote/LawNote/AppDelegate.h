//
//  AppDelegate.h
//  LawNote
//
//  Created by Ahmed Sadiq on 02/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) NSString *restrictRotation;

@property (strong, nonatomic) UIWindow *window;
+(NSString *)getDeviceToken;

@end

