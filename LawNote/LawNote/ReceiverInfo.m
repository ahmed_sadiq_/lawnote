//
//  ReceiverInfo.m
//  LawNote
//
//  Created by Samreen on 02/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "ReceiverInfo.h"
#import "ReceivedInfoCell.h"
#import "Receiver.h"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"
#import "NavigationHandler.h"

@implementation ReceiverInfo

-(void) setUpView{
    Receiver *rec = [[Receiver alloc]init];
    if (infoArray.count == 0) {
        rec.fName = @"";
        rec.lName = @"";
        rec.email = @"";
        [infoArray addObject:rec];

    }
    [_tblView reloadData];
    if (isWithSign) {
        _signatureBox.hidden = NO;
        _receiverInfoBox.hidden =  YES;
    }
    else
    {
        _signatureBox.hidden = YES;
        _receiverInfoBox.hidden =  NO;
    }
    if (isWithSign) {
        [[NavigationHandler getInstance] rotateScreenInLandScape];

    }

}
+ (id) loadWithReceiverInfoViewWithSignature:(BOOL)withSIgn andRecInfoArray:(NSMutableArray *)recInfoArray
{
    // int device = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)?0:1;
    
    
    ReceiverInfo *view = (ReceiverInfo *)[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ReceiverInfo class]) owner:nil options:nil][0];
    
    [view setFrame:[UIScreen mainScreen].bounds];
    
    view.alpha = 0.0;
    view->isWithSign = withSIgn;
     view->infoArray  = [[NSMutableArray alloc]init];
    if (recInfoArray.count>0) {
        view->infoArray = recInfoArray.mutableCopy;

    }
    [view setUpView];
    
    return view;
}

-(void) show{
    
    self.alpha = 1.0;
 
}
-(void) hide{
    [self endEditing:YES];
    
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
      
        
    } completion:^(BOOL finished) {
        
        self.alpha = 0.0;
        
        
    }];
}

-(BOOL) checkDataIsValid{
    BOOL isValid = NO;
    for (Receiver *opj in infoArray) {
        if (![opj.fName isEqualToString:@""]&& opj.fName != nil && ![opj.lName isEqualToString:@""]&& opj.lName != nil && [self emailValidate: opj.email]) {
            isValid = YES;
        }
        else{
         isValid = NO;
            break;
        }
    }
    return isValid;
}


- (IBAction)btnClear:(id)sender {
    [_signatureView erase];
}
- (IBAction)btnSaveSignature:(id)sender {
    if ( _signatureView.signatureImage != nil) {
        _signatureBox.hidden = YES;
        _receiverInfoBox.hidden = NO;
        [[NavigationHandler getInstance] restrictRotation:@"2"];
        [[NavigationHandler getInstance] rotateScreenPortrait];
        [[NavigationHandler getInstance] restrictRotation:@"0"];


    }
    
    
}

- (IBAction)btnCancel:(id)sender {
    [self endEditing:YES];

    [[NavigationHandler getInstance] restrictRotation:@"2"];
    [[NavigationHandler getInstance] rotateScreenPortrait];

    for (int i=0;i< infoArray.count ; i++) {
        Receiver *rec = infoArray[i];
        if ([rec.lName isEqualToString:@""] && [rec.fName isEqualToString:@""] && [rec.email isEqualToString:@""]) {
            [infoArray removeObjectAtIndex:i];
        }
    }
    [self.delegate cancelUserInfo:infoArray];
    [self hide];
  
    [[NavigationHandler getInstance] restrictRotation:@"0"];




}

- (IBAction)btnSend:(id)sender {
    
    if ([self checkDataIsValid]) {

        [self.delegate userInfo:infoArray withSignature:_signatureView.signatureImage];
        [self hide];
    }
    else{
        [self showAlertMessage:@"Please enter data carefully"];
    }
  
  

}

- (IBAction)btnAddUser:(id)sender {
    if (infoArray.count < 5) {

    Receiver *rec = [[Receiver alloc]init];
        rec.fName = @"";
        rec.lName = @"";
        rec.email = @"";
    [infoArray addObject:rec];
    [_tblView reloadData];
    }
}
- (IBAction)btnRemove:(UIButton*)sender {
    if (infoArray.count !=1) {
        [infoArray removeObjectAtIndex:sender.tag];
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblView];

        NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint:buttonPosition];
        [_tblView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                         withRowAnimation:UITableViewRowAnimationFade];
        [_tblView reloadData];
    }
    
}



#pragma mark - TableView Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 171;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return infoArray.count;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Receiver *obj = [infoArray objectAtIndex:indexPath.row];

    ReceivedInfoCell * cell;
    if (indexPath.row ==  0) {
         cell = (ReceivedInfoCell *)[_tblView dequeueReusableCellWithIdentifier:@"cell"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ReceivedInfoCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        

    }
    else{
       cell = (ReceivedInfoCell *)[_tblView dequeueReusableCellWithIdentifier:@"cell2"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"ReceivedInfoCell" owner:self options:nil];
            cell=[nib objectAtIndex:1];
        }
        

    }
    cell.btnRemove.tag = indexPath.row;
    [cell.btnRemove addTarget:self action:@selector(btnRemove:) forControlEvents:UIControlEventTouchUpInside];
    cell.tfFname.tag = 1;
    cell.tfLname.tag = 2;
    cell.tfEmail.tag = 3;
    cell.tfFname.text = obj.fName;
    cell.tfLname.text = obj.lName;
    cell.tfEmail.text = obj.email;
    cell.tag = indexPath.row;

    
    return cell;

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma  mark - TextField Delegates


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    UIView *view = textField;
    while (view && ![view isKindOfClass:[UITableViewCell class]]){
        view = view.superview;
    }
    if (view.tag >=1) {
        [self animateTextField:textField up:YES];

    }
   
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    UIView *view = textField;
    while (view && ![view isKindOfClass:[UITableViewCell class]]){
        view = view.superview;
    }
    
    if (view.tag >=1) {
        
        [self animateTextField:textField up:NO];
    }
    if (view.tag < infoArray.count) {
 
    Receiver *obj = [infoArray objectAtIndex:view.tag];


    if (textField.tag ==1) {
        obj.fName = textField.text;
    }
    else if (textField.tag ==2) {
        obj.lName = textField.text;
    }
    else if (textField.tag ==3) {
        obj.email = textField.text;
        if (![textField.text isEqualToString:@""]) {
            if (![self emailValidate:textField.text]) {
                [self showAlertMessage:@"Please enter valid email"];
            }
        }
       
    }
    }
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 150; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.frame = CGRectOffset(self.frame, 0, movement);
    [UIView commitAnimations];
}

-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}


#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"Validation Error!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
        }
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}



@end
