//
//  UIImage+Base64.h
//  LawNote
//
//  Created by Samreen on 09/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Base64)
- (NSString *)base64String;

@end
