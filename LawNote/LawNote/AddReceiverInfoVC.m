//
//  AddReceiverInfoVC.m
//  LawNote
//
//  Created by Samreen on 26/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "AddReceiverInfoVC.h"
#import "ReceivedInfoCell.h"
#import "KLCPopup.h"

@interface AddReceiverInfoVC ()<UITableViewDelegate,UITableViewDataSource>
{
    int count;
}
@end

@implementation AddReceiverInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    count =1;
    [_tblView reloadData];
    // Do any additional setup after loading the view.
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
- (IBAction)btnAddUser:(id)sender {
    count++;
    [_tblView reloadData];
    
}
- (IBAction)btnRemove:(UIButton*)sender {
    if (count !=1) {
        count--;
        [_tblView reloadData];
    }
  
}

#pragma  mark - tableview Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

        return 171;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return count;
}






- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   // Templates *temp = _templateArray[indexPath.row];
    
//    
//    ReceivedInfoCell      *cell = [tableView dequeueReusableCellWithIdentifier:
//                               @"cell"];
    static NSString *CellIdentifier = @"cell";
    ReceivedInfoCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    

    //ReceivedInfoCell *cell = (ReceivedInfoCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.btnRemove.tag= indexPath.row;
    if (indexPath.row == 0) {
        NSLog(@"%ld",(long)indexPath.row);
        cell.btnRemove.hidden = YES;
    }
    

    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
    
    
}




- (void)showButtonPressed:(id)sender {
    
//    // Generate content view to present
    UIView* contentView = [[UIView alloc] init];
    contentView.translatesAutoresizingMaskIntoConstraints = NO;
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.layer.cornerRadius = 12.0;
    
    UIButton* dismissButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton1.translatesAutoresizingMaskIntoConstraints = NO;
    dismissButton1.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    dismissButton1.backgroundColor = [UIColor colorWithRed:37.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1];
    [dismissButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dismissButton1 setTitleColor:[[dismissButton1 titleColorForState:UIControlStateNormal] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    dismissButton1.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    [dismissButton1 setTitle:@" Sign Now and Send " forState:UIControlStateNormal];
    dismissButton1.layer.cornerRadius = 6.0;
    [dismissButton1 addTarget:self action:@selector(dismissButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    dismissButton.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    dismissButton.backgroundColor = [UIColor colorWithRed:37.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1];
    [dismissButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dismissButton setTitleColor:[[dismissButton titleColorForState:UIControlStateNormal] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    dismissButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    [dismissButton setTitle:@"Send Now and Sign Later" forState:UIControlStateNormal];
    dismissButton.layer.cornerRadius = 6.0;
    [dismissButton addTarget:self action:@selector(dismissButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [contentView addSubview:dismissButton1];
    [contentView addSubview:dismissButton];
    
    NSDictionary* views = NSDictionaryOfVariableBindings(contentView, dismissButton, dismissButton1);
    
    [contentView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(16)-[dismissButton1]-(10)-[dismissButton]-(24)-|"
                                             options:NSLayoutFormatAlignAllCenterX
                                             metrics:nil
                                               views:views]];
    
    [contentView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(36)-[dismissButton1]-(36)-|"
                                             options:0
                                             metrics:nil
                                               views:views]];
    
    // Show in popup
    KLCPopupLayout layout = KLCPopupLayoutMake((KLCPopupHorizontalLayout)3,
                                               (KLCPopupVerticalLayout)3);
    
    KLCPopup* popup = [KLCPopup popupWithContentView:contentView
                                            showType:(KLCPopupShowType)8
                                         dismissType:(KLCPopupDismissType)8
                                            maskType:(KLCPopupMaskType)2
                            dismissOnBackgroundTouch:YES
                               dismissOnContentTouch:YES];
    

        [popup showWithLayout:layout];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
