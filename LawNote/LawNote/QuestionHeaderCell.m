//
//  QuestionHeaderCell.m
//  LawNote
//
//  Created by Samreen Noor on 23/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "QuestionHeaderCell.h"

@implementation QuestionHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
