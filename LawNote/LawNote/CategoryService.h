//
//  CategoryService.h
//  LawNote
//
//  Created by Apple Txlabz on 18/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseService.h"

@interface CategoryService : BaseService

+(void) getCategoriesWithsuccess:(serviceSuccess) success
                         failure:(serviceFailure) failure;


+(void) getOnlyCategoriesWithsuccess:(serviceSuccess) success
                             failure:(serviceFailure) failure;

+(void) getTemplateWithCategoryID:(NSString *)categoryId
                          success:(serviceSuccess) success
                          failure:(serviceFailure) failure;

+(void) getTemplateForReceiverSignatureWithTemplateId:(NSString *)tempId
                                               userID:(NSString *)userId
                                          isCreatedBy:(NSString *)isCreated
                                              success:(serviceSuccess) success
                                              failure:(serviceFailure) failure;
+(void) getReceiveTemplatesWithsuccess:(serviceSuccess) success
                               failure:(serviceFailure) failure;
@end
