//
//  BaseController.h
//  LawNote
//
//  Created by Samreen on 27/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol BaseControllerDelegate

@optional
-(void) sendContractWithSignature:(BOOL )state;
//- delegate method;

@end

@interface BaseController : UIViewController<UIPopoverControllerDelegate, UINavigationControllerDelegate>
@property (assign)id<BaseControllerDelegate> delegate;
- (void)showButtonPressed:(id)controller ;
@end
