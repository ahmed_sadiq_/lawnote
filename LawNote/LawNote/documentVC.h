//
//  documentVC.h
//  LawNote
//
//  Created by Apple Txlabz on 03/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Category.h"
@interface documentVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) NSMutableArray *templateArray;
@property (strong, nonatomic) NSMutableArray *tempTemplateArray;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) Category *category;
@property (nonatomic) BOOL isFromCategory;

@property (strong, nonatomic) NSString *selectedCat;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewLeftConstrain;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;

@property (weak, nonatomic) IBOutlet UILabel *lblCatName;

@end
