//
//  ForgotView.h
//  LawNote
//
//  Created by Apple Txlabz on 03/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ForgotViewDelegate

@optional
//- delegate method;

@end

@interface ForgotView : UIView<UITextFieldDelegate>


@property (strong, nonatomic) IBOutlet UIView *boxView;

@property (strong, nonatomic) IBOutlet UITextField *tfEmail;

@property (weak, nonatomic) IBOutlet UIButton *btnReset;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *boxViewConstrain;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
+ (id) loadWithNibForgotPassword;
-(void) show;

-(void) hide;




@end
