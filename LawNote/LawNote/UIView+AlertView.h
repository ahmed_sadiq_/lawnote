//
//  UIView+AlertView.h
//  LawNote
//
//  Created by Samreen Noor on 24/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (AlertView)
- (UIView *)createDemoView:(NSString *)title message:(NSString *)message;

@end
