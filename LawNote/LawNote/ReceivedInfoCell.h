//
//  ReceivedInfoCell.h
//  LawNote
//
//  Created by Samreen on 26/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceivedInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *tfFname;
@property (weak, nonatomic) IBOutlet UITextField *tfLname;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnRemove;

@end
