//
//  NetworkManager.h
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject
typedef void (^loadSuccess)(id data);
typedef void (^loadFailure)(NSError *error);

+(void) postURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure;

+(void)  getURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure;

+(void)  putURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure;


+(void) postURI:(NSString *) uri
   withFilePath:(NSURL *) path
 attachmentName:(NSString *) paramName
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure;


+(void)  deleteURI:(NSString *) uri
        parameters:(NSDictionary *) params
           success:(loadSuccess) success
           failure:(loadFailure) failure;

+(void) postURIWithFormData:(NSString *) uri
                 parameters:(NSDictionary *) params
                   formData:(NSData *)formdata
                    success:(loadSuccess) success
                    failure:(loadFailure) failure;

+(void) postContractURIWithFormData:(NSString *) uri
                         parameters:(NSDictionary *) params
                           formData:(NSData *)formdata
                            success:(loadSuccess) success
                            failure:(loadFailure) failure;

+(void) postReceiverContractURIWithFormData:(NSString *) uri
                                 parameters:(NSDictionary *) params
                                   formData:(NSData *)formdata
                                    success:(loadSuccess) success
                                    failure:(loadFailure) failure;

@end
