//
//  SignUpVC.h
//  LawNote
//
//  Created by Apple Txlabz on 03/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *bgEmailImg;
@property (weak, nonatomic) IBOutlet UIImageView *bgFirstImg;
@property (weak, nonatomic) IBOutlet UIImageView *bgLastIMg;
@property (weak, nonatomic) IBOutlet UIImageView *bgPasswordImg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTopContrain;
@property (weak, nonatomic) IBOutlet UITextField *tfFirstName;
@property (weak, nonatomic) IBOutlet UITextField *tfLastName;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIView *boxView;
@property (weak, nonatomic) IBOutlet UIImageView *imgbox;
@property (weak, nonatomic) IBOutlet UILabel *lblTermsCondition;

@end
