//
//  AccountService.m
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AccountService.h"
#import "Category.h"
#import "AppDelegate.h"
#import "NotificationObject.h"
#import "User.h"

@implementation AccountService

+(void)forgetPasswordWhereEmail:(NSString *)email
                        succuss:(serviceSuccess)success
                        failure:(serviceFailure)failure{
    NSDictionary * user = @{
                            
                            @"email"  : email
                            
                            
                            };
    
    NSString *forget_uri = [NSString stringWithFormat:URI_FORGET_PASSWORD];
    
    [NetworkManager postURI:forget_uri parameters:user success:^(id data) {
        
        success (data);
        
    } failure:^(NSError *error) {
        failure(error);
        
    }
     ];
    
}



+(void)loginUserWithEmail:(NSString *)email
                 password:(NSString *)pass
                  success:(serviceSuccess)success
                  failure:(serviceFailure)failure{
    
    NSString *deviceTokens = [AppDelegate getDeviceToken];
    if(deviceTokens==nil){
    deviceTokens = @"";
    }
    
    NSDictionary * user = @{
                            KEY_EMAIL:email,
                            KEY_PASSWORD:pass,
                            @"method"  : @"login",
                            @"device_id" : deviceTokens,
                            @"type":@"2"
                            };
    
    
    
    
    NSLog(@"user: %@",user);
    
    [NetworkManager postURI:URI_SIGN_IN
                 parameters:user
                    success:^(id data) {
                        
                        BOOL status=data[@"status"];
                        if (status) {
                            
                            User * user = [User new];
                            user.name = data[@"user_data"][KEY_FIRST_NAME];
                            user.email = data[@"user_data"][KEY_EMAIL];
                            user.firstName = data[@"user_data"][KEY_FIRST_NAME];
                            user.lastName = data[@"user_data"][KEY_LAST_NAME];
                            user.userID = data[@"user_data"][KEY_USER_ID];
                            user.profilePicture = data[@"user_data"][KEY_PROFILE_IMG];

                            [[DataManager sharedManager] setCurrentUser:user];
                            NSMutableArray *categoryList = [Category mapCategoryFromArray:data[@"categories"]].mutableCopy;
                            
                            
                            NSMutableArray *notificationArray = [NotificationObject mapNotificationFromArray:data[@"notifications_data"][@"notification"]].mutableCopy;
                            
                            [DataManager sharedManager].notificationArray = notificationArray.mutableCopy;
                            NSString *receiveCount =   data[@"notifications_data"][@"received_notification_count"];
                              NSString *completeCount =   data[@"notifications_data"][@"completed_notification_count"];
                            NSString *pendingCount =   data[@"notifications_data"][@"pending_notification_count"];

                                        [DataManager sharedManager].notifReceiveCount = [receiveCount intValue];
                            [DataManager sharedManager].notifCompleteCount = [completeCount intValue] + [pendingCount intValue];
                            
                            
                            [DataManager sharedManager].notificationToTalCount = [completeCount intValue] + [receiveCount intValue]+ [pendingCount intValue];




                            
                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logged_in"];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLogin"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"user_name"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.lastName forKey:@"user_lastName"];

                            [[NSUserDefaults standardUserDefaults] setObject:user.email forKey:@"user_email"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.userID forKey:@"user_id"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.profilePicture forKey:@"profilePic"];

                            
                            success(categoryList);
                            // success (user);
                            
                            
                        }
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
}





+(void)registerUserWithFirstName:(NSString *)fName
                        lastName:(NSString *)lName
                           email:(NSString *)email
                        password:(NSString *)password
                         success:(serviceSuccess)success
                         failure:(serviceFailure)failure {
    
    NSDictionary * userParam = @{
                                 
                                 KEY_FIRST_NAME:fName,
                                 KEY_LAST_NAME:lName,
                                 KEY_EMAIL:email,
                                 KEY_PASSWORD:password,
                                 @"method"  : @"signup"
                                 
                                 
                                 };
    
    [NetworkManager postURI:URI_SIGN_UP
                 parameters:userParam
                    success:^(id data) {
                        
                        
                        BOOL status=data[@"status"];
                        if (status) {
                            
                            User * user = [User new];
                            user.name = data[@"user_data"][KEY_FIRST_NAME];
                            user.email = data[@"user_data"][KEY_EMAIL];
                            user.firstName = data[@"user_data"][KEY_FIRST_NAME];
                            user.lastName = data[@"user_data"][KEY_LAST_NAME];
                            user.userID = data[@"user_data"][KEY_USER_ID];
                            user.profilePicture = data[@"user_data"][KEY_PROFILE_IMG];

                            [[DataManager sharedManager] setCurrentUser:user];
                            NSMutableArray *categoryList = [Category mapCategoryFromArray:data[@"categories"]].mutableCopy;
                            
                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logged_in"];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstLogin"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"user_name"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.lastName forKey:@"user_lastName"];

                            [[NSUserDefaults standardUserDefaults] setObject:user.email forKey:@"user_email"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.userID forKey:@"user_id"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.profilePicture forKey:@"profilePic"];

                            
                            success (categoryList);
                            
                            // success (user);
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}

+(void) updateUserProfileWithFirstName:(NSString *)fName
                        lastName:(NSString *)lName
                           userImg:(UIImage *)img
                         success:(serviceSuccess)success
                         failure:(serviceFailure)failure {
    
    User *user = [DataManager sharedManager].currentUser;
    NSDictionary * userParam = @{
                                 @"user_id":user.userID,
                                 @"first_name":fName,
                                 @"last_name":lName
                                };
    NSData *imageData = UIImageJPEGRepresentation(img, 0.5);

    [NetworkManager postURIWithFormData:URL_EDIT_PROFILE parameters:userParam formData:imageData success:^(id data) {
        
        BOOL status=data[@"status"];
        if (status) {
            
            User * user = [User new];
            user.name = data[@"user_data"][KEY_FIRST_NAME];
            user.email = data[@"user_data"][KEY_EMAIL];
            user.firstName = data[@"user_data"][KEY_FIRST_NAME];
            user.lastName = data[@"user_data"][KEY_LAST_NAME];
            user.userID = data[@"user_data"][KEY_USER_ID];
            user.profilePicture = data[@"user_data"][KEY_PROFILE_IMG];

            [[DataManager sharedManager] setCurrentUser:user];
        
            [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"user_name"];
            [[NSUserDefaults standardUserDefaults] setObject:user.lastName forKey:@"user_lastName"];

            [[NSUserDefaults standardUserDefaults] setObject:user.email forKey:@"user_email"];
            [[NSUserDefaults standardUserDefaults] setObject:user.userID forKey:@"user_id"];
            [[NSUserDefaults standardUserDefaults] setObject:user.profilePicture forKey:@"profilePic"];

            success(data);
        }
    } failure:^(NSError *error) {
        failure(error);

    }];
    
    
}


+(void) updateUserPasswordWithPassword:(NSString *)pass
                               success:(serviceSuccess)success
                               failure:(serviceFailure)failure {
    
    User *user = [DataManager sharedManager].currentUser;
    NSDictionary * userParam = @{
                                 @"user_id":user.userID,
                                 @"password":pass
                                 };
    
    [NetworkManager postURI:URL_EDIT_PROFILE parameters:userParam success:^(id data) {
        BOOL status=data[@"status"];
        if (status) {
            success(data);
        }
    } failure:^(NSError *error) {
        failure(error);

    }];
    
    
}




+(void) registerDeviceIdWithsuccess:(serviceSuccess)success
                  failure:(serviceFailure)failure{
    
    NSString *deviceTokens = [AppDelegate getDeviceToken];
    User *user = [DataManager sharedManager].currentUser;
    if(deviceTokens==nil){
        deviceTokens = @"";
    }
    
    NSDictionary * params = @{
                          @"user_id": user.userID,
                            @"device_id" : deviceTokens,
                            @"type":@"2"
                            };
    
    
    
    
    NSLog(@"user: %@",user);
    
    [NetworkManager postURI:URI_REGISTER_DEVICE
                 parameters:params
                    success:^(id data) {
                    
                        
                        BOOL status=data[@"status"];
                        if (status) {
                            success(data);

                                                 }
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
}


//
//+ (void)getUserInfoWithSuccess:(serviceSuccess)success
//                       failure:(serviceFailure)failure {
//
//
////    NSString * uri_update_profile = [NSString stringWithFormat:URI_USER_STATUS,[self currentUserID]];
////
////
////    [NetworkManager getURI:uri_update_profile parameters:nil success:^(id data)
////     {
////         User *user = [[User alloc ] initWithDictionary:data[@"user"]];
////
////         success(user);
////     } failure:^(NSError *error) {
////
////         failure(error);
////     }];
//    
//    
//}

@end
