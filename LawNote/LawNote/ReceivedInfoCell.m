//
//  ReceivedInfoCell.m
//  LawNote
//
//  Created by Samreen on 26/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "ReceivedInfoCell.h"

@implementation ReceivedInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)prepareForReuse{
    [super prepareForReuse];
    self.tfFname.text = @"";
    self.tfLname.text = @"";
    self.tfEmail.text = @"";
    
    
    // Then Reset here back to default values that you want.
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
