//
//  CreateYourOwnAgreementVC.m
//  LawNote
//
//  Created by Samreen Noor on 08/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CreateYourOwnAgreementVC.h"
#import "DocumentService.h"
#import "UIAlertView+JS.h"
#import "ManageViewController.h"
#import "UIImageView+URL.m"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"
#import "ReceiverInfo.h"
#import "Receiver.h"
#import "InfoCell.h"
#import "CategoryService.h"
#import "NotificationObject.h"


@interface CreateYourOwnAgreementVC ()<UITextFieldDelegate,UITextViewDelegate,UITextViewDelegate,ReceiverInfoDelegate>
{
    BOOL isSenderView;
    UIImage *signatureImgSender;
    ReceiverInfo *receiverInfoView;

    NSString *senderFName;
    NSString *senderLName;
    NSString *recFName;
    NSString *recLName;
    NSString *senderEmail;
    NSString *recEmail;
    UITextField *checkFiled;
    UITextView *checkedTextView;
    UIGestureRecognizer *tapper;
    NSString *templateId;
    BOOL isPending;
    NSMutableArray *recInfoArray;
    BOOL isDiscardContract;

    
}
@end

@implementation CreateYourOwnAgreementVC


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
    if ([segue.identifier isEqualToString:@"manage" ]) {
        ManageViewController  *manageVC = segue.destinationViewController;
        manageVC.isPanding = isPending;
        
    }
    
    
}

-(void)setUpView{
   senderFName = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"];
    senderLName = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_lastName"];
    
   senderEmail = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_email"];
    _btnSendAgreement.layer.cornerRadius = 5; // this value vary as per your desire
    _btnSendAgreement.clipsToBounds = YES;
    _btnSendForsign.layer.cornerRadius = 5; // this value vary as per your desire
    _btnSendForsign.clipsToBounds = YES;
    _boxView.layer.cornerRadius = 5; // this value vary as per your desire
    _boxView.clipsToBounds = YES;
    _lblSenderNamePreview.text =senderFName;
    _lblsenderEmailPreview.text =senderEmail;
    _lblSenderName.text=senderFName;
    _lblSenderEmail.text =senderEmail;
    if (_isfromDraft) {
        [_btnCreate setTitle:@"Edit" forState:UIControlStateNormal];
        _lblDocName.text = _templete.template_title;
        _lblDescription.text = _templete.template_description;
        _tfDocName.text = _templete.template_title;
      _editTextView.text = _templete.template_description;
        if(![_templete.sender_signature_image isEqualToString:@""]){
            [_senderSignatureView setImageWithURL:_templete.sender_signature_image];
        }
        CGSize size = [_editTextView.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
        
        _DescriptionHeightConstrain.constant = size.height+80.0;
        templateId = _templete.template_id;
     
            _editTextView.textColor = [UIColor blackColor]; //optional
        
        
    }
    else if(_isfromPending){
      
        [_btnCreate setTitle:@"Edit" forState:UIControlStateNormal];

        _lblReciverName.text=_templete.receiver_first_name;
        _lblRecEmail.text = _templete.receiver_email;
        _lblRecNamePreview.text = _templete.receiver_first_name;
        _lblRevEmailPreview.text =_templete.receiver_email;
        recLName = _templete.receiver_last_name;
        recFName=_templete.receiver_first_name;
        recEmail = _templete.receiver_email;
        _lblDocName.text = _templete.template_title;
        _lblDescription.text = _templete.template_description;
        _tfDocName.text = _templete.template_title;
        _editTextView.text = _templete.template_description;
        if(![_templete.sender_signature_image isEqualToString:@""]){

        [_senderSignatureView setImageWithURL:_templete.sender_signature_image];
            
        }
        CGSize size = [_editTextView.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
        
        _DescriptionHeightConstrain.constant = size.height+80.0;
        templateId = _templete.template_id;
        _editTextView.textColor = [UIColor blackColor]; //optional
            if (![_templete.sender_signature_image isEqualToString:@""] && _templete.sender_signature_image != nil) {
      [self btnPreview:nil];
//        _btnSendAgreement.userInteractionEnabled = NO;
//        _btnSendForsign.userInteractionEnabled = NO;
//        _btnCreate.userInteractionEnabled = NO;
//              _btnSendForsign.hidden = YES;
//              _btnSendAgreement.hidden  = YES;
        //[self setupPendingView];
          }

    }
    recInfoArray = _templete.receiverInfoArray.mutableCopy;
    [_infoTbl1 reloadData];
    [_infoTbl2 reloadData];
    CGSize tableViewSize = _infoTbl1.contentSize;
    _infoBox1HeightConstrain.constant =  tableViewSize.height;
    _infoBox2HeightConstrain.constant =70 + tableViewSize.height;
    _infoBox1UperViewHeightConstrain.constant = 500 + tableViewSize.height;
    _infoBox2UperViewHeightConstrain.constant = 400 + tableViewSize.height;
}

-(void) setupPendingView{
    
    
    User *user = [DataManager sharedManager].currentUser;
    if (![user.userID isEqualToString:_templete.sender_id]) {
        if(![_templete.receiver_signature_image isEqualToString:@""]){

        [_senderSignatureView setImageWithURL:_templete.receiver_signature_image];
        }
        
        _lblSenderNamePreview.text =_templete.receiver_first_name;
        _lblsenderEmailPreview.text =_templete.receiver_email;
        _lblRecNamePreview.text =_templete.sender_first_name;
        _lblRevEmailPreview.text =_templete.sender_email;
        _lblAuthorPart.text = @"Receiver Signature and Information";
        _lblReceiverPart.text = @"Author Signature and Information";
    }
    
    
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    recInfoArray = [[NSMutableArray alloc]init];
    templateId = @"0";
    [self setUpView];
    [DataManager sharedManager].isChageDoc = NO;

    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    //[self getNotifiTemplate];
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [checkedTextView resignFirstResponder];
    [checkFiled resignFirstResponder];
}

#pragma mark - IBButton Action

- (IBAction)btnHide:(id)sender {
    [checkFiled resignFirstResponder];

}
- (IBAction)btnCancel:(id)sender {
    
    _signatureBox.hidden = YES;
    _tfFirstName.text = @"";
    _tfLastName.text = @"";
    _tfEmail.text = @"";
    [_signView erase];
    [checkFiled resignFirstResponder];

}
- (IBAction)btnSaveContract:(id)sender {
    if ([self checktextFields]) {
        
        if (isSenderView) {
            senderFName =_tfFirstName.text;
            senderLName = _tfLastName.text;
            senderEmail = _tfEmail.text;
            _lblSenderName.text = senderFName;
            _lblSenderEmail.text =senderEmail;
            signatureImgSender = _signView.signatureImage;
            _lblSenderNamePreview.text =senderFName;
            _lblsenderEmailPreview.text =senderEmail;
            _senderSignatureView.image = _signView.signatureImage;

            _tfFirstName.text = @"";
            _tfLastName.text = @"";
            _tfEmail.text = @"";
            [_signView erase];
            
        }
        else{
            recFName =_tfFirstName.text;
            recLName = _tfLastName.text;
            recEmail = _tfEmail.text;
            _lblReciverName.text = recFName;
            _lblRecEmail.text =recEmail;
            _lblRecNamePreview.text =recFName;
            _lblRevEmailPreview.text =recEmail;
            _tfFirstName.text = @"";
            _tfLastName.text = @"";
            _tfEmail.text = @"";
            
            
            
        }
        _signatureBox.hidden = YES;
        [self updateChangeStatus];

    }

}
- (IBAction)btnClear:(id)sender {
    [_signView erase];

}
- (IBAction)btnCreate:(id)sender {
    if (_isfromPending) {
        if (![_templete.sender_signature_image isEqualToString:@""] && _templete.sender_signature_image != nil) {
            [self showWarningAlert];
        }
        else{
            _previewScroll.hidden = YES;
            _createScrollView.hidden = NO;
            [_btnCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_btnSave setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
            [_btnPreview setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
            _editLine.hidden = NO;
            _previewLine.hidden = YES;
        }
    }
    else{
        _previewScroll.hidden = YES;
        _createScrollView.hidden = NO;
        [_btnCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnSave setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
        [_btnPreview setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
        _editLine.hidden = NO;
        _previewLine.hidden = YES;}
    
    
   
}
- (IBAction)btnPreview:(id)sender {
    _previewScroll.hidden = NO;
    _createScrollView.hidden = YES;

    [_btnPreview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnCreate setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    [_btnSave setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    _editLine.hidden = YES;
    _previewLine.hidden = NO;
}
- (IBAction)btnSave:(id)sender {
    _previewScroll.hidden = YES;
    _createScrollView.hidden = NO;

    [_btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnCreate setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_btnPreview setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    if (signatureImgSender==nil) {
        signatureImgSender = _senderSignatureView.image;
        
    }
    [self saveInDraft];

}
- (IBAction)btnTapOne:(id)sender {
    _signatureBox.hidden = NO;
    isSenderView = YES;
    _signView.userInteractionEnabled = YES;
    _tfFirstName.text = senderFName;
    _tfLastName.text = senderLName;
    _tfEmail.text = senderEmail;
    //_signatureView.signatureImage = signatureImgSender;
}
- (IBAction)btnTapTwo:(id)sender {
    _signatureBox.hidden = NO;
    isSenderView = NO;
    _signView.userInteractionEnabled = NO;
    _tfFirstName.text = recFName;
    _tfLastName.text = recLName;
    _tfEmail.text = recEmail;

}
- (IBAction)btnBack:(id)sender {
    [self updateChangeStatus];

    if (_isChageDoc) {
        //_saveDocumentView.hidden = NO;

        [self showSaveDocumentAlert];
        
        
    }
    else{
        if (isDiscardContract) {
            [DataManager sharedManager].isChageDoc = YES;

        }
        else
        [DataManager sharedManager].isChageDoc = NO;
        
        [self.navigationController popViewControllerAnimated:YES];
    }

}
- (IBAction)btnSend:(id)sender {
//    if (signatureImgSender==nil) {
//        signatureImgSender = _senderSignatureView.image;
//
//    }
//
//[self sendSignature];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Signature Options" message:@"Contract send for Signature" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Sign Now and Send" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self sendContractWithSignature:YES];
        
        // Distructive button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Send Now and Sign Later" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        [self sendContractWithSignature:NO];
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];

}

-(void) updateChangeStatus{
    
    if (_isfromPending || _isfromDraft) {
        if ([_lblDocName.text isEqualToString:_templete.template_title]&& [_lblDescription.text isEqualToString:_templete.template_description]) {
            _isChageDoc = NO;

        }
        else{
            _isChageDoc = YES;

        }
    }
    else{
    
    if ([_lblDocName.text length]>0||[_lblDescription.text length]>0||signatureImgSender!=nil) {
        _isChageDoc = YES;
    }
   
    else{
        _isChageDoc = NO;
        
    }
    }
}

#pragma mark - TextField Methods


-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}


-(BOOL)checktextFields{
    
    BOOL willEnabled = NO;
    if (isSenderView) {
        
        if( [_tfFirstName.text length] >= 1 && [_tfLastName.text length] >= 1 &&[self emailValidate:_tfEmail.text] == YES && _signView.signatureImage!=nil ){
            willEnabled = YES;
        }
        if (_tfFirstName.text.length==0 || _tfLastName.text.length==0||_tfEmail.text.length==0) {
            [self showAlertMessage:@"Please enter valid informtation"];
            return NO;
        }
        else if (![self emailValidate:_tfEmail.text]) {
            [self showAlertMessage:@"Please enter valid email"];
            return NO;
        }
        else if (_signView.signatureImage == nil) {
            [self showAlertMessage:@"Enter a Vaild Signature"];
            return NO;
        }
    }
    else{
        if( [_tfFirstName.text length] >= 1 && [_tfLastName.text length] >= 1 &&[self emailValidate:_tfEmail.text] == YES ){
            willEnabled = YES;
        }
        if (_tfFirstName.text.length==0 || _tfLastName.text.length==0||_tfEmail.text.length==0) {
            [self showAlertMessage:@"Please enter valid informtation"];
            return NO;
        }
        else if (![self emailValidate:_tfEmail.text]) {
            [self showAlertMessage:@"Please enter valid email"];
            return NO;
        }
        
        
    }
    
    return willEnabled;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    checkFiled = textField;
    if ([textField isEqual: _tfDocName] ) {
        
    }
    else{
    [self animateTextField:nil up:YES];
    }
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField isEqual: _tfDocName] ) {
        _lblDocName.text = _tfDocName.text;
        [self updateChangeStatus];
    }
    else{
    [self animateTextField:nil up:NO];
    }
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    _lblDocName.text = _tfDocName.text;
    [self updateChangeStatus];
    [textField resignFirstResponder];
    
    
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    checkedTextView = textView;

    if ([textView.text isEqualToString:@"type here..."]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"type here...";
        textView.textColor = [UIColor lightGrayColor]; //optional
        _lblDescription.text = @"";
    }
    else{
        _lblDescription.text = _editTextView.text;
        CGSize size = [_editTextView.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
        
        _DescriptionHeightConstrain.constant = size.height+80.0;
        [self updateChangeStatus];

    }
    [textView resignFirstResponder];
}
- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}


-(void)showSaveDocumentAlert{
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Save",@"Discard",@"Cancel" ,nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"Wait!" message:@"Do you want to save this agreement as a draft?"]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            [self saveInDraft];

        }
        if(buttonIndex == 1){
            [DataManager sharedManager].isChageDoc = YES;
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }  if(buttonIndex == 2){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
    
    
    
    //////////////////////
   /* UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Wait!" message:@"Do you want to save this agreement as a draft?" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save",@"Discard", nil];
    
    
    [alert showWithBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        
        if (buttonIndex == 1) {
        
                [self saveInDraft];
            
            
            
        }
        else if (buttonIndex==2){
            
            [DataManager sharedManager].isChageDoc = YES;

            [self.navigationController popViewControllerAnimated:YES];
            
        }
        else{
            
        }
    }] ;
    */
    
    
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        
       // _lblDescription.text = _editTextView.text;
        
        return NO;
    }
    
    return YES;
}

#pragma mark -Api Calling
-(void) saveInDraft{
    [DataManager sharedManager].isCompleted=NO;
    if (signatureImgSender==nil) {
        signatureImgSender = _senderSignatureView.image;
        
    }
    if ([_lblDocName.text length]>0||[_lblDescription.text length]>0) {
        if (_isfromDraft||_isfromPending) {
            
            [DocumentService saveCreateOwnContractWithReceiverFirstName:recFName lastName:recLName senderSignImg:signatureImgSender eamil:recEmail TemplateId:templateId templateTitle:_lblDocName.text Content:_lblDescription.text success:^(id data) {
                [DataManager sharedManager].isChageDoc = YES;
                isPending=NO;

                [self.navigationController popViewControllerAnimated:YES];
                
            } failure:^(NSError *error) {
                [self showAlertMessage:error.localizedDescription];
                
            }];

        }
        else{
        [DocumentService saveCreateOwnContractWithReceiverFirstName:recFName lastName:recLName senderSignImg:signatureImgSender eamil:recEmail TemplateId:templateId templateTitle:_lblDocName.text Content:_lblDescription.text success:^(id data) {
            isPending=NO;

        [self performSegueWithIdentifier:@"manage" sender:nil];

    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];

    }];
        }
        
    }
    else {
        [self showAlertMessage:@"Please enter document informtation Carefully!"];
        
    }
  

    
}
-(void) sendSignature{
    [DataManager sharedManager].isCompleted=NO;

    if ([recFName length]>0&&[recLName length]>0&&[recEmail length]>0) {
        
        if (_isfromDraft||_isfromPending) {
            [DocumentService sendCreateOwnContractWithReceiverInfo:recInfoArray  senderSignImg:signatureImgSender TemplateId:templateId templateTitle:_lblDocName.text Content:_lblDescription.text success:^(id data) {
                isPending=YES;

                [DataManager sharedManager].isChageDoc = YES;
                [DataManager sharedManager].isFromDraftSend = YES;

                
                [self.navigationController popViewControllerAnimated:YES];
            } failure:^(NSError *error) {
                [self showAlertMessage:error.localizedDescription];
                
            }];
        }
        else{
        [DocumentService sendCreateOwnContractWithReceiverInfo:recInfoArray senderSignImg:signatureImgSender TemplateId:templateId templateTitle:_lblDocName.text Content:_lblDescription.text success:^(id data) {
            isPending=YES;
            [self performSegueWithIdentifier:@"manage" sender:nil];

        } failure:^(NSError *error) {
            [self showAlertMessage:error.localizedDescription];

        }];
        }
    }
    else if([_lblDocName.text length]==0){
        [self showAlertMessage:@"Please enter title informtation Carefully!"];
        
    }
    
    else if([_lblDescription.text length]==0){
        [self showAlertMessage:@"Please enter documment detail Carefully!"];
        
    }
//    else if (signatureImgSender==nil){
//        
//        [self showAlertMessage:@"Please enter your Signature"];
//        
//    }

    else {
        [self showAlertMessage:@"Please enter complete informtation of receiver"];
        
    }
}





-(void)sendContractWithSignature:(BOOL)state{
    
    receiverInfoView = [ReceiverInfo loadWithReceiverInfoViewWithSignature:state andRecInfoArray:recInfoArray];
    receiverInfoView.delegate = self;
    [self.view addSubview:receiverInfoView];
    
    
    [receiverInfoView show];
    
    
    
    
    
}
///////////////////// Receiver Delegates ////////////////
-(void)userInfo:(NSArray *)infoArray withSignature:(UIImage *)signImg{
    
    Receiver *obj = infoArray[0];
    recFName = obj.fName;
    recLName = obj.lName;
    recEmail = obj.email;
    signatureImgSender = signImg;
    recInfoArray = infoArray.mutableCopy;
    [_infoTbl1 reloadData];
    [_infoTbl2 reloadData];
    CGSize tableViewSize = _infoTbl1.contentSize;
    _infoBox1HeightConstrain.constant = tableViewSize.height;
    _infoBox2HeightConstrain.constant = 70 + tableViewSize.height;
    _infoBox1UperViewHeightConstrain.constant = 500 + tableViewSize.height;
    _infoBox2UperViewHeightConstrain.constant = 500 + tableViewSize.height;
    [self sendSignature];
    
}
-(void)cancelUserInfo:(NSMutableArray *)infoArray{
    
    recInfoArray = infoArray.mutableCopy;
    [_infoTbl1 reloadData];
    [_infoTbl2 reloadData];
    CGSize tableViewSize = _infoTbl1.contentSize;
    _infoBox1HeightConstrain.constant = tableViewSize.height;
    _infoBox2HeightConstrain.constant = 70 + tableViewSize.height;
    _infoBox1UperViewHeightConstrain.constant = 500 + tableViewSize.height;
    _infoBox2UperViewHeightConstrain.constant = 500 + tableViewSize.height;
}



#pragma mark - UITableView Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Receiver *rec = recInfoArray[indexPath.row];
    if (indexPath.row == 0) {
        if([rec.receiver_signature_image isEqualToString:@""] || rec.receiver_signature_image ==nil){
            return 70;
        }
        return 140;
        
    }
    else
        return 140;
    
    
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  recInfoArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Receiver *rec = recInfoArray[indexPath.row];
    InfoCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if (cell==nil) {
        cell =[[InfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.lblName.text = [NSString stringWithFormat:@"%@  %@",rec.fName,rec.lName];
    cell.lblEmail.text = rec.email;
    
    if([rec.receiver_signature_image isEqualToString:@""] || rec.receiver_signature_image ==nil){
        cell.signImg.image   = [UIImage imageNamed:@"no-sig"];
        
    }
    else{
        [cell.signImg setImageWithURL:rec.receiver_signature_image];
        
    }
    
    
    return cell;
    
}
-(void) getNotifiTemplate{
    
    [CategoryService getTemplateForReceiverSignatureWithTemplateId:_templete.template_id userID:@"" isCreatedBy:@"" success:^(id data) {
        for (int i=0 ; i< [DataManager sharedManager].notificationArray.count ; i++) {
            NotificationObject *notif  = [DataManager sharedManager].notificationArray[i];
            if ([_templete.template_id isEqualToString:notif.template_id]) {
                
                [[DataManager sharedManager].notificationArray removeObjectAtIndex:i];
                [DataManager sharedManager].isChangeInReceived = YES;
                
                break;
            }
        }
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    }];
}







-(void) editPendingTemplate{
    [DocumentService editPendingTemplateWithTemplateID:_templete.template_id success:^(id data) {
        
        [self setUpViewForEditing];
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    }];
    
}

-(void) setUpViewForEditing{
    [recInfoArray removeAllObjects];
    _templete.receiverInfoArray =  recInfoArray.mutableCopy;
    [_infoTbl1 reloadData];
    [_infoTbl2 reloadData];
    CGSize tableViewSize = _infoTbl1.contentSize;
    _infoBox1HeightConstrain.constant = tableViewSize.height;
    _infoBox2HeightConstrain.constant = 70 + tableViewSize.height;
    _infoBox1UperViewHeightConstrain.constant = 500 + tableViewSize.height;
    _infoBox2UperViewHeightConstrain.constant = 500 + tableViewSize.height;
    signatureImgSender = nil;
    _previewScroll.hidden = YES;
    _createScrollView.hidden = NO;
    [_btnCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnPreview setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    _editLine.hidden = NO;
    _previewLine.hidden = YES;
    _senderSignatureView.image = [UIImage imageNamed:@""];
    _templete.sender_signature_image =@"";
    isDiscardContract =YES;
}

-(void)showWarningAlert{
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Edit",@"Cancel" ,nil]];
    [customAlertView setContainerView:[self createFriendsDialog:@"Warning!" message:@"You have already signed the contract and marked it completed. If you wish to edit this, current version will be discarded." imageUrl:@""]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
            [self editPendingTemplate];
            
        }
        else{
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
    
}


-(UIView *)createFriendsDialog:(NSString *)title message:(NSString *)message imageUrl:(NSString *)imageUrl
{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 170)];
    if([message isEqualToString:@""] || message == nil){
        demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    }
    
    UILabel *headingView = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 270, 20)];
    UILabel *descriptionView = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 270, 90)];
    headingView.text = title;
    descriptionView.text = message;
    descriptionView.minimumFontSize = 8;
    descriptionView.adjustsFontSizeToFitWidth = YES;
    descriptionView.numberOfLines = 0;
    descriptionView.textColor = [UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
    headingView.textColor = [UIColor blackColor];//colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
    descriptionView.textAlignment = NSTextAlignmentCenter;
    headingView.textAlignment = NSTextAlignmentCenter;
    headingView.font = [UIFont fontWithName:@"PassionOne-Bold" size:16];
    descriptionView.font = [UIFont fontWithName:@"ArchivoNarrow-Bold" size:14];
    [demoView addSubview:headingView];
    [demoView addSubview:descriptionView];
    
    
    
    return demoView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
