//
//  CatalogViewController.m
//  LawNote
//
//  Created by Apple Txlabz on 14/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CatalogViewController.h"
#import "UIImageView+URL.h"
#import "CatalogCell.h"
#import "Category.h"
#import "DataManager.h"
#import "documentVC.h"
#import "BGTableViewRowActionWithImage.h"
#import "Constant.h"
#import "CategoryFooterCell.h"
#import "CategoryService.h"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"
@interface CatalogViewController ()
{
    long count;
}
@end

@implementation CatalogViewController
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
   if ([segue.identifier isEqualToString:@"document" ]) {
        documentVC  *docVC = segue.destinationViewController;
       Category *cat = sender;
        docVC.templateArray = cat.templatesArray.mutableCopy;
       docVC.category = cat;
       docVC.isFromCategory = YES;
    }
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    _catArray = [DataManager sharedManager].categoryArray.mutableCopy;
    _tempcatArray = [DataManager sharedManager].categoryArray.mutableCopy;

    NSLog(@"%@",_catArray);
    
    count=10;
    [self setupRefreshControl];
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if ([DataManager sharedManager].isChageDoc) {
        [self getCategory];
    }
    
}
#pragma  mark - tableview Methods
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    CategoryFooterCell  *footer;
        footer = [[[NSBundle mainBundle] loadNibNamed:@"CategoryFooterCell" owner:self options:nil] objectAtIndex:0];
  
    [footer.btnCreateNewAggremnet addTarget:self action:@selector(createNewAgreement:) forControlEvents:UIControlEventTouchUpInside];
    return footer;

}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    if (IS_IPAD) {
        return 74;
        
    }
    else
        return 55;}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPAD) {
        return 74;

    }
    else
    return 55;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _catArray.count;
}






- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    CatalogCell      *cell = [tableView dequeueReusableCellWithIdentifier:
                               @"cell"];
    
   Category *cat = _catArray[indexPath.row ];

    cell.lblName.text = [cat.title uppercaseString];
    cell.lblDescription.text = cat.category_description;
    [cell.catImg setImageWithURL:cat.catImg];
    cell.tag = indexPath.row;
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Category *cat = _catArray[indexPath.row ];
    [[DataManager sharedManager] setSelectedCtaegory:cat.title];

    [self performSegueWithIdentifier:@"document" sender:cat];
    
    
    
}




- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete

        NSLog(@"row deleted");

        [_catArray removeObjectAtIndex:indexPath.row];
                [_tblView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                [_tblView reloadData];
       

    }
}



- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    UITableViewRowAction *moreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
//        // show UIActionSheet
//    }];
    NSUInteger height;
    if (IS_IPAD) {
        height = 74;
    }
    else
        height =55;
    BGTableViewRowActionWithImage *moreAction= [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDefault title:@"           " backgroundColor:[UIColor redColor] image:[UIImage imageNamed:@"delete"] forCellHeight:height andFittedWidth:YES handler:^(UITableViewRowAction *act, NSIndexPath *indx) {
    [self tableView:tableView commitEditingStyle:UITableViewCellEditingStyleDelete forRowAtIndexPath:indexPath];

}];


  
    return @[moreAction];
}

#pragma mark - Searchbar delegates & helpers
- (IBAction)btnSearch:(id)sender {
    
    _searchBar.hidden=NO;
    _lblTitle.hidden = YES;
    _btnSearch.hidden = YES;
    _btnMenu.hidden = YES;
    _searchBarLeftConstrain.constant = 15;
    [UIView animateWithDuration:0.3f
                     animations:^{
                         
                         [self.view layoutIfNeeded];
                         
                     }];
    
    [_searchBar setShowsCancelButton:YES animated:YES];
    
    [_searchBar setAlpha:1.0];
    [_searchBar becomeFirstResponder];
}
-(void)searchFieldReset{
    if (_searchBar.hidden==NO) {
        
        [_searchBarLeftConstrain setConstant:230];
        
        [UIView animateWithDuration:0.3f
                         animations:^{
                             
                             [self.view layoutIfNeeded];
                             
                         }];
        _searchBar.text = @"";
        [_searchBar resignFirstResponder];
        
        [_searchBar setShowsCancelButton:NO animated:YES];
        _searchBar.hidden = YES;
        _btnSearch.hidden =NO;
        _lblTitle.hidden=NO;
        _btnMenu.hidden = NO;

    }
    
    
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [_searchBar resignFirstResponder];
    
    [_searchBarLeftConstrain setConstant:230];
    [UIView animateWithDuration:0.3f
                     animations:^{
                         
                         [self.view layoutIfNeeded];
                         
                     }];
    _searchBar.text = @"";
    [_searchBar resignFirstResponder];
    
    [_searchBar setShowsCancelButton:NO animated:YES];
    _searchBar.hidden = YES;
    _btnSearch.hidden =NO;
    _lblTitle.hidden=NO;
    _btnMenu.hidden = NO;
    _catArray = _tempcatArray;
    [_tblView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    //  NSString  *searchString = _searchBar.text;
    //    if (![searchString isEqualToString:@""]) {
    //        [self searchFieldReset];
    //    }
    [_searchBar resignFirstResponder];
}


- (void)getActiveUserListForSearch

{
    // NSString *name;
    NSMutableArray *searchArray=[[NSMutableArray alloc]init];
    
    if (_searchBar.text) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.title CONTAINS[cd] %@",_searchBar.text];
        searchArray = [_tempcatArray filteredArrayUsingPredicate:predicate].mutableCopy;
        
        //[searchArray addObject:dic];
        
    }
    
    
    if (searchArray.count>0) {
        
        _catArray = searchArray;
        
        [_tblView reloadData];
    }
    
    
}
-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString * searchStr = [_searchBar.text stringByReplacingCharactersInRange:range withString:text];
    
    if ([searchStr isEqualToString:@""]) {
        
        _catArray = _tempcatArray;
        [_tblView reloadData];
    }
    else{
        [self performSelector:@selector(getActiveUserListForSearch) withObject:nil afterDelay:0.15];
    }
    return YES;
}

-(void)createNewAgreement:(UIButton*)sender{
    [self performSegueWithIdentifier:@"createNewAgree" sender:nil];
    
}
#pragma mark - Api Calling
-(void) getCategory{
    [CategoryService getOnlyCategoriesWithsuccess:^(id data) {
        [self.refreshControl endRefreshing];

        [DataManager sharedManager].categoryArray = data;
        _catArray = data;
        [_tblView reloadData];
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    }];

}
#pragma mark - Helper Methods

- (void)setupRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor       = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(refreshCategory:) forControlEvents:UIControlEventValueChanged];
    [_tblView addSubview:_refreshControl];
}

- (void)refreshCategory:(id)sender
{
    [self getCategory];

    
}
#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
