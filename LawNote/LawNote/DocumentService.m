//
//  DocumentService.m
//  LawNote
//
//  Created by Samreen Noor on 27/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "DocumentService.h"
#import "DataManager.h"
#import "User.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "Options.h"
#import "Questions.h"
#import "Receiver.h"
#import "UIImage+Base64.h"

@implementation DocumentService

+(void) saveFilledDocumentWithAnswersArray:(NSArray *)answerArray
                             andTemplateId:(NSString *)templateId
                                   success:(serviceSuccess)success
                                   failure:(serviceFailure)failure {
    
    NSMutableArray *ansArray = [[NSMutableArray alloc]init];
    for (Questions *question in answerArray) {
        NSDictionary *answer;
        if (question.isFilled) {

        if ([question.question_type isEqualToString:@"editText"]||[question.question_type isEqualToString:@"editTextArea"]||[question.question_type isEqualToString:@"date"]) {
            
            answer = @{
                       @"question_id":question.question_id,
                       @"answer":question.answer,
                       @"type":@"question"

                       
                       };
            [ansArray addObject:answer];

        }
        else if ([question.question_type isEqualToString:@"radio_option"]){
            
            answer = @{
                       @"question_id":question.question_id,
                       @"answer":question.optionAnswerId,
                       @"type":@"question"

                       
                       };
            [ansArray addObject:answer];

            for (Options *opt in question.optionArray) {
                for (Questions *question in opt.questionArray) {
                    if (question.isFilled) {
                        if([question.question_type isEqualToString:@"editText"]){
                            answer = @{
                                       @"question_id":question.question_id,
                                       @"answer":question.answer,
                                       @"type":@"option_question"

                                       
                                       };
                            [ansArray addObject:answer];

                        }
                        
                    }
                }
            }
        }
        else{
            answer = @{
                       @"question_id":question.question_id,
                       @"answer":question.optionAnswerId,
                       @"type":@"question"

                       
                       };
            [ansArray addObject:answer];

            
        }
        }
        
    }

    
    
    User *user = [DataManager sharedManager].currentUser;
    
    NSDictionary *docParam = @{
                               @"user_id":user.userID,
                               @"answers_array":ansArray,
                               @"template_id":templateId
                               
                               };
    
    
    
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:docParam
                                                    options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if (error) {
        NSLog(@"%@",error);
 
    }
    else{
    NSString *myString = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",myString);

    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSURL *url = [NSURL URLWithString:@"http://lawnotes.witsapplication.com/user/saveAnswers"];

    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            [SVProgressHUD dismiss];
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int status = [[result objectForKey:@"status"] intValue];
            NSString *message =  [result objectForKey:@"message"];
            if(status == 1)
            {
                
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
               // [alertView show];
                success(result);
                          }
        }
        else{
            
            [SVProgressHUD dismiss];
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
            [alertView show];
            failure(error);
        }
    }];
    
}


+(void) getManagedDocumentWithsuccess:(serviceSuccess) success
                         failure:(serviceFailure) failure{
    User *user = [DataManager sharedManager].currentUser;
    NSDictionary *param = @{
                            @"user_id": user.userID
                            };
    [NetworkManager getURI:URL_MANAGE_TEMPLATE parameters:param
                   success:^(id data) {
                       
//                       NSMutableArray *categoryList = [Category mapCategoryFromArray:data[@"categories"]].mutableCopy;
                       success(data);
                       
                   }
                   failure:^(NSError *error) {
                       failure(error);
                   }];
    
}

+(void) sendContractWithSenderFirstName:(NSString *)fName
                              lastName:(NSString *)lName
                               userImg:(UIImage *)img
                                  eamil:(NSString *)email
                             templateID:(NSString *)tempId
                               success:(serviceSuccess)success
                               failure:(serviceFailure)failure {
    
    User *user = [DataManager sharedManager].currentUser;
    NSDictionary * userParam = @{
                                 @"user_id":user.userID,
                                 @"template_id":tempId,
                                 @"receiver_first_name":fName,
                                 @"receiver_last_name":lName,
                                 @"receiver_email":email
                                 };
    
    if (img != nil) {

    NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
    
    [NetworkManager postContractURIWithFormData:URL_SEND_DOCUMENT  parameters:userParam formData:imageData success:^(id data) {
        
        BOOL status=data[@"status"];
        if (status) {
            
      
            
            success(data);
        }
    } failure:^(NSError *error) {
        failure(error);
        
    }];
    
    }
    else{
        [NetworkManager postURI:URL_SEND_DOCUMENT parameters:userParam success:^(id data) {
            BOOL status=data[@"status"];
            if (status) {
                
                
                
                success(data);
            }
            
        } failure:^(NSError *error) {
            failure(error);
            
        }];
    
    
    }
}
+(void) saveCreateOwnContractWithReceiverFirstName:(NSString *)fName
                               lastName:(NSString *)lName
                                senderSignImg:(UIImage *)img
                                  eamil:(NSString *)email
                                      TemplateId:(NSString *)templateid
                             templateTitle:(NSString *)tempTitle
                                   Content:(NSString *)content
                                success:(serviceSuccess)success
                                failure:(serviceFailure)failure {
    
    User *user = [DataManager sharedManager].currentUser;

    NSDictionary * userParam;
    if (([lName length]==0||[fName length]==0||[email length]==0) && img==nil) {
        userParam = @{
                                     @"user_id":user.userID,
                                     @"content":content,
                                     @"template_title":tempTitle,
                                     @"template_id":templateid
                                     };
        
        [NetworkManager postURI:URL_CREATE_OWN_AGREEMENT_SAVE parameters:userParam success:^(id data) {
            BOOL status=data[@"status"];
            if (status) {
                
                
                
                success(data);
            }

        } failure:^(NSError *error) {
            failure(error);

        }];

    }
    else if (([lName length]==0||[fName length]==0||[email length]==0) && img!=nil){
        
        userParam = @{
                      @"user_id":user.userID,
                      @"content":content,
                      @"template_title":tempTitle,
                      @"template_id":templateid
                      };
        NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
        
        [NetworkManager postContractURIWithFormData:URL_CREATE_OWN_AGREEMENT_SAVE  parameters:userParam formData:imageData success:^(id data) {
            
            BOOL status=data[@"status"];
            if (status) {
                
                
                
                success(data);
            }
        } failure:^(NSError *error) {
            failure(error);
            
        }];
    
    }
    else {
    
    
   userParam = @{
                                 @"user_id":user.userID,
                                 @"receiver_first_name":fName,
                                 @"receiver_last_name":lName,
                                 @"receiver_email":email,
                                 @"content":content,
                                 @"template_title":tempTitle,
                                 @"template_id":templateid
                                 };
    NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
    
    [NetworkManager postContractURIWithFormData:URL_CREATE_OWN_AGREEMENT_SAVE  parameters:userParam formData:imageData success:^(id data) {
        
        BOOL status=data[@"status"];
        if (status) {
            
            
            
            success(data);
        }
    } failure:^(NSError *error) {
        failure(error);
        
    }];
    }
    
    
}
+(void) sendCreateOwnContractWithReceiverFirstName:(NSString *)fName
                                          lastName:(NSString *)lName
                                             eamil:(NSString *)email
                                     senderSignImg:(UIImage *)img
                                        TemplateId:(NSString *)templateid
                                     templateTitle:(NSString *)tempTitle
                                           Content:(NSString *)content
                                           success:(serviceSuccess)success
                                           failure:(serviceFailure)failure {
    

    User *user = [DataManager sharedManager].currentUser;
    NSDictionary * userParam = @{
                                 @"user_id":user.userID,
                                 @"receiver_first_name":fName,
                                 @"receiver_last_name":lName,
                                 @"receiver_email":email,
                                 @"content":content,
                                 @"template_title":tempTitle,
                                 @"template_id":templateid

                                 };
    if (img !=nil) {
        
    NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
    
    [NetworkManager postContractURIWithFormData:URL_CREATE_OWN_AGREEMENT_SEND  parameters:userParam formData:imageData success:^(id data) {
        
        BOOL status=data[@"status"];
        if (status) {
            
            
            
            success(data);
        }
    } failure:^(NSError *error) {
        failure(error);
        
    }];
    }
    else{
        [NetworkManager postURI:URL_CREATE_OWN_AGREEMENT_SEND parameters:userParam success:^(id data) {
            BOOL status=data[@"status"];
            if (status) {
                
                
                
                success(data);
            }
            
        } failure:^(NSError *error) {
            failure(error);
            
        }];
    
    }
    
}



+(void) sendReceiverSignatureWithTemplateID:(NSString *)tempId
                                userSignImg:(UIImage *)img
                                success:(serviceSuccess)success
                                failure:(serviceFailure)failure {
    
    User *user = [DataManager sharedManager].currentUser;
    NSDictionary * userParam = @{
                                 @"user_id":user.userID,
                                 @"template_id":tempId,
                                 };
    NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
    
    [NetworkManager postReceiverContractURIWithFormData:URL_SEND_RECEIVER_SIGN parameters:userParam formData:imageData success:^(id data) {
        BOOL status = data[@"status"];
        if (status) {
            
            
            
            success(data);
        }
    } failure:^(NSError *error) {
        failure(error);

    }];
    
    
}


+(void) deleteDocumentWithTemplateID:(NSString *)tempId
                                    success:(serviceSuccess)success
                                    failure:(serviceFailure)failure {
    
    User *user = [DataManager sharedManager].currentUser;
    NSDictionary * userParam = @{
                                 @"user_id":user.userID,
                                 @"template_id":tempId,
                                 };
    
    [NetworkManager postURI:URL_DELETE_DOCUMENT parameters:userParam success:^(id data) {
        BOOL status=data[@"status"];
        if (status) {
            
            
            
            success(data);
        }
    } failure:^(NSError *error) {
        failure(error);

    }];
    
    
}



+(void) sendContractWithReceiverInfo:(NSArray *)recInfo
                             userImg:(UIImage *)img
                          templateID:(NSString *)tempId
                             success:(serviceSuccess)success
                             failure:(serviceFailure)failure {
    
    NSMutableArray *infoArray = [[NSMutableArray alloc]init];
    for (Receiver *rec in recInfo) {
        NSDictionary   *recDict = @{
                                    @"receiver_first_name":rec.fName,
                                    @"receiver_last_name":rec.lName,
                                    @"receiver_email":rec.email
                                    
                                    
                                    };
        [infoArray addObject:recDict];
        
    }
    
    User *user = [DataManager sharedManager].currentUser;
    NSDictionary * param;
    if (img == nil) {
                     param = @{
                                 @"user_id":user.userID,
                                 @"template_id":tempId,
                                 @"receivers":infoArray,
                                 };
    }
    else{
                      param = @{
                                 @"user_id":user.userID,
                                 @"template_id":tempId,
                                 @"receivers":infoArray,
                                 @"sender_signature_image": [img base64String]
                                 };
    }
    
    
//    if (img != nil) {
//        
//        NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
//        
//        [NetworkManager postContractURIWithFormData:URL_SEND_DOCUMENT  parameters:userParam formData:imageData success:^(id data) {
//            
//            BOOL status=data[@"status"];
//            if (status) {
//                
//                
//                
//                success(data);
//            }
//        } failure:^(NSError *error) {
//            failure(error);
//            
//        }];
//        
//    }
//    else{
//        [NetworkManager postURI:URL_SEND_DOCUMENT parameters:userParam success:^(id data) {
//            BOOL status=data[@"status"];
//            if (status) {
//                
//                
//                
//                success(data);
//            }
//            
//        } failure:^(NSError *error) {
//            failure(error);
//            
//        }];
    
        
  //  }
    
    
    
    
    
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:param
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if (error) {
        NSLog(@"%@",error);
        
    }
    else{
        NSString *myString = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",myString);
        
    }
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSURL *url = [NSURL URLWithString:@"http://lawnotes.witsapplication.com/user/sendContract"];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            
        
            [SVProgressHUD dismiss];
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int status = [[result objectForKey:@"status"] intValue];
            NSString *message =  [result objectForKey:@"message"];
            if(status == 1)
            {
                
              
                success(result);
            }
        }
        else{
            
            [SVProgressHUD dismiss];
       
            failure(error);
        }
    }];

}

+(void) sendCreateOwnContractWithReceiverInfo:(NSArray *)recInfoArray
                                     senderSignImg:(UIImage *)img
                                        TemplateId:(NSString *)templateid
                                     templateTitle:(NSString *)tempTitle
                                           Content:(NSString *)content
                                           success:(serviceSuccess)success
                                           failure:(serviceFailure)failure {
    
    
    NSMutableArray *infoArray = [[NSMutableArray alloc]init];
    for (Receiver *rec in recInfoArray) {
        NSDictionary   *recDict = @{
                                    @"receiver_first_name":rec.fName,
                                    @"receiver_last_name":rec.lName,
                                    @"receiver_email":rec.email
                                    
                                    
                                    };
        [infoArray addObject:recDict];
        
    }
    

    User *user = [DataManager sharedManager].currentUser;
    
    NSDictionary * param;
    if (img != nil) {
                    param = @{
                                 @"user_id":user.userID,
                                 @"receivers":infoArray,
                                 @"content":content,
                                 @"template_title":tempTitle,
                                 @"template_id":templateid,
                                 @"sender_signature_image": [img base64String]
                                 };
    }
    else{
        param = @{
                  @"user_id":user.userID,
                  @"receivers":infoArray,
                  @"content":content,
                  @"template_title":tempTitle,
                  @"template_id":templateid,
                  };
    }
   
    
    
    
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:param
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if (error) {
        NSLog(@"%@",error);
        
    }
    else{
        NSString *myString = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",myString);
        
    }
    NSString *urlStr = [NSString stringWithFormat:@"http://lawnotes.witsapplication.com%@",URL_CREATE_OWN_AGREEMENT_SEND];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSURL *url = [NSURL URLWithString:urlStr];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            
            
            [SVProgressHUD dismiss];
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            int status = [[result objectForKey:@"status"] intValue];
            NSString *message =  [result objectForKey:@"message"];
            if(status == 1)
            {
                
                
                success(result);
            }
        }
        else{
            
            [SVProgressHUD dismiss];
            
            failure(error);
        }
    }];
    
}


+(void) editPendingTemplateWithTemplateID:(NSString *)tempId
                                    success:(serviceSuccess)success
                                    failure:(serviceFailure)failure {
    
    User *user = [DataManager sharedManager].currentUser;
    NSDictionary * userParam = @{
                                 @"user_id":user.userID,
                                 @"template_id":tempId,
                                 };

    [NetworkManager postURI:URL_EDIT_PENDING_TEMPLATE parameters:userParam success:^(id data) {
        BOOL status = data[@"status"];
        if (status) {
            
            success(data);
        }
    } failure:^(NSError *error) {
        failure(error);
    }];
    
    
}





@end
