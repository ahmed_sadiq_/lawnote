//
//  DetailTextView.h
//  LawNote
//
//  Created by Samreen Noor on 02/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Questions.h"
@protocol DetailTextViewDelegate

@optional
-(void) returnDetail:(Questions *)ques;
//- delegate method;

@end
@interface DetailTextView : UIView

@property (assign)id<DetailTextViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet UITextView *txtDetailView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxViewTopConstrain;
+ (id) loadWithNibForDetailText:(Questions *)question andQuestionNumber:(NSString *)questionNumber;
-(void) show;
@end
