//
//  User.h
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseEntity.h"

@interface User : BaseEntity
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * userID;
@property (nonatomic, strong) NSString * firstName;
@property (nonatomic, strong) NSString * lastName;
@property (strong, nonatomic) NSString  * profilePicture;
@property (nonatomic, strong) NSString * imageURL;







-(id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapUserFromArray:(NSArray *) arrlist;




@end
