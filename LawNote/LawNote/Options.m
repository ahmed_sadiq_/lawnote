//
//  Options.m
//  LawNote
//
//  Created by Samreen Noor on 25/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Options.h"
#import "OptionsQuestion.h"
@implementation Options
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.option_id = [NSString stringWithFormat:@"%@",[self validStringForObject:responseData[@"option_id"]]];
        self.option_text = [self validStringForObject:responseData[@"option_text"]];
        self.option_value =[self validStringForObject:responseData[@"option_value"]];
        self.isSelected = NO;
        self.questionArray = [OptionsQuestion mapQuestionsFromArray:responseData[@"questions"]];

        
        
    }
    
    return self;
}




+ (NSArray *)mapOptionsFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Options alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}

@end
