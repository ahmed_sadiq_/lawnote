//
//  ForgotView.m
//  LawNote
//
//  Created by Apple Txlabz on 03/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ForgotView.h"
#import "AccountService.h"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"
@implementation ForgotView
{
    UIGestureRecognizer *tapper;

}
- (IBAction)forgotPassword:(id)sender {
    if ([self emailValidate:_tfEmail.text]) {
        [AccountService forgetPasswordWhereEmail:_tfEmail.text succuss:^(id data) {
            [self showAlertMessage:data[@"message"]];

        } failure:^(NSError *error) {
            [self showAlertMessage:error.localizedDescription];

        }];
    }
    else{
        [self showAlertMessage:@"Please enter valid email"];

    }
    
    
    
}


- (IBAction)btnBack:(id)sender {
    [self hide];
    
    
    
}


-(void)popBoxConfig{
    _boxView.layer.cornerRadius = 3.0;
    _boxView.layer.masksToBounds = YES;
    _btnReset.layer.cornerRadius = 3.0;
    _btnReset.layer.masksToBounds = YES;
    
    self.tfEmail.text = @"";
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapper];
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.scrollView endEditing:YES];
}

+ (id) loadWithNibForgotPassword{
    
    int device = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)?0:1;

    
    ForgotView *view = (ForgotView *)[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ForgotView class]) owner:nil options:nil][device];
    
    [view setFrame:[UIScreen mainScreen].bounds];
    
    view.alpha = 0.0;
    
    
    return view;
}

-(void) show{
    [self popBoxConfig];
    [[[UIApplication sharedApplication] keyWindow] addSubview:self];
    
    self.alpha = 0.0;
    self.hidden = NO;
    
    [UIView animateWithDuration:0.5 animations:^{
        
        self.alpha = 1.0;
        
    } completion:^(BOOL finished) {
        
    }];
}

-(void) hide{
    [UIView animateWithDuration:0.5 animations:^{
        
        
        self.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        
        self.hidden = YES;
        [self removeFromSuperview];
        
    }];
}
#pragma mark - TextField Methods


-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}


@end
