//
//  RadioBtnView.h
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Questions.h"
@protocol RadioBtnViewDelegate

@optional
-(void) filledRadioBox:(Questions *)question;
//- delegate method;

@end
@interface RadioBtnView : UIView<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *btnInfo;
@property (weak, nonatomic) IBOutlet UIImageView *infoImg;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (assign)id<RadioBtnViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblQuestionCount;
+ (id) loadWithNibForRadioBox:(Questions *)question andQuestionNumber:(NSString *)questionNumber;
-(void) show;

-(void) hide;
@end
