//
//  ManageHeaderCell.m
//  LawNote
//
//  Created by Samreen Noor on 04/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ManageHeaderCell.h"

@implementation ManageHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _btnNewAggrement.layer.cornerRadius = 5; // this value vary as per your desire
    _btnNewAggrement.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
