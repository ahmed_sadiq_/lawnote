//
//  Questions.m
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Questions.h"
#import "Options.h"
@implementation Questions
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.question_id = [NSString stringWithFormat:@"%@",[self validStringForObject:responseData[@"question_id"]]];
        self.question_text = [self validStringForObject:responseData[@"question_text"]];
        self.question_type =[self validStringForObject:responseData[@"question_type"]];
        self.tag_id =[self validStringForObject:responseData[@"tag_id"]];
        self.template_id =[self validStringForObject:responseData[@"template_id"]];
        self.question_description =[self validStringForObject:responseData[@"description"]];

        self.answer =[self validStringForObject:responseData[@"answer"]];
        if (![self.answer isEqualToString:@""]) {
            self.isFilled = YES;
        }
        self.optionArray = [Options mapOptionsFromArray:responseData[@"options"]];
        self.optionAnswerValue = @"";
        self.optionAnswerId =@"" ;

        if (self.optionArray.count>0) {
            for (Options *opt in self.optionArray) {
                if ([self.question_type isEqualToString:@"checkbox"]) {
                    NSArray *items = [self.answer componentsSeparatedByString:@","];
                   self.optionAnswerId = [self validStringForObject:responseData[@"answer"]];
                    self.checkeBoxOptionAnswerIdArray =items.copy;
                    for (NSString *ansId in items) {
                        if ([opt.option_id isEqualToString:ansId]) {
                            
                            [opt setIsSelected:YES];
                      
                        }
                    }

                }
                else{
                if ([opt.option_id isEqualToString:self.answer]) {
                    [opt setIsSelected:YES];
                    self.optionAnswerId = opt.option_id;
                    self.optionAnswerValue = opt.option_value;
                    break;
                }
                }
            }
        }
        
    }
    
    return self;
}




+ (NSArray *)mapQuestionsFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Questions alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}

@end
