//
//  ForgotPasswordVC.h
//  LawNote
//
//  Created by Samreen Noor on 08/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *emailBgImg;

@property (strong, nonatomic) IBOutlet UIView *boxView;

@property (strong, nonatomic) IBOutlet UITextField *tfEmail;

@property (weak, nonatomic) IBOutlet UIButton *btnReset;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end
