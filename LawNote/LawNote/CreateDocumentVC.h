//
//  CreateDocumentVC.h
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditText.h"
#import "CheckBoxView.h"
#import "RadioBtnView.h"
#import "ListView.h"
#import "DateView.h"
#import "Questions.h"
#import "CircleProgressBar.h"
#import "Templates.h"
#import "BaseController.h"

#import "PPSSignatureView.h"
@interface CreateDocumentVC : BaseController <EditTextViewDelegate,CheckBoxViewDelegate,RadioBtnViewDelegate,ListViewDelegate,DateViewDelegate,BaseControllerDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxViewHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userInfoBoxHeightConstrain;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnCreateDoc;
@property (weak, nonatomic) IBOutlet UIButton *btnPreview;
@property (weak, nonatomic) IBOutlet UITableView *infoTabl;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UILabel *lblTampleteDescrption;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIScrollView *questionScroller;
@property (weak, nonatomic) IBOutlet UIView *editView;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelEditing;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) NSMutableArray *questionArray;
@property (strong, nonatomic) Templates *templete;
@property (weak, nonatomic) IBOutlet UIView *saveDocumentView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *boxView;
@property (weak, nonatomic) IBOutlet UIView *boxView1;
@property (nonatomic)  BOOL isfromDraft;
@property (weak, nonatomic) IBOutlet UILabel *lblDocName;
@property (weak, nonatomic) IBOutlet UIView *DocumentDescriptionView;
@property (weak, nonatomic) IBOutlet UIImageView *editLine;
@property (weak, nonatomic) IBOutlet UIImageView *createLine;

@property (weak, nonatomic) IBOutlet UIImageView *previewLine;
@property (nonatomic)  BOOL isChageDoc;
@property (weak, nonatomic) IBOutlet UITextView *lblDocDes;

@property (weak, nonatomic) IBOutlet UILabel *lblRecName;
@property (weak, nonatomic) IBOutlet UILabel *lblRecEmail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeightConstrain;
@property (weak, nonatomic) IBOutlet UIView *boxView2;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveDraft;
@property (weak, nonatomic) IBOutlet UIScrollView *webScroller;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSenderSignature;
@property (weak, nonatomic) IBOutlet UIScrollView *signatureBoxView;

@property (weak, nonatomic) IBOutlet UIView *swipeLabelView;
@property (weak, nonatomic) IBOutlet CircleProgressBar *circleProgressBar;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UITextField *tfFirstName;
@property (weak, nonatomic) IBOutlet UITextField *tfLastName;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet PPSSignatureView *signatureView;
-(void)keyboardWillShow ;
-(void)keyboardWillHide;
@end
