//
//  DetailTextBox.m
//  LawNote
//
//  Created by Samreen Noor on 02/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "DetailTextBox.h"
#import "RCEasyTipView.h"
@implementation DetailTextBox
{
    NSString *returnStr;
    Questions *question;
    UIViewController *viewController;
}

- (IBAction)btnChangeBgView:(id)sender {
    
    self.superview.layer.borderWidth = 1.5;
    self.superview.layer.borderColor = [[UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0] CGColor];
}

- (IBAction)btnInfo:(id)sender {
    
    RCEasyTipPreferences *preferences = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferences.drawing.backgroundColor = [UIColor  colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
    preferences.animating.showDuration = 1.5;
    preferences.animating.dismissDuration = 1.5;
    preferences.animating.dismissTransform = CGAffineTransformMakeTranslation(0, 100);
    preferences.animating.showInitialTransform = CGAffineTransformMakeTranslation(0, -100);
    preferences.shouldDismissOnTouchOutside = YES;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferences];
    tipView.text =question.question_description;

    tipView.delegate = self;
    [tipView showAnimated:YES forView:sender withinSuperView:nil];
    
    
    [self btnChangeBgView:nil];

    
}


- (IBAction)btnAddDetail:(id)sender {
    [self btnChangeBgView:nil];

    DetailTextView *custview;
    if (!custview) {
        custview   = [DetailTextView loadWithNibForDetailText:question andQuestionNumber:@""];
       custview.delegate=self;
        [viewController.view addSubview:custview];
    }
    [custview show];
}

-(void) setupView{
    
//    CALayer *viewLayer = self.txtView.layer;
//    [viewLayer setCornerRadius:5.0];
//    [viewLayer setBorderWidth:1];
//    viewLayer.borderColor=[[UIColor lightGrayColor] CGColor];
    
    _lblQuestion.text = question.question_text;
    _txtView.text = question.answer;
    if ([question.question_description isEqualToString:@""]||[question.question_description isEqual:nil]) {
        _btnInfo.hidden = YES;
        _infoImg.hidden = YES;
    }
}

+ (id) loadWithNibForEditTextWithId:(id) uiviewcolroler questionView:(Questions *)question andQuestionNumber:(NSString *)questionNumber
{
    
    
    DetailTextBox *view = (DetailTextBox *)[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([DetailTextBox class]) owner:nil options:nil][0];
    
    
    [view setFrame:[UIScreen mainScreen].bounds];
    
    view.alpha = 0.0;
    view->viewController =uiviewcolroler;
    view->question = question;
    [view setupView];
    return view;
}

-(void) show{
    
    self.alpha = 1.0;
    
}
-(void)returnDetail:(Questions *)ques{
    _txtView.text = question.answer;

    [self.delegate fillDetailQuestion:question];


}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
