//
//  ReceiveTemplateViewController.m
//  LawNote
//
//  Created by Samreen Noor on 03/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "ReceiveTemplateViewController.h"
#import "UIAlertView+JS.h"
#import "Templates.h"
#import "DocumentCell.h"
#import "CategoryService.h"
#import "ReceiverViewController.h"
#import "DataManager.h"
@interface ReceiveTemplateViewController ()<UITableViewDelegate>

@end

@implementation ReceiveTemplateViewController


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    Templates *temp = sender;
    
    if ([segue.identifier isEqualToString:@"rec" ]) {
        ReceiverViewController  *recVC = segue.destinationViewController;
        recVC.templ = temp;
        recVC.isFromReceiveTempVC = YES;
    }
   
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    [self setupRefreshControl];
        [self getReceiveTemplate];
        
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if([DataManager sharedManager].isChageDoc){
    
        [self getReceiveTemplate];

    }

}

#pragma  mark - IBBAction
- (IBAction)btnMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}




#pragma  mark - tableview Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPAD) {
        return 119;
        
    }
    else
        return 80;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _templateArray.count;
}






- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Templates *temp = _templateArray[indexPath.row];
    
    
    DocumentCell      *cell = [tableView dequeueReusableCellWithIdentifier:
                               @"cell"];
    
    
    cell.lblDocName.text = [temp.template_title uppercaseString];
    cell.lblDescription.text = temp.template_description;
    
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Templates *temp = _templateArray[indexPath.row];
    
    [self performSegueWithIdentifier:@"rec" sender:temp];
    
    
    
}

#pragma mark - Helper Methods

- (void)setupRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor       = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(refreshTemplete:) forControlEvents:UIControlEventValueChanged];
    [_tblView addSubview:_refreshControl];
}

- (void)refreshTemplete:(id)sender
{
    [self getReceiveTemplate];
    
    
}
#pragma mark - Api Calling
-(void) getReceiveTemplate{
    
    [CategoryService getReceiveTemplatesWithsuccess:^(id data) {
        [self.refreshControl endRefreshing];
        
        _templateArray = data;
       // [DataManager sharedManager].receiverTemplateArray = _templateArray.mutableCopy;
        [_tblView reloadData];
    } failure:^(NSError *error) {
        
    }];
    

}




//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        //add code here for when you hit delete
//
//        NSLog(@"row deleted");
//
//
//        Group * group = groupArray[indexPath.row];
//        NSMutableSet *set =groupArray.mutableCopy;
//        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Exit!" message:@"Do you want to leave this group." delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Exit", nil];
//
//
//        [alert showWithBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//
//            if (buttonIndex == 1) {
//
//                [set removeObject:group];
//                groupArray = set.mutableCopy;
//                [_tblView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
//                [_tblView reloadData];
//                [self deleteGroupApi:group.groupId];
//            }
//            else{
//
//            }
//        }] ;
//
//
//    }
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
