//
//  BaseController.m
//  LawNote
//
//  Created by Samreen on 27/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "BaseController.h"
#import "KLCPopup.h"
#import <QuartzCore/QuartzCore.h>


typedef NS_ENUM(NSInteger, FieldTag) {
    FieldTagHorizontalLayout = 1001,
    FieldTagVerticalLayout,
    FieldTagMaskType,
    FieldTagShowType,
    FieldTagDismissType,
    FieldTagBackgroundDismiss,
    FieldTagContentDismiss,
    FieldTagTimedDismiss,
};


typedef NS_ENUM(NSInteger, CellType) {
    CellTypeNormal = 0,
    CellTypeSwitch,
};


@interface BaseController ()
{
    
    NSArray* _fields;
    NSDictionary* _namesForFields;
    
    NSArray* _horizontalLayouts;
    NSArray* _verticalLayouts;
    NSArray* _maskTypes;
    NSArray* _showTypes;
    NSArray* _dismissTypes;
    
    NSDictionary* _namesForHorizontalLayouts;
    NSDictionary* _namesForVerticalLayouts;
    NSDictionary* _namesForMaskTypes;
    NSDictionary* _namesForShowTypes;
    NSDictionary* _namesForDismissTypes;
    
    NSInteger _selectedRowInHorizontalField;
    NSInteger _selectedRowInVerticalField;
    NSInteger _selectedRowInMaskField;
    NSInteger _selectedRowInShowField;
    NSInteger _selectedRowInDismissField;
    BOOL _shouldDismissOnBackgroundTouch;
    BOOL _shouldDismissOnContentTouch;
    BOOL _shouldDismissAfterDelay;
}

@property (nonatomic, strong) UITableView* tableView;
@property (nonatomic, strong) UIPopoverController* popover;

// Event handlers
- (void)toggleValueDidChange:(id)sender;
- (void)showButtonPressed:(id)sender;
- (void)dismissButtonPressed:(id)sender;
- (void)fieldCancelButtonPressed:(id)sender;

@end


@interface UIColor (KLCPopupExample)
+ (UIColor*)klcLightGreenColor;
+ (UIColor*)klcGreenColor;
@end


@interface UIView (KLCPopupExample)
- (UITableViewCell*)parentCell;

@end

@implementation BaseController

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.title = @"KLCPopup Example";
        
        // MAIN LIST
        _fields = @[@(FieldTagHorizontalLayout),
                    @(FieldTagVerticalLayout),
                    @(FieldTagMaskType),
                    @(FieldTagShowType),
                    @(FieldTagDismissType),
                    @(FieldTagBackgroundDismiss),
                    @(FieldTagContentDismiss),
                    @(FieldTagTimedDismiss)];
        
        _namesForFields = @{@(FieldTagHorizontalLayout) : @"Horizontal layout",
                            @(FieldTagVerticalLayout) : @"Vertical layout",
                            @(FieldTagMaskType) : @"Background mask",
                            @(FieldTagShowType) : @"Show type",
                            @(FieldTagDismissType) : @"Dismiss type",
                            @(FieldTagBackgroundDismiss) : @"Dismiss on background touch",
                            @(FieldTagContentDismiss) : @"Dismiss on content touch",
                            @(FieldTagTimedDismiss) : @"Dismiss after delay"};
        
        // FIELD SUB-LISTS
        _horizontalLayouts = @[@(KLCPopupHorizontalLayoutLeft),
                               @(KLCPopupHorizontalLayoutLeftOfCenter),
                               @(KLCPopupHorizontalLayoutCenter),
                               @(KLCPopupHorizontalLayoutRightOfCenter),
                               @(KLCPopupHorizontalLayoutRight)];
        
        _namesForHorizontalLayouts = @{@(KLCPopupHorizontalLayoutLeft) : @"Left",
                                       @(KLCPopupHorizontalLayoutLeftOfCenter) : @"Left of Center",
                                       @(KLCPopupHorizontalLayoutCenter) : @"Center",
                                       @(KLCPopupHorizontalLayoutRightOfCenter) : @"Right of Center",
                                       @(KLCPopupHorizontalLayoutRight) : @"Right"};
        
        _verticalLayouts = @[@(KLCPopupVerticalLayoutTop),
                             @(KLCPopupVerticalLayoutAboveCenter),
                             @(KLCPopupVerticalLayoutCenter),
                             @(KLCPopupVerticalLayoutBelowCenter),
                             @(KLCPopupVerticalLayoutBottom)];
        
        _namesForVerticalLayouts = @{@(KLCPopupVerticalLayoutTop) : @"Top",
                                     @(KLCPopupVerticalLayoutAboveCenter) : @"Above Center",
                                     @(KLCPopupVerticalLayoutCenter) : @"Center",
                                     @(KLCPopupVerticalLayoutBelowCenter) : @"Below Center",
                                     @(KLCPopupVerticalLayoutBottom) : @"Bottom"};
        
        _maskTypes = @[@(KLCPopupMaskTypeNone),
                       @(KLCPopupMaskTypeClear),
                       @(KLCPopupMaskTypeDimmed)];
        
        _namesForMaskTypes = @{@(KLCPopupMaskTypeNone) : @"None",
                               @(KLCPopupMaskTypeClear) : @"Clear",
                               @(KLCPopupMaskTypeDimmed) : @"Dimmed"};
        
        _showTypes = @[@(KLCPopupShowTypeNone),
                       @(KLCPopupShowTypeFadeIn),
                       @(KLCPopupShowTypeGrowIn),
                       @(KLCPopupShowTypeShrinkIn),
                       @(KLCPopupShowTypeSlideInFromTop),
                       @(KLCPopupShowTypeSlideInFromBottom),
                       @(KLCPopupShowTypeSlideInFromLeft),
                       @(KLCPopupShowTypeSlideInFromRight),
                       @(KLCPopupShowTypeBounceIn),
                       @(KLCPopupShowTypeBounceInFromTop),
                       @(KLCPopupShowTypeBounceInFromBottom),
                       @(KLCPopupShowTypeBounceInFromLeft),
                       @(KLCPopupShowTypeBounceInFromRight)];
        
        _namesForShowTypes = @{@(KLCPopupShowTypeNone) : @"None",
                               @(KLCPopupShowTypeFadeIn) : @"Fade in",
                               @(KLCPopupShowTypeGrowIn) : @"Grow in",
                               @(KLCPopupShowTypeShrinkIn) : @"Shrink in",
                               @(KLCPopupShowTypeSlideInFromTop) : @"Slide from Top",
                               @(KLCPopupShowTypeSlideInFromBottom) : @"Slide from Bottom",
                               @(KLCPopupShowTypeSlideInFromLeft) : @"Slide from Left",
                               @(KLCPopupShowTypeSlideInFromRight) : @"Slide from Right",
                               @(KLCPopupShowTypeBounceIn) : @"Bounce in",
                               @(KLCPopupShowTypeBounceInFromTop) : @"Bounce from Top",
                               @(KLCPopupShowTypeBounceInFromBottom) : @"Bounce from Bottom",
                               @(KLCPopupShowTypeBounceInFromLeft) : @"Bounce from Left",
                               @(KLCPopupShowTypeBounceInFromRight) : @"Bounce from Right"};
        
        _dismissTypes = @[@(KLCPopupDismissTypeNone),
                          @(KLCPopupDismissTypeFadeOut),
                          @(KLCPopupDismissTypeGrowOut),
                          @(KLCPopupDismissTypeShrinkOut),
                          @(KLCPopupDismissTypeSlideOutToTop),
                          @(KLCPopupDismissTypeSlideOutToBottom),
                          @(KLCPopupDismissTypeSlideOutToLeft),
                          @(KLCPopupDismissTypeSlideOutToRight),
                          @(KLCPopupDismissTypeBounceOut),
                          @(KLCPopupDismissTypeBounceOutToTop),
                          @(KLCPopupDismissTypeBounceOutToBottom),
                          @(KLCPopupDismissTypeBounceOutToLeft),
                          @(KLCPopupDismissTypeBounceOutToRight)];
        
        _namesForDismissTypes = @{@(KLCPopupDismissTypeNone) : @"None",
                                  @(KLCPopupDismissTypeFadeOut) : @"Fade out",
                                  @(KLCPopupDismissTypeGrowOut) : @"Grow out",
                                  @(KLCPopupDismissTypeShrinkOut) : @"Shrink out",
                                  @(KLCPopupDismissTypeSlideOutToTop) : @"Slide to Top",
                                  @(KLCPopupDismissTypeSlideOutToBottom) : @"Slide to Bottom",
                                  @(KLCPopupDismissTypeSlideOutToLeft) : @"Slide to Left",
                                  @(KLCPopupDismissTypeSlideOutToRight) : @"Slide to Right",
                                  @(KLCPopupDismissTypeBounceOut) : @"Bounce out",
                                  @(KLCPopupDismissTypeBounceOutToTop) : @"Bounce to Top",
                                  @(KLCPopupDismissTypeBounceOutToBottom) : @"Bounce to Bottom",
                                  @(KLCPopupDismissTypeBounceOutToLeft) : @"Bounce to Left",
                                  @(KLCPopupDismissTypeBounceOutToRight) : @"Bounce to Right"};
        
        // DEFAULTS
        _selectedRowInHorizontalField = [_horizontalLayouts indexOfObject:@(KLCPopupHorizontalLayoutCenter)];
        _selectedRowInVerticalField = [_verticalLayouts indexOfObject:@(KLCPopupVerticalLayoutCenter)];
        _selectedRowInMaskField = [_maskTypes indexOfObject:@(KLCPopupMaskTypeDimmed)];
        _selectedRowInShowField = [_showTypes indexOfObject:@(KLCPopupShowTypeBounceInFromTop)];
        _selectedRowInDismissField = [_dismissTypes indexOfObject:@(KLCPopupDismissTypeBounceOutToBottom)];
        _shouldDismissOnBackgroundTouch = YES;
        _shouldDismissOnContentTouch = NO;
        _shouldDismissAfterDelay = NO;
    }
    return self;
}


- (void)loadView {
    [super loadView];
    

    // FOOTER
   }


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = YES;
    self.view.backgroundColor = [UIColor whiteColor];
}


#pragma mark - Event Handlers

- (void)toggleValueDidChange:(id)sender {
    
    if ([sender isKindOfClass:[UISwitch class]]) {
        UISwitch* toggle = (UISwitch*)sender;
        
        NSIndexPath* indexPath = [self.tableView indexPathForCell:[toggle parentCell]];
        id obj = [_fields objectAtIndex:indexPath.row];
        if ([obj isKindOfClass:[NSNumber class]]) {
            
            NSInteger fieldTag = [(NSNumber*)obj integerValue];
            if (fieldTag == FieldTagBackgroundDismiss) {
                _shouldDismissOnBackgroundTouch = toggle.on;
                
            } else if (fieldTag == FieldTagContentDismiss) {
                _shouldDismissOnContentTouch = toggle.on;
                
            } else if (fieldTag == FieldTagTimedDismiss) {
                _shouldDismissAfterDelay = toggle.on;
            }
        }
    }
}


- (void)showButtonPressed:(id )controller {
    self.delegate = controller;
    // Generate content view to present
    UIView* contentView = [[UIView alloc] init];
    contentView.translatesAutoresizingMaskIntoConstraints = NO;
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.layer.cornerRadius = 12.0;
    
    UIButton* dismissButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton1.translatesAutoresizingMaskIntoConstraints = NO;
    dismissButton1.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    dismissButton1.backgroundColor = [UIColor colorWithRed:37.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1];
    [dismissButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dismissButton1 setTitleColor:[[dismissButton1 titleColorForState:UIControlStateNormal] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    dismissButton1.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    [dismissButton1 setTitle:@" Sign Now and Send " forState:UIControlStateNormal];
    dismissButton1.layer.cornerRadius = 6.0;
    [dismissButton1 addTarget:self action:@selector(sendWithSign:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton* dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton.translatesAutoresizingMaskIntoConstraints = NO;
    dismissButton.contentEdgeInsets = UIEdgeInsetsMake(10, 20, 10, 20);
    dismissButton.backgroundColor = [UIColor colorWithRed:37.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1];
    [dismissButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dismissButton setTitleColor:[[dismissButton titleColorForState:UIControlStateNormal] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    dismissButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    [dismissButton setTitle:@"Send Now and Sign Later" forState:UIControlStateNormal];
    dismissButton.layer.cornerRadius = 6.0;
    [dismissButton addTarget:self action:@selector(sendWithOutSign:) forControlEvents:UIControlEventTouchUpInside];
    
    [contentView addSubview:dismissButton1];
    [contentView addSubview:dismissButton];
    
    NSDictionary* views = NSDictionaryOfVariableBindings(contentView, dismissButton, dismissButton1);
    
    [contentView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(16)-[dismissButton1]-(10)-[dismissButton]-(24)-|"
                                             options:NSLayoutFormatAlignAllCenterX
                                             metrics:nil
                                               views:views]];
    
    [contentView addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(36)-[dismissButton1]-(36)-|"
                                             options:0
                                             metrics:nil
                                               views:views]];
    
    // Show in popup
    KLCPopupLayout layout = KLCPopupLayoutMake((KLCPopupHorizontalLayout)3,
                                               (KLCPopupVerticalLayout)3);
    
    KLCPopup* popup = [KLCPopup popupWithContentView:contentView
                                            showType:(KLCPopupShowType)8
                                         dismissType:(KLCPopupDismissType)8
                                            maskType:(KLCPopupMaskType)2
                            dismissOnBackgroundTouch:YES
                               dismissOnContentTouch:NO];
    
   
        [popup showWithLayout:layout];
    
}

- (void)dismissButtonPressed:(id)sender {
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
}
- (void)sendWithOutSign:(id)sender {
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
    [self.delegate sendContractWithSignature:NO];
    
}
- (void)sendWithSign:(id)sender {
    if ([sender isKindOfClass:[UIView class]]) {
        [(UIView*)sender dismissPresentingPopup];
    }
    [self.delegate sendContractWithSignature:YES];

}

- (void)fieldCancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - <UIPopoverControllerDelegate>

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    
    // Cleanup by removing KVO and reference to popover
    UIView* view = popoverController.contentViewController.view;
    if ([view isKindOfClass:[UITableView class]]) {
        [(UITableView*)view removeObserver:self forKeyPath:@"contentSize"];
    }
    
    self.popover = nil;
}


#pragma mark - <NSKeyValueObserving>

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"contentSize"]) {
        
        if ([object isKindOfClass:[UITableView class]]) {
            UITableView* tableView = (UITableView*)object;
            
            if (self.popover != nil) {
                [self.popover setPopoverContentSize:tableView.contentSize animated:NO];
            }
            
            // Make sure the selected row is scrolled into view when it appears
         
        }
    }
}


@end


#pragma mark - Categories

@implementation UIColor (KLCPopupExample)

+ (UIColor*)klcLightGreenColor {
    return [UIColor colorWithRed:(184.0/255.0) green:(233.0/255.0) blue:(122.0/255.0) alpha:1.0];
}

+ (UIColor*)klcGreenColor {
    return [UIColor colorWithRed:(0.0/255.0) green:(204.0/255.0) blue:(134.0/255.0) alpha:1.0];
}

@end



@implementation UIView (KLCPopupExample)

- (UITableViewCell*)parentCell {
    
    // Iterate over superviews until you find a UITableViewCell
    UIView* view = self;
    while (view != nil) {
        if ([view isKindOfClass:[UITableViewCell class]]) {
            return (UITableViewCell*)view;
        } else {
            view = [view superview];
        }
    }
    return nil;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
