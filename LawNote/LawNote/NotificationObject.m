//
//  NotificationObject.m
//  LawNote
//
//  Created by Samreen Noor on 11/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "NotificationObject.h"

@implementation NotificationObject
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        
        self.catImg = [self completeImageURL:responseData[@"category_icon"]];
        self.template_url_view = [self completeImageURL:responseData[@"template_url_view"]];

        self.template_id = [self validStringForObject:responseData[@"template_id"]];
        self.template_title =[self validStringForObject:responseData[@"template_title"]];
        self.template_description =[self validStringForObject:responseData[@"template_url"]];
        self.message =[self validStringForObject:responseData[@"message"]];
        self.flag =[NSString stringWithFormat:@"%@",[self validStringForObject:responseData[@"flag"]]];

        
        
    }
    
    return self;
}




+ (NSArray *)mapNotificationFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[NotificationObject alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}

@end
