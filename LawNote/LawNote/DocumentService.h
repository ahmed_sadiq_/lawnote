//
//  DocumentService.h
//  LawNote
//
//  Created by Samreen Noor on 27/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseService.h"

@interface DocumentService : BaseService
+(void) saveFilledDocumentWithAnswersArray:(NSArray *)answerArray
                             andTemplateId:(NSString *)templateId
                                   success:(serviceSuccess)success
                                   failure:(serviceFailure)failure;
+(void) getManagedDocumentWithsuccess:(serviceSuccess) success
                              failure:(serviceFailure) failure;

+(void) sendContractWithSenderFirstName:(NSString *)fName
                               lastName:(NSString *)lName
                                userImg:(UIImage *)img
                                  eamil:(NSString *)email
                             templateID:(NSString *)tempId
                                success:(serviceSuccess)success
                                failure:(serviceFailure)failure;

+(void) saveCreateOwnContractWithReceiverFirstName:(NSString *)fName
                                          lastName:(NSString *)lName
                                     senderSignImg:(UIImage *)img
                                             eamil:(NSString *)email
                                        TemplateId:(NSString *)templateid
                                     templateTitle:(NSString *)tempTitle
                                           Content:(NSString *)content
                                           success:(serviceSuccess)success
                                           failure:(serviceFailure)failure ;

+(void) sendCreateOwnContractWithReceiverFirstName:(NSString *)fName
                                        lastName:(NSString *)lName
                                             eamil:(NSString *)email
                                     senderSignImg:(UIImage *)img
                                      TemplateId:(NSString *)templateid
                                   templateTitle:(NSString *)tempTitle
                                         Content:(NSString *)content
                                         success:(serviceSuccess)success
                                         failure:(serviceFailure)failure;
+(void) sendReceiverSignatureWithTemplateID:(NSString *)tempId
                                userSignImg:(UIImage *)img
                                    success:(serviceSuccess)success
                                    failure:(serviceFailure)failure;
+(void) deleteDocumentWithTemplateID:(NSString *)tempId
                             success:(serviceSuccess)success
                             failure:(serviceFailure)failure;
+(void) sendContractWithReceiverInfo:(NSArray *)recInfo
                             userImg:(UIImage *)img
                          templateID:(NSString *)tempId
                             success:(serviceSuccess)success
                             failure:(serviceFailure)failure;

+(void) sendCreateOwnContractWithReceiverInfo:(NSArray *)recInfoArray
                                senderSignImg:(UIImage *)img
                                   TemplateId:(NSString *)templateid
                                templateTitle:(NSString *)tempTitle
                                      Content:(NSString *)content
                                      success:(serviceSuccess)success
                                      failure:(serviceFailure)failure;


+(void) editPendingTemplateWithTemplateID:(NSString *)tempId
                                  success:(serviceSuccess)success
                                  failure:(serviceFailure)failure;
@end
