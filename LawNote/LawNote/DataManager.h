//
//  DataManager.h
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Templates.h"
#import "RKNotificationHub.h"

@interface DataManager : NSObject
@property (strong, nonatomic) RKNotificationHub *hub;
@property (strong, nonatomic) RKNotificationHub *receivedHub;
@property (strong, nonatomic) RKNotificationHub *completedHub;

@property (strong, nonatomic) Templates *receiverTemplate;
@property (strong, nonatomic) User *currentUser;
@property (strong, nonatomic) NSString *selectedCtaegory ;
@property (nonatomic) BOOL isCompleted;
@property (nonatomic) BOOL isHomeFirstTime;
@property (nonatomic)  BOOL isChangeInReceived;
 @property (nonatomic) BOOL isdeviceRegister;
@property (strong, nonatomic) UITextView *selectedView;
@property (assign, nonatomic) int notificationToTalCount;
@property (assign, nonatomic) int notifReceiveCount;
@property (assign, nonatomic) int notifCompleteCount;

@property (strong, nonatomic) NSString *authToken;
@property (strong, nonatomic) NSString *deviceToken;
@property (strong, nonatomic) NSMutableArray *categoryArray;
@property (strong, nonatomic) NSMutableArray *catItemArray;
@property (strong, nonatomic) NSMutableArray *notificationArray;

@property ( nonatomic) BOOL ischnagePasswordView;
@property (nonatomic)  BOOL isChageDoc;
@property (nonatomic)  BOOL isFromDraftSend;

+ (DataManager *) sharedManager;

@end
