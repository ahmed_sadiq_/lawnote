//
//  MutipleOptionTextView.m
//  LawNote
//
//  Created by Samreen Noor on 05/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "MutipleOptionTextView.h"
#import "OptionTextViewCell.h"
#import "OptionTextHeaderViewCell.h"
#import "Options.h"
#import "Questions.h"
#import "DataManager.h"
#import "RCEasyTipView.h"
@implementation MutipleOptionTextView
{
    NSString *returnStr;
    NSUInteger currentSelectedIndex;
    Questions *question;

}


- (IBAction)btnInfo:(id)sender {
    
    RCEasyTipPreferences *preferences = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferences.drawing.backgroundColor = [UIColor  colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
    preferences.animating.showDuration = 1.5;
    preferences.animating.dismissDuration = 1.5;
    preferences.animating.dismissTransform = CGAffineTransformMakeTranslation(0, 100);
    preferences.animating.showInitialTransform = CGAffineTransformMakeTranslation(0, -100);
    preferences.shouldDismissOnTouchOutside = YES;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferences];
    tipView.text =question.question_description;
    tipView.delegate = self;
    [tipView showAnimated:YES forView:sender withinSuperView:nil];
    
    [self btnChnageBgView:nil];
    
    
}
- (IBAction)btnChnageBgView:(id)sender {
    
    self.superview.layer.borderWidth = 1.5;
    self.superview.layer.borderColor = [[UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0] CGColor];
    
}

-(void) setupView{
    self.lblQuestion.text = question.question_text;
    currentSelectedIndex = 10;
    //////////////// to adjust view movement on keyboard show/////////
    UITextView *utxt = [[UITextView alloc]init];
    [utxt setTag:0];
    [DataManager sharedManager].selectedView  = utxt;
    if ([question.question_description isEqualToString:@""]||[question.question_description isEqual:nil]) {
        _btnInfo.hidden = YES;
        _infoImg.hidden = YES;
    }
}
+ (id) loadWithNibForRadioBox:(Questions *)question andQuestionNumber:(NSString *)questionNumber{
    // int device = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)?0:1;
    
    
    MutipleOptionTextView *view = (MutipleOptionTextView *)[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MutipleOptionTextView class]) owner:nil options:nil][0];
    
    [view setFrame:[UIScreen mainScreen].bounds];
    
    view.alpha = 0.0;
    
    view->question = question;
    
    [view setupView];
    return view;
}

-(void) show{
    
    
    self.alpha = 1.0;
    
    
}


-(void) buttonOptionClicked:(UIButton*)sender{
    currentSelectedIndex = sender.tag;
    Options *option = [question.optionArray objectAtIndex:currentSelectedIndex];
    option.isSelected = YES;
    for (Options *o in question.optionArray) {
        if (! [o.option_id isEqualToString:option.option_id]) {
            o.isSelected = NO;
            
        }
    }
    [_tblView reloadData];
    [question setIsFilled:YES];
    [question setAnswer:option.option_text];
    [question setOptionAnswerValue:option.option_value];
    
    [question setOptionAnswerId:option.option_id];
    [self btnChnageBgView:nil];

   // [self.delegate filledRadioMultilevelBox:question];
}
#pragma mark - TableView Methods
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    Options *option = [question.optionArray objectAtIndex:section];

    OptionTextHeaderViewCell   *headerView;
    headerView = [[[NSBundle mainBundle] loadNibNamed:@"OptionTextHeaderViewCell" owner:self options:nil] objectAtIndex:0];

    headerView.btnOption.tag = section;
    [headerView.btnOption addTarget:self action:@selector(buttonOptionClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    CGSize stringsize = [option.option_text sizeWithFont:[UIFont systemFontOfSize:16]];
    //or whatever font you're using
    [headerView.btnOption setFrame:CGRectMake(10,5,stringsize.width+40, 35)];
    
    [headerView.btnOption setTitle:option.option_text forState:UIControlStateNormal];
    if (option.isSelected) {
        [headerView.btnOption setTitleColor:[UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0] forState:UIControlStateNormal] ;
        [headerView.btnOption setBackgroundColor:[UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:0.25]];
     

    }
    else{
        [headerView.btnOption setBackgroundColor:[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:0.5]];
        [headerView.btnOption setTitleColor:[UIColor blackColor] forState:UIControlStateNormal] ;
        
    }
    

    
    return headerView;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return question.optionArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPAD) {
        return 119;
        
    }
    else{
        if (currentSelectedIndex == indexPath.section) {
            return 40;
        }
        else
            return 0;
        
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (currentSelectedIndex == section) {
        Options *option = [question.optionArray objectAtIndex:section];

        return  option.questionArray.count;
 
    }
    else
    return  0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Options *option = question.optionArray[indexPath.section];
    Questions *ques = option.questionArray[indexPath.row];

   
    
    OptionTextViewCell * cell = (OptionTextViewCell *)[_tblView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"OptionTextViewCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    cell.txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:ques.question_text];

    cell.backgroundColor = [UIColor whiteColor];
    cell.txtField.text = ques.answer;
    cell.txtField.tag = indexPath.row;
    cell.txtField.delegate = self;

    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}


#pragma mark - Textfield Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{

}
-(void)textFieldDidEndEditing:(UITextField *)textField{

    Options *option = [question.optionArray objectAtIndex:currentSelectedIndex];
    Questions *ques = option.questionArray[textField.tag];

    if (![textField.text isEqualToString:@""]) {

        
        [ques setAnswer:textField.text];
        [ques setIsFilled:YES];
        
    }
    else{
        [ques setAnswer:textField.text];
        [ques setIsFilled:NO];
        
    }
    [self setUpProgress];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];

    return true;
}
-(void) setUpProgress{
    Options *option = [question.optionArray objectAtIndex:currentSelectedIndex];
    for (Questions *q in option.questionArray) {
        if (q.isFilled) {
            question.isFilled = YES;
        }
        else{
            question.isFilled = NO;

        }
    }
    [self.delegate filledRadioMultilevelBox:question];


}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
