//
//  OptionTextViewCell.h
//  LawNote
//
//  Created by Samreen Noor on 05/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionTextViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *txtField;

@end
