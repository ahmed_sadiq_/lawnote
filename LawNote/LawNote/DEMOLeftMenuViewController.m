//
//  DEMOMenuViewController.m
//  RESideMenuExample
//
//  Created by Roman Efimov on 10/10/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOLeftMenuViewController.h"
#import "UIImageView+RoundImage.h"
#import "MenuCell.h"
#import "DataManager.h"
#import "User.h"
#import "UIImageView+URL.h"
#import "AccountService.h"
#import "ManageViewController.h"
#import "DataManager.h"
#import "SettingViewController.h"
#import "ReceiveTemplateViewController.h"
@interface DEMOLeftMenuViewController ()



@end

@implementation DEMOLeftMenuViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _profileImage.layer.cornerRadius = 35 ;
    _profileImage.layer.masksToBounds = YES;

    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"];
    NSString *userEmial = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_email"];
NSString *img = [[NSUserDefaults standardUserDefaults] objectForKey:@"profilePic"];
    [_profileImage setImageWithURL:img];
    _userName.text =  userName;
    _userEmail.text =userEmial;
    if ([DataManager sharedManager].receiverTemplate.isReceiver) {
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ManageViewController"]]
                                                     animated:YES];
        [self.sideMenuViewController hideMenuViewController];
    }

}

-(void)viewWillAppear:(BOOL)animated{
    NSString *img = [[NSUserDefaults standardUserDefaults] objectForKey:@"profilePic"];
    [_profileImage setImageWithURL:img];
    [_menuTblView reloadData];
}
#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    switch (indexPath.row) {
//        case 0:
//            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[DEMOFirstViewController alloc] init]]
//                                                         animated:YES];
//            [self.sideMenuViewController hideMenuViewController];
//            break;
//        case 1:
//            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[DEMOSecondViewController alloc] init]]
//                                                         animated:YES];
//            [self.sideMenuViewController hideMenuViewController];
//            break;
//        default:
//            break;
//    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"firstViewController"]]
                                                         animated:YES];
            [DataManager sharedManager].isChageDoc = YES;
            [DataManager sharedManager].isChangeInReceived = NO;


            [self.sideMenuViewController hideMenuViewController];
            break;
        case 1:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"CatalogViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
      

        case 2:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ManageViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 3:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ReceiveTemplateViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 4:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
         case 5:
        {

            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"logged_in"];
            if (IS_IPAD) {
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Ipad" bundle:nil];
                UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"navLogin"];
                [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
            }
            else{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"navLogin"];
            [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
            }
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_name"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_lastName"];

            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_email"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_id"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profilePic"];

            [[NSUserDefaults standardUserDefaults] synchronize];
            [[DataManager sharedManager].receiverTemplate setIsReceiver:NO];
            [DataManager sharedManager].isCompleted= NO;
            [[DataManager sharedManager].notificationArray removeAllObjects];

//            NSArray *array = [self.navigationController viewControllers];
//            [self.navigationController popToViewController:[array objectAtIndex:0] animated:YES];
            break;
        }
            

            
        default:
            break;
    }

}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD) {
        return 80;

    }
    else
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"menucell";
    
 //   MenuCell * cell = (MenuCell *)[_menuTblView dequeueReusableCellWithIdentifier:@"menucell"];
//    if (cell == nil) {
//        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MenuCell" owner:self options:nil];
//        cell=[nib objectAtIndex:0];
//    }
    
    MenuCell * cell= [tableView dequeueReusableCellWithIdentifier:
     cellIdentifier];

    [cell setBackgroundColor:[UIColor clearColor]];

    NSArray *titles = @[@"Home", @"Catalog",  @"Manage",@"Received",@"Setting", @"Log Out"];
    NSArray *images = @[@"home", @"draft", @"managed",@"received",@"settings", @"logout"];
    cell.lbl.text = titles[indexPath.row];
    cell.img.image = [UIImage imageNamed:images[indexPath.row]];
    if (indexPath.row==2) {
        [ [DataManager sharedManager].completedHub setCount:0];

    [DataManager sharedManager].completedHub = [[RKNotificationHub alloc]initWithView:cell.img];
    
    [ [DataManager sharedManager].completedHub incrementBy:[DataManager sharedManager].notifCompleteCount];
        [[DataManager sharedManager].completedHub moveCircleByX:120 Y:6];

    }
    else if(indexPath.row == 3){
        [ [DataManager sharedManager].receivedHub setCount:0];

        [DataManager sharedManager].receivedHub = [[RKNotificationHub alloc]initWithView:cell.img];
        
        [ [DataManager sharedManager].receivedHub incrementBy:[DataManager sharedManager].notifReceiveCount];
        [[DataManager sharedManager].receivedHub moveCircleByX:120 Y:6];
        NSLog(@"%d",[DataManager sharedManager].notifReceiveCount);
    }
    
    return cell;
}
- (IBAction)btnUpdateProfile:(id)sender {

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
    
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSLog(@"image dictonary : %@", info);
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    
        if (image == nil){
            NSLog(@"Image with some error.");
    }
    
    
    
   //_profileImage.image  = [image imageWithScaledToSize:CGSizeMake(204, 204)];
    
    if (image) {
        
        [_profileImage setImage:image];
        [AccountService updateUserProfileWithFirstName:nil lastName:nil userImg:image success:^(id data) {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:data[@"message"] delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
            [alertView show];
            
        } failure:^(NSError *error) {
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
                [alertView show];
        }];
     
    }
    
}
@end
