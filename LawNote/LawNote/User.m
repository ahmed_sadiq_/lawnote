//
//  User.m
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "User.h"

@implementation User
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.userID  = [self validStringForObject:responseData [@""]];
        self.name  = [self validStringForObject:responseData [@""]];
        self.email  = [self validStringForObject:responseData [@"email"]];
        self.profilePicture = [self completeImageURL:responseData [@"image_path"]];
        
    }
    
    return self;
}




+ (NSArray *)mapUserFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[User alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
}

@end
