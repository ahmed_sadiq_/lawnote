//
//  QuestionViewCell.m
//  LawNote
//
//  Created by Samreen Noor on 23/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "QuestionViewCell.h"

@implementation QuestionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.QuestionBox.layer.masksToBounds = NO;
    self.QuestionBox.layer.shadowOffset = CGSizeMake(-3, 2);
    self.QuestionBox.layer.shadowRadius = 4;
    self.QuestionBox.layer.shadowOpacity = 0.4;
    self.QuestionBox.layer.borderWidth = 1;
    self.QuestionBox.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
