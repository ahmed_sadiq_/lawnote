//
//  HomeViewController.m
//  LawNote
//
//  Created by Apple Txlabz on 13/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "HomeViewController.h"
#import "UIImageView+URL.h"
#import "Category.h"
#import "AccountService.h"
#import "CatalogViewController.h"
#import "documentVC.h"
#import "ContextMenuCell.h"
#import "YALContextMenuTableView.h"
#import "DataManager.h"
#import "RKNotificationHub.h"
#import "NotificationObject.h"
#import "ReceiverViewController.h"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"

static NSString *const menuCellIdentifier = @"rotationCell";

@interface HomeViewController ()<NavigationHandlerDelegate,SwipeDelegate,SwipeViewDataSource,
UITableViewDelegate,
UITableViewDataSource,
YALContextMenuTableViewDelegate
>
{    Category *cat1;
    Category *cat2;
    Category *cat3;
    Category *selectedCat;
    NSMutableArray *receivedArray;
    NavigationHandler *navCv;
    NotificationObject *selectednotif;
  
}
@property (nonatomic, strong) YALContextMenuTableView* contextMenuTableView;

@property (nonatomic, strong) NSArray *menuTitles;
@property (nonatomic, strong) NSArray *menuIcons;
@property (weak, nonatomic) IBOutlet SwipeView *swipeView;
@property (assign, nonatomic) NSInteger count;

@end

@implementation HomeViewController

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    

    if ([segue.identifier isEqualToString:@"more" ]) {
        CatalogViewController  *catVC = segue.destinationViewController;
        catVC.catArray = _categoryArray.mutableCopy;
        
    }
    if ([segue.identifier isEqualToString:@"detail" ]) {
        Templates *temp = [[Templates alloc]init];
        temp.template_id = selectednotif.template_id;
        temp.template_title = selectednotif.template_title;
        temp.template_description = selectednotif.template_description;
        temp.template_url_view = selectednotif.template_url_view;
        ReceiverViewController  *recVC = segue.destinationViewController;
        recVC.templ = temp;
        if ([selectednotif.flag isEqualToString:@"1"]||[selectednotif.flag isEqualToString:@"3"]) {
            recVC.isFromReceiveTempVC = YES;

        }
        else{
            recVC.isCompleted = YES;

        }

        
    }
    else  if ([segue.identifier isEqualToString:@"document" ]) {
        documentVC  *docVC = segue.destinationViewController;
        docVC.templateArray = sender;
        docVC.category = selectedCat;
        if ([DataManager sharedManager].isChageDoc) {
            docVC.isFromCategory = YES;
        }
        else{
        docVC.isFromCategory = NO;
        }
    }
    
    
}

#pragma mark - ViewController setup



- (void)viewDidLoad {
    [super viewDidLoad];
    if (![DataManager sharedManager ].isdeviceRegister) {
        [self registerDevice];

    }
    selectedCat = [[Category alloc]init];
    self.count = 1;
    self.swipeView.dataSource = self;
    self.swipeView.delegate = self;
    receivedArray = [[NSMutableArray alloc]init];
    //navCv = [NavigationHandler new];
     [NavigationHandler getInstance].delegate = self;
   // [self initiateMenuOptions];
    
   
    
    

    [self setUpView];

    [NSTimer scheduledTimerWithTimeInterval:0.5
                                     target:self
                                   selector:@selector(setUpScrollerView:)
                                   userInfo:nil
                                    repeats:NO];
    
    
    // set custom navigationBar with a bigger height
   // [self.navigationController setValue:[[YALNavigationBar alloc]init] forKeyPath:@"navigationBar"];
   
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if ([DataManager sharedManager].isChangeInReceived) {
       // [self setupSwipeView];
        [DataManager sharedManager].isChangeInReceived = NO;
    }
    
    
    [ [DataManager sharedManager].hub setCount:0];
    [DataManager sharedManager].hub = [[RKNotificationHub alloc]initWithView:_notificationCount];
    [ [DataManager sharedManager].hub incrementBy:[DataManager sharedManager].notificationToTalCount];

    NSLog(@"%d",[DataManager sharedManager].notifReceiveCount);
   
}


-(void) setupSwipeView{
    
    receivedArray = [[DataManager sharedManager].notificationArray mutableCopy];
    if (receivedArray.count>0) {
        self.count = receivedArray.count;
        [self.swipeView reloadData];

    }
    else{
        self.count = 1;
        [self.swipeView reloadData];

    }

}
- (void) setUpScrollerView:(NSTimer *) timer {
    //do something here..
    self.swipeView.hidden = false;
    [self.swipeView reloadData];
    [self setupSwipeView];

}

-(void) setUpView{
    
    _categoryArray = [DataManager sharedManager].categoryArray.mutableCopy;

    cat1 = _categoryArray[0];
    _lblCat1.text = [cat1.title uppercaseString];
    [_img1 setImageWithURL:cat1.catImg];
    
    cat2 = _categoryArray[1];
    _lblCat2.text = [cat2.title uppercaseString];
    [_img2 setImageWithURL:cat2.catImg];

    cat3 = _categoryArray[2];
    _lblCat3.text = [cat3.title uppercaseString];
    [_img3 setImageWithURL:cat3.catImg];
    
    
    
}

-(void)updateCardData{
    [DataManager sharedManager].hub = [[RKNotificationHub alloc]initWithView:_notificationCount];
    
    [ [DataManager sharedManager].hub incrementBy:[DataManager sharedManager].notificationToTalCount];
    NSLog(@"%d",[DataManager sharedManager].notificationToTalCount);

    
[self setupSwipeView];
    
 
}



#pragma mark - IBBAction
- (IBAction)btnPlusPressed:(id)sender {
    [self performSegueWithIdentifier:@"more" sender:nil];

}

- (IBAction)btnFreelance:(id)sender {
    selectedCat = cat2;
    [[DataManager sharedManager] setSelectedCtaegory:cat2.title];
    [self performSegueWithIdentifier:@"document" sender:cat2.templatesArray];

}

- (IBAction)btnPersonal:(id)sender {
    selectedCat = cat1;

    [[DataManager sharedManager] setSelectedCtaegory:cat1.title];

    [self performSegueWithIdentifier:@"document" sender:cat1.templatesArray];

}
- (IBAction)btnDigital:(id)sender {
    selectedCat = cat3;

    [[DataManager sharedManager] setSelectedCtaegory:cat3.title];

    [self performSegueWithIdentifier:@"document" sender:cat3.templatesArray];

}
- (IBAction)btnMore:(id)sender {
   [self performSegueWithIdentifier:@"more" sender:nil];

}


#pragma mark - context menu
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    //should be called after rotation animation completed
    [self.contextMenuTableView reloadData];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self.contextMenuTableView updateAlongsideRotation];
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    
    [coordinator animateAlongsideTransition:nil completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        //should be called after rotation animation completed
        [self.contextMenuTableView reloadData];
    }];
    [self.contextMenuTableView updateAlongsideRotation];
    
}


- (IBAction)presentMenuButtonTapped:(UIButton *)sender {
//    // init YALContextMenuTableView tableView
//    if (!self.contextMenuTableView) {
//        self.contextMenuTableView = [[YALContextMenuTableView alloc]initWithTableViewDelegateDataSource:self];
//        self.contextMenuTableView.animationDuration = 0.15;
//        //optional - implement custom YALContextMenuTableView custom protocol
//        self.contextMenuTableView.yalDelegate = self;
//        //optional - implement menu items layout
//        self.contextMenuTableView.menuItemsSide = Right;
//        self.contextMenuTableView.menuItemsAppearanceDirection = FromTopToBottom;
//        
//        //register nib
//        UINib *cellNib = [UINib nibWithNibName:@"ContextMenuCell" bundle:nil];
//        [self.contextMenuTableView registerNib:cellNib forCellReuseIdentifier:menuCellIdentifier];
//    }
//    
//    // it is better to use this method only for proper animation
//    [self.contextMenuTableView showInView:self.navigationController.view withEdgeInsets:UIEdgeInsetsZero animated:YES];
    
    [self.sideMenuViewController presentLeftMenuViewController];
}

#pragma mark - Local methods

- (void)initiateMenuOptions {
    self.menuTitles = @[@"",
                        @"Home",
                        @"Category",
                        @"Logout"];
    
    self.menuIcons = @[@"icn_close",@"icn_close",@"icn_close",@"icn_close"];
  
    
    
    NSLog(@"hbcdhjsbdjhbd");
}


#pragma mark - YALContextMenuTableViewDelegate

- (void)contextMenuTableView:(YALContextMenuTableView *)contextMenuTableView didDismissWithIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        NSArray *array = [self.navigationController viewControllers];

        [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];

    }
    NSLog(@"Menu dismissed with indexpath = %@", indexPath);
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (void)tableView:(YALContextMenuTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView dismisWithIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuTitles.count;
}

- (UITableViewCell *)tableView:(YALContextMenuTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ContextMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:menuCellIdentifier forIndexPath:indexPath];
    
    if (cell) {
        cell.backgroundColor = [UIColor clearColor];
        cell.menuTitleLabel.text = [self.menuTitles objectAtIndex:indexPath.row];
       cell.menuImageView.image = [UIImage imageNamed:[self.menuIcons objectAtIndex:indexPath.row] ];
    }
    
    return cell;
}

#pragma mark - swipe view delegate



- (NSUInteger)swipeViewNumberOfCards:(SwipeView *)swipeView
{
    return self.count;
}

- (UIView *)swipeView:(SwipeView *)swipeView
          cardAtIndex:(NSUInteger)index
{
    //  NSString *imageName = [NSString stringWithFormat:@"cards_%@",@(index + 1)];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,swipeView.frame.size.width,swipeView.frame.size.height)];
    view.backgroundColor = [UIColor clearColor];
    NSString *imageName = [NSString stringWithFormat:@"Card"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    imageView.frame = CGRectMake(0,0,view.frame.size.width,view.frame.size.height);
    NSString *imgIcon = [NSString stringWithFormat:@"Stroke"];
    UIImageView *iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgIcon]];
    CGSize size = imageView.frame.size;
    [iconImageView setFrame:CGRectMake(0, 0, 90, 90)];
    [iconImageView setCenter:CGPointMake(size.width/2, size.height/2)];

    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(20, view.frame.size.height-52, view.frame.size.width-40, 50)];
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    lbl.numberOfLines = 2;
    lbl.textColor=[UIColor colorWithRed:233/255 green:1 blue:1 alpha:1.0];
    lbl.userInteractionEnabled=NO;
    lbl.textAlignment = NSTextAlignmentCenter;
    [lbl setFont:[UIFont fontWithName:@"Avenir-Bold" size:15.0f]];
    lbl.backgroundColor = [UIColor clearColor];
    
    lbl.textColor=[UIColor colorWithRed:0.921 green:1.0 blue:1.0 alpha:1.0];
    
    
    

    lbl.text= @"No Notification";
    [view addSubview:imageView];
    [view addSubview:iconImageView];
    [view addSubview:lbl];
    if (receivedArray.count>0) {
        
      NotificationObject *tem = receivedArray[index];
        lbl.text= tem.message;
        [iconImageView setImageWithURL:tem.catImg];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self
                   action:@selector(btnNotificationClicked:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"" forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, view.frame.size.width,  view.frame.size.width);
        button.tag = index;
        [view addSubview:button];
    }
   

    return view;
}

- (OverlayView *)swipeView:(SwipeView *)swipeView
        cardOverlayAtIndex:(NSUInteger)index
{
    CustomOverlayView *overlay = [[NSBundle mainBundle] loadNibNamed:@"OverlayView" owner:self options:nil][0];
    return overlay;
}

- (void)swipeView:(SwipeView *)swipeView didSwipeCardAtIndex:(NSUInteger)index inDirection:(SwipeDirection)direction
{
    if (receivedArray.count>0) {
        if (index == receivedArray.count - 1) {
    
        self.count = receivedArray.count;
        [self.swipeView reloadData];
        }
    }

   else if (index >= 1) {
        self.count = 1;
        [self.swipeView reloadData];
        
    }
}

- (void)swipeViewDidRunOutOfCards:(SwipeView *)swipeView
{
    [swipeView resetCurrentCardNumber];
}

- (void)swipeView:(SwipeView *)swipeView didSelectCardAtIndex:(NSUInteger)index
{
    NSLog(@"点击");
}

- (BOOL)swipeViewShouldApplyAppearAnimation:(SwipeView *)swipeView
{
    return YES;
}

- (BOOL)swipeViewShouldMoveBackgroundCard:(SwipeView *)swipeView
{
    return YES;
}

- (BOOL)swipeViewShouldTransparentizeNextCard:(SwipeView *)swipeView
{
    return YES;
}

- (POPPropertyAnimation *)swipeViewBackgroundCardAnimation:(SwipeView *)swipeView
{
    return nil;
}

-(void) btnNotificationClicked:(UIButton*)sender
{
    
    if (receivedArray.count>0) {
        selectednotif = receivedArray[sender.tag];


        [receivedArray removeObjectAtIndex:sender.tag];
       [[DataManager sharedManager].notificationArray removeObjectAtIndex:sender.tag];
        [DataManager sharedManager].isChangeInReceived = YES;
        [self.swipeView cleanUp];
        [self performSegueWithIdentifier:@"detail" sender:nil];
        [self setupSwipeView];

    }


}


-(void) registerDevice{

    [AccountService registerDeviceIdWithsuccess:^(id data) {
        [DataManager sharedManager ].isdeviceRegister =YES;
    } failure:^(NSError *error) {
        [self  showAlertMessage:error.localizedDescription];
    }];
}


#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
