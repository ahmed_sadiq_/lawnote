//
//  WalkThroughVC.h
//  LawNote
//
//  Created by Apple Txlabz on 03/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMPageControl.h"
@interface WalkThroughVC : UIViewController
@property (weak, nonatomic) IBOutlet UIPageControl *pager;
@property (nonatomic, weak) IBOutlet SMPageControl *customPageControl;

@end
