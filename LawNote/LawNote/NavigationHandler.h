//
//  NavigationHandler.h
//  LawNote
//
//  Created by Samreen Noor on 09/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotificationObject.h"
#import "HomeViewController.h"
@protocol NavigationHandlerDelegate<NSObject>

@optional
-(void) updateCardData;
//- delegate method;

@end

@interface NavigationHandler : NSObject

@property (nonatomic, weak) id<NavigationHandlerDelegate> delegate;


+(NavigationHandler *)getInstance;
-(void)addReceivedTemplate:(NotificationObject *)temp;
-(void) rotateScreenInLandScape;
-(void) rotateScreenPortrait;
-(void) restrictRotation:(NSString *) restriction;
@end
