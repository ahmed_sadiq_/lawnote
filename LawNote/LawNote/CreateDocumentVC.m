//
//  CreateDocumentVC.m
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CreateDocumentVC.h"
#import "DocumentService.h"
#import "Options.h"
#import "Constant.h"
#import "UIAlertView+JS.h"
#import "ManageViewController.h"
#import "Constant.h"
#import "QuestionViewCell.h"
#import "QuestionHeaderCell.h"
#import "CustomIOSAlertView.h"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"
#import "QuestionFooterCell.h"
#import "EditViewController.h"
#import "UIColor+Style.h"
#import "RCEasyTipView.h"
#import "DetailTextBox.h"
#import "InfoCell.h"
#import "SVProgressHUD.h"
#import "ReceiverInfo.h"
#import "MutipleOptionTextView.h"
#import "Receiver.h"

@interface CreateDocumentVC ()<RCEasyTipViewDelegate,UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate,DetailTextBoxDelegate,MutipleOptionTextViewDelegate,UIScrollViewDelegate,ReceiverInfoDelegate>
{
    EditText *editTxt;
    EditText *editTxt2;
    CGRect workingFrame;
    UIImage *signatureImgSender;
    UIImage *signatureImgRec;
    BOOL isUserDirectSend;

    BOOL isSenderView;
    int filledCount;
    NSString *senderFName;
    NSString *senderLName;
    NSString *recFName;
    NSString *recLName;
    NSString *senderEmail;
    NSString *recEmail;
    UITextField *checkFiled;
    BOOL isPnading;
    BOOL isSaved;
    BOOL isScreenUp;

    NSMutableArray *viewsArray ;
    NSInteger selectedSection;
    UITextView *questionTxtView;
    
    ReceiverInfo *receiverInfoView;
    NSArray *recInfoArray;
    

}
@end

@implementation CreateDocumentVC


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
    if ([segue.identifier isEqualToString:@"moveToDraft" ]) {
        ManageViewController  *manageVC = segue.destinationViewController;
        manageVC.isPanding = isPnading;
    }
    else  if ([segue.identifier isEqualToString:@"edit" ]) {
       EditViewController *editVC = segue.destinationViewController;
        editVC.templete = _templete;
        editVC.questionArray = _questionArray;
    }
}

-(void)setupView{
    [self updateProgressBar];
    _lblDocName.text = _templete.template_title;
    _lblDocDes.text = _templete.template_description;

    _lblTampleteDescrption.text = _templete.template_description;
    _lblTitle.text = _templete.template_title;
    _btnSaveDraft.layer.cornerRadius = 5; // this value vary as per your desire
    _btnSaveDraft.clipsToBounds = YES;
    _btnCancel.layer.cornerRadius = 5; // this value vary as per your desire
    _btnCancel.clipsToBounds = YES;
    _boxView.layer.cornerRadius = 5; // this value vary as per your desire
    _boxView.clipsToBounds = YES;
    _btnSenderSignature.layer.cornerRadius = 5; // this value vary as per your desire
    _btnSenderSignature.clipsToBounds = YES;
    self.signatureView.layer.masksToBounds = NO;
    self.signatureView.layer.shadowOffset = CGSizeMake(-3, 1);
    self.signatureView.layer.shadowRadius = 2;
    self.signatureView.layer.shadowOpacity = 0.2;
   _btnCancelEditing.layer.borderWidth =  1.0;
 _btnCancelEditing.layer.borderColor =   [UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0].CGColor;
    isUserDirectSend = NO;

    _isChageDoc = NO;
    [DataManager sharedManager].isChageDoc = NO;
    
    senderFName = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"];
    senderLName = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_lastName"];
    
    senderEmail = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_email"];
    
    _lblSenderName.text =senderFName;
    _lblSenderEmail.text =senderEmail;
    
 
    
    if(IS_IPHONE_6Plus)
    {
        self.webViewHeightConstrain.constant= 736-60;
        
    }
    else if(IS_IPHONE_5)
    {
        
        self.webViewHeightConstrain.constant= 568-60;
        
    }
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [self setupView];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}
-(void)viewDidLayoutSubviews{
//    _boxViewHeightConstrain.constant = 550;
//    _userInfoBoxHeightConstrain.constant = 250;
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [self setUpPreview];
    _webViewHeightConstrain.constant = _webView.scrollView.contentSize.height + 40;

}

- (void) hideLabel
{
    _swipeLabelView.hidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //[self setupView];
    recInfoArray = [[NSArray alloc]init];
    _swipeLabelView.hidden = false;
    [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(hideLabel) userInfo:nil repeats:NO];
    
    
    ////////////  ###### Web View #####
    NSURL *url = [NSURL URLWithString:_templete.template_url_view];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
    ////////////// ############# end
    filledCount = 0;
    self.automaticallyAdjustsScrollViewInsets = NO;
    workingFrame.origin.x =0;
    int i=1;
    int space=40;
    if (IS_IPHONE_6Plus) {
        space=50;
        
    }
    viewsArray = [[NSMutableArray alloc]init];
    for (Questions *question in _questionArray) {
        NSString *count = [NSString stringWithFormat:@"%d/%lu",i,(unsigned long)_questionArray.count];
        if ([question.question_type isEqualToString:@"editTextArea"]) {
            DetailTextBox *edittxtView;
            edittxtView   = [DetailTextBox loadWithNibForEditTextWithId:self questionView:question andQuestionNumber:count];
            [edittxtView setDelegate:self];
            edittxtView.frame = CGRectMake(workingFrame.origin.x, 0, self.view.frame.size.width-space, self.questionScroller.frame.size.height);
            
            [edittxtView show];
            [viewsArray addObject:edittxtView];
            //workingFrame.origin.x = workingFrame.origin.x + edittxtView.frame.size.width;
            
            i++;
            
            
        }
        
     else   if ([question.question_type isEqualToString:@"editText"]) {
            EditText *edittxtView;
            edittxtView   = [EditText loadWithNibForEditText:question andQuestionNumber:count];
            [edittxtView setDelegate:self];
            edittxtView.frame = CGRectMake(workingFrame.origin.x, 0, self.view.frame.size.width-space, self.questionScroller.frame.size.height);
            
            [edittxtView show];
            [viewsArray addObject:edittxtView];
            //workingFrame.origin.x = workingFrame.origin.x + edittxtView.frame.size.width;
            
            i++;
            
            
        }
        else if ([question.question_type isEqualToString:@"date"]) {
            DateView *dateView;
            dateView   = [DateView loadWithNibForDate:question andQuestionNumber:count];
            [dateView setDelegate:self];
            dateView.frame = CGRectMake(workingFrame.origin.x, 0, self.view.frame.size.width-space, self.questionScroller.frame.size.height);
            
            [dateView show];
            [viewsArray addObject:dateView];
            // workingFrame.origin.x = workingFrame.origin.x + dateView.frame.size.width;
            
            i++;
            
        }
        else if ([question.question_type isEqualToString:@"radio"]) {
            RadioBtnView *radioBox;
            radioBox   = [RadioBtnView loadWithNibForRadioBox:question andQuestionNumber:count];
            [radioBox setDelegate:self];
            radioBox.frame = CGRectMake(workingFrame.origin.x, 0, self.view.frame.size.width-space, self.questionScroller.frame.size.height);
            
            [radioBox show];
            [viewsArray addObject:radioBox];
            // workingFrame.origin.x = workingFrame.origin.x + radioBox.frame.size.width;
            
            i++;
            
            
            
        }
        else if ([question.question_type isEqualToString:@"spinner"]) {
            ListView *listView;
            listView   = [ListView loadWithNibForListView:question andQuestionNumber:count];
            [listView setDelegate:self];
            listView.frame = CGRectMake(workingFrame.origin.x, 0, self.view.frame.size.width-space, self.questionScroller.frame.size.height);
            
            [listView show];
            [viewsArray addObject:listView];
            // workingFrame.origin.x = workingFrame.origin.x + listView.frame.size.width;
            i++;
            
            
            
        }
        else if ([question.question_type isEqualToString:@"checkbox"]) {
            CheckBoxView *checkBoxView;
            checkBoxView   = [CheckBoxView loadWithNibForCheckBox:question andQuestionNumber:count];
            [checkBoxView setDelegate:self];
            checkBoxView.frame = CGRectMake(workingFrame.origin.x, 0, self.view.frame.size.width-space, self.questionScroller.frame.size.height);
            
            [checkBoxView show];
            [viewsArray addObject:checkBoxView];
            // workingFrame.origin.x = workingFrame.origin.x + checkBoxView.frame.size.width;
            
            i++;
            
            
        }
        else if ([question.question_type isEqualToString:@"radio_option"]) {
            MutipleOptionTextView *radioBox;
            radioBox   = [MutipleOptionTextView loadWithNibForRadioBox:question andQuestionNumber:count];
           [radioBox setDelegate:self];
            radioBox.frame = CGRectMake(workingFrame.origin.x, 0, self.view.frame.size.width-space, self.questionScroller.frame.size.height);
            
            [radioBox show];
            [viewsArray addObject:radioBox];
            // workingFrame.origin.x = workingFrame.origin.x + radioBox.frame.size.width;
            
            i++;
            
            
            
        }
        
    }
    [_tblView reloadData];
    _boxViewHeightConstrain.constant = 400 ;
    _userInfoBoxHeightConstrain.constant =  100 ;
}




#pragma mark -Keyboard Method

-(void)keyboardWillShow {
    // Animate the current view out of the way
    
    
    if(checkFiled.tag!=11 && [DataManager sharedManager].selectedView.tag != 11&& !isScreenUp){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:selectedSection];
        [_tblView scrollToRowAtIndexPath:indexPath
                        atScrollPosition:UITableViewScrollPositionTop
                                animated:YES];
        _tblView.scrollEnabled = NO;
        isScreenUp = YES;
        [self animateTextView:questionTxtView up:YES];

    }
}

-(void)keyboardWillHide {
    if(checkFiled.tag!=11 &&[DataManager sharedManager].selectedView.tag != 11)
    {
        _tblView.scrollEnabled = YES;
        isScreenUp = NO;

    [self animateTextView:questionTxtView up:NO];
    }
}
#pragma  mark - Scrollview Methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"hidePickerView"
     object:nil];
}

#pragma  mark - tableview Methods



- (void)InfoButtonTapped:(id)sender {
    RCEasyTipPreferences *preferences = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferences.drawing.backgroundColor = [UIColor  colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
    preferences.animating.showDuration = 1.5;
    preferences.animating.dismissDuration = 1.5;
    preferences.animating.dismissTransform = CGAffineTransformMakeTranslation(0, 100);
    preferences.animating.showInitialTransform = CGAffineTransformMakeTranslation(0, -100);
    preferences.shouldDismissOnTouchOutside = YES;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferences];
    tipView.text = @"EasyTipView is an easy to use tooltip view. It can point to any UIView or UIBarItem subclasses. Tap the buttons to see other tooltips.";
    tipView.delegate = self;
    [tipView showAnimated:YES forView:sender withinSuperView:nil];
    
}
-(void) buttonSaveAgreementClicked:(UIButton*)sender{
    [self btnSave:sender];
}

-(void) buttonDetailClicked:(UIButton*)sender
{
    selectedSection = sender.tag;
    [_tblView reloadData ];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:selectedSection];
    [_tblView scrollToRowAtIndexPath:indexPath
                         atScrollPosition:UITableViewScrollPositionTop
                                 animated:YES];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (tableView.tag == 100) {
       return 0;

    }
    if (_questionArray.count -1 == section) {
    return 100;
    }
    else return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
   
    if (_questionArray.count -1 == section) {

    QuestionFooterCell   *footerView;
    footerView = [[[NSBundle mainBundle] loadNibNamed:@"QuestionFooterCell" owner:self options:nil] objectAtIndex:0];

        footerView.backgroundColor = [UIColor whiteColor];
    footerView.btnSaveAgreement.tag = section;
    [footerView.btnSaveAgreement addTarget:self action:@selector(buttonSaveAgreementClicked:) forControlEvents:UIControlEventTouchUpInside];
        if (filledCount>0) {
            footerView.btnSaveAgreement.userInteractionEnabled = YES;
        }
        else{
            footerView.btnSaveAgreement.userInteractionEnabled = NO;
            
        }
    
        return footerView;
 
    }
    else
        return nil;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
 
    QuestionHeaderCell   *headerView;
    headerView = [[[NSBundle mainBundle] loadNibNamed:@"QuestionHeaderCell" owner:self options:nil] objectAtIndex:0];
    
//    headerView.layer.masksToBounds = NO;
//    headerView.layer.shadowOffset = CGSizeMake(-3, 1);
//    headerView.layer.shadowRadius = 3;
//    headerView.layer.shadowOpacity = 0.4;
    headerView.layer.borderColor=[UIColor whiteColor].CGColor;
    headerView.layer.borderWidth=1.0f;
    
    headerView.btnShowDetail.tag = section;
    [headerView.btnShowDetail addTarget:self action:@selector(buttonDetailClicked:) forControlEvents:UIControlEventTouchUpInside];
    [headerView.btnInfo addTarget:self action:@selector(InfoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

    Questions *question = _questionArray[section];
    headerView.lblQuestion.text = question.question_text;
    // [headerView addSubview:lblQuestion];
    
    
    return headerView;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView.tag == 100) {
        return 1;

    }
    else
    return viewsArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView.tag == 100) {
        return 0;
        
    }
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == 100) {
        return 80;

    }
    else{
    if (IS_IPAD) {
        return 119;
        
    }
    else{
        if (selectedSection == indexPath.section) {
            return 250;
        }
        else
            return 0;
        
    }
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag == 100) {
        return  recInfoArray.count;

    }
    else
    return  1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 100) {
        Receiver *rec = recInfoArray[indexPath.row];
        InfoCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
        if (cell==nil) {
            cell =[[InfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        cell.lblName.text = [NSString stringWithFormat:@"%@  %@",rec.fName,rec.lName];
        cell.lblEmail.text = rec.email;
        return cell;

    }
    else{

    Questions *question = _questionArray[indexPath.section];
    static NSString *MyIdentifier;
    if ([question.question_type isEqualToString:@"editText"]) {
        MyIdentifier = @"text";
    }
    else if ([question.question_type isEqualToString:@"date"]) {
        MyIdentifier = @"date";
    }
    else if ([question.question_type isEqualToString:@"radio"]) {
        MyIdentifier = @"radio";
    }
    else if ([question.question_type isEqualToString:@"spinner"]) {
        MyIdentifier = @"list";
    }
    else if ([question.question_type isEqualToString:@"checkbox"]) {
        MyIdentifier = @"checkBox";
    }
    
    QuestionViewCell *cell =[tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell==nil) {
        cell =[[QuestionViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
    }
    UIView *view = viewsArray[indexPath.section];
    view.frame  =    CGRectMake(0,0,cell.QuestionBox.frame.size.width,cell.QuestionBox.frame.size.height);
    [cell.QuestionBox addSubview:view];
    cell.QuestionBox.layer.borderWidth = 1;
    cell.QuestionBox.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    cell.backgroundColor = [UIColor whiteColor];
   
    
    return cell;
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void) setUpPreview{
    for (Questions *question in _questionArray) {
        
        if ([question.question_type isEqualToString:@"editTextArea"]) {
            
            if (question.isFilled) {
                [self updateWebViewDetailTxtWithTag:question.tag_id andValue:question.answer];
            }
            
        }
       else if ([question.question_type isEqualToString:@"editText"]) {
            
            if (question.isFilled) {
                [self updateWebViewWithTag:question.tag_id andValue:question.answer];
            }
            
        }
        else if ([question.question_type isEqualToString:@"date"]) {
            
            if (question.isFilled) {
                [self updateWebViewWithTag:question.tag_id andValue:question.answer];
                
            }
            
        }
        else if ([question.question_type isEqualToString:@"radio"]) {
            
            if (question.isFilled) {
                
                [self updateWebViewWithTag:question.tag_id andOptionValue:question.optionAnswerValue];
            }
        }
        else if ([question.question_type isEqualToString:@"radio_option"]) {
            
            if (question.isFilled) {
                
                [self updateWebViewWithTag:question.tag_id andOptionValue:question.optionAnswerValue];
                for (Options *opt in question.optionArray) {
                    if ([question.optionAnswerId isEqualToString:opt.option_id]) {
                        for (Questions *ques in opt.questionArray) {
                            if (ques.isFilled) {

                            [self updateWebViewWithTag:ques.tag_id andValue:ques.answer];
                            }
                        }
                    }
                }
            }
        }
        else if ([question.question_type isEqualToString:@"spinner"]) {
            if (question.isFilled) {
                [self updateWebViewWithTag:question.tag_id andValue:question.optionAnswerValue];
            }
        }
        else if ([question.question_type isEqualToString:@"checkbox"]) {
            
            if (question.isFilled) {
                for (Options *o in question.optionArray) {
                    BOOL isTheObjectThere = [question.checkeBoxOptionAnswerIdArray containsObject:o.option_id];
                    
                    if (isTheObjectThere) {
                        [self updateWebViewWithTag:question.tag_id andOptionValue:o.option_value];
                        
                    }
                    else{
                        [self uncheckWebViewWithTag:question.tag_id andOptionValue:o.option_value];
                    }
                }
            }
        }
    }
}
-(void) updateProgressBar{
    filledCount = 0;
    for (Questions *q in _questionArray) {
        if (q.isFilled) {
            filledCount++;
        }
    }
    if (filledCount>0) {
        _btnSave.userInteractionEnabled = YES;
    }
    else{
        _btnSave.userInteractionEnabled = NO;
        
    }
    int totalCount = (int)_questionArray.count;
    float progressValue = (float)totalCount;
    NSLog(@"%f",filledCount/progressValue);
    if (progressValue>0) {
        
        [_circleProgressBar setProgress:(filledCount/progressValue) animated:YES];
    }
    if (filledCount>0 && filledCount == _questionArray.count) {
        [self showSendDocumentAlert];
    }
}



#pragma mark - PREVIEW UPDATE METHODS
-(void) updateWebViewDetailTxtWithTag:(NSString *)tag andValue:(NSString *)value{
    NSString *script = [NSString stringWithFormat:@"document.getElementById('%@').innerHTML = '%@';",tag, value];
    
    [self.webView stringByEvaluatingJavaScriptFromString:script];
    
}
-(void) updateWebViewWithTag:(NSString *)tag andValue:(NSString *)value{
    NSString *script = [NSString stringWithFormat:@"document.getElementById('%@').value = '%@';",tag, value];
    
    [self.webView stringByEvaluatingJavaScriptFromString:script];
    
}
-(void) updateWebViewWithTag:(NSString *)tag andOptionValue:(NSString *)value{
    int index = [value intValue];
    NSString *script = [NSString stringWithFormat:                       @"document.getElementsByName('%@')[%d].checked = %@",tag,index-1, value];
    [self.webView stringByEvaluatingJavaScriptFromString:script];
}
-(void) uncheckWebViewWithTag:(NSString *)tag andOptionValue:(NSString *)value{
    int index = [value intValue];
    NSString *script = [NSString stringWithFormat:                       @"document.getElementsByName('%@')[%d].checked = false",tag,index-1];
    [self.webView stringByEvaluatingJavaScriptFromString:script];
}
#pragma mark - Question Delegate
-(void)fillQuestion:(Questions *)question{
    _isChageDoc = YES;
    [self updateProgressBar];
    
    [self updateWebViewWithTag:question.tag_id andValue:question.answer];
    for (int i=0 ;i< _questionArray.count; i++) {
        Questions *q = [_questionArray objectAtIndex:i];
        if ([q.question_id isEqualToString:question.question_id]) {
            [_questionArray replaceObjectAtIndex:i withObject:question];
            break;
        }
    }
    
}

-(void)fillDetailQuestion:(Questions *)question{
    _isChageDoc = YES;
    [self updateProgressBar];
    
    [self updateWebViewDetailTxtWithTag:question.tag_id andValue:question.answer];
    for (int i=0 ;i< _questionArray.count; i++) {
        Questions *q = [_questionArray objectAtIndex:i];
        if ([q.question_id isEqualToString:question.question_id]) {
            [_questionArray replaceObjectAtIndex:i withObject:question];
            break;
        }
    }
    
}

-(void)filledCheckBox:(Questions *)question{
    _isChageDoc = YES;
    
    [self updateProgressBar];
    for (Options *o in question.optionArray) {
        BOOL isTheObjectThere = [question.checkeBoxOptionAnswerIdArray containsObject:o.option_id];
        
        if (isTheObjectThere) {
            [self updateWebViewWithTag:question.tag_id andOptionValue:o.option_value];
            
        }
        else{
            [self uncheckWebViewWithTag:question.tag_id andOptionValue:o.option_value];
            
        }
    }
    
    for (int i=0 ;i< _questionArray.count; i++) {
        Questions *q = [_questionArray objectAtIndex:i];
        if ([q.question_id isEqualToString:question.question_id]) {
            [_questionArray replaceObjectAtIndex:i withObject:question];
            break;
        }
    }
    
}
-(void)filledRadioBox:(Questions *)question{
    _isChageDoc = YES;
    
    [self updateProgressBar];
    [self updateWebViewWithTag:question.tag_id andOptionValue:question.optionAnswerValue];
    
    for (int i=0 ;i< _questionArray.count; i++) {
        Questions *q = [_questionArray objectAtIndex:i];
        if ([q.question_id isEqualToString:question.question_id]) {
            [_questionArray replaceObjectAtIndex:i withObject:question];
            break;
        }
    }
}
-(void)selectedDate:(Questions *)question{
    _isChageDoc = YES;
    
    [self updateProgressBar];
    [self updateWebViewWithTag:question.tag_id andValue:question.answer];
    
    for (int i=0 ;i< _questionArray.count; i++) {
        Questions *q = [_questionArray objectAtIndex:i];
        if ([q.question_id isEqualToString:question.question_id]) {
            [_questionArray replaceObjectAtIndex:i withObject:question];
            break;
        }
    }
}
-(void)selectedOption:(Questions *)question{
    _isChageDoc = YES;
    
    [self updateProgressBar];
    [self updateWebViewWithTag:question.tag_id andValue:question.optionAnswerValue];
    
    for (int i=0 ;i< _questionArray.count; i++) {
        Questions *q = [_questionArray objectAtIndex:i];
        if ([q.question_id isEqualToString:question.question_id]) {
            [_questionArray replaceObjectAtIndex:i withObject:question];
            break;
        }
    }
}
-(void)filledRadioMultilevelBox:(Questions *)question{
    _isChageDoc = YES;
    
    [self updateWebViewWithTag:question.tag_id andOptionValue:question.optionAnswerValue];
    
    for (Options *opt in question.optionArray) {
        if ([question.optionAnswerId isEqualToString:opt.option_id]) {
            for (Questions *ques in opt.questionArray) {
                [self updateWebViewWithTag:ques.tag_id andValue:ques.answer];

            }
        }
    }
    [self updateProgressBar];

    
    for (int i=0 ;i< _questionArray.count; i++) {
        Questions *q = [_questionArray objectAtIndex:i];
        if ([q.question_id isEqualToString:question.question_id]) {
            [_questionArray replaceObjectAtIndex:i withObject:question];
            break;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didShowTip:(RCEasyTipView *)tipView {
}
#pragma mark - IBButton Action
- (IBAction)btnEditingYes:(id)sender {
    [self performSegueWithIdentifier:@"edit" sender:nil];

}
- (IBAction)btnEditingCancel:(id)sender {
    _editView.hidden = YES;
    
}
- (IBAction)btnContinue:(id)sender {
    _DocumentDescriptionView.hidden = YES;
    _btnEdit.hidden = NO;
    _btnPreview.hidden = NO;
    _btnCreateDoc.hidden = NO;
    _createLine.hidden = NO;
}



- (IBAction)btnEdit:(id)sender {
    _editView.hidden = NO;
    _webScroller.hidden = YES;
    [_btnEdit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnCreateDoc setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    [_btnPreview setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    _editLine.hidden      = false;
    _createLine.hidden   = true;
    _previewLine.hidden  = true;
}



- (IBAction)btnPreView:(id)sender {
    _webScroller.hidden = NO;
    _editView.hidden = YES;

    [_btnPreview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnCreateDoc setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    [_btnSave setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    [_btnEdit setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    _previewLine.hidden      = false;
    _createLine.hidden   = true;
    _editLine.hidden  = true;
}
- (IBAction)btnCreate:(id)sender {
    _editView.hidden = YES;
    _webScroller.hidden = YES;
    [_btnCreateDoc setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnSave setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    [_btnPreview setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    [_btnEdit setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    _createLine.hidden      = false;
    _previewLine.hidden   = true;
    _editLine.hidden  = true;
}

- (IBAction)btnSave:(id)sender {
    _webScroller.hidden = YES;
    [_btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //[_btnCreateDoc setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_btnCreateDoc setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    [_btnPreview setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
  
        [self saveDocumentIntoDraf];
    
}

- (IBAction)btnBack:(id)sender {
    if (_isChageDoc) {
        //_saveDocumentView.hidden = NO;
        [self showSaveDocumentAlert];
        
        
    }
    else{
        [DataManager sharedManager].isChageDoc = NO;
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}
-(void)showSaveDocumentAlert{
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Save",@"Discard",@"Cancel" ,nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"Wait!" message:@"Do you want to save this agreement as a draft?"]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
          
                [self saveDocumentIntoDraf];
            
            
        }
        else if(buttonIndex == 1){
            [DataManager sharedManager].isChageDoc = YES;
            
            [self.navigationController popViewControllerAnimated:YES];
            
        } else{
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
    
}

-(void)showSendDocumentAlert{
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Send",@"Cancel" ,nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"Wait!" message:@"Your contract has successfully been completed."]];
    //msg=@".Do you want to Send this agreement?"
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
         
     
                //[self sendDocumentForSignature];
            [self btnSendSignature:nil];
        
        }
        else if(buttonIndex == 1){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
    
}
- (IBAction)btnTapOnePressed:(id)sender {
    _signatureBoxView.hidden = NO;
    isSenderView = YES;
    _signatureView.userInteractionEnabled = YES;
    _tfFirstName.text = senderFName;
    _tfLastName.text = senderLName;
    _tfEmail.text = senderEmail;
    _signatureView.signatureImage = signatureImgSender;
    
    
}
- (IBAction)btnTapTwoPressed:(id)sender {
//    _signatureBoxView.hidden = NO;
//    isSenderView = NO;
//    _signatureView.userInteractionEnabled = NO;
//    _tfFirstName.text = recFName;
//    _tfLastName.text = recLName;
//    _tfEmail.text = recEmail;
    [self performSegueWithIdentifier:@"addEmail" sender:nil];

    
    
}

- (IBAction)btnSendSignature:(id)sender {
    
    
   // [self showButtonPressed:self];
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Signature Options" message:@"Contract send for Signature" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Sign Now and Send" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self sendContractWithSignature:YES];

        // Distructive button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Send Now and Sign Later" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        [self sendContractWithSignature:NO];

        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)btnClear:(id)sender {
    [_signatureView erase];
    
   
    
}
- (IBAction)btnSaveContractInfo:(id)sender {
    if ([self checktextFields]) {
        
        if (isSenderView) {
            senderFName =_tfFirstName.text;
            senderLName = _tfLastName.text;
            senderEmail = _tfEmail.text;
            _lblSenderName.text = senderFName;
            _lblSenderEmail.text =senderEmail;
            signatureImgSender = _signatureView.signatureImage;
            _tfFirstName.text = @"";
            _tfLastName.text = @"";
            _tfEmail.text = @"";
            [_signatureView erase];
            
        }
        else{
            recFName =_tfFirstName.text;
            recLName = _tfLastName.text;
            recEmail = _tfEmail.text;
            _lblRecName.text = _tfFirstName.text;;
            _lblRecEmail.text =_tfEmail.text;
            _tfFirstName.text = @"";
            _tfLastName.text = @"";
            _tfEmail.text = @"";
            
            
            
        }
        _signatureBoxView.hidden = YES;
        
    }
}
- (IBAction)btnCancelContractInfo:(id)sender {
    _signatureBoxView.hidden = YES;
    _tfFirstName.text = @"";
    _tfLastName.text = @"";
    _tfEmail.text = @"";
    [_signatureView erase];
    [checkFiled resignFirstResponder];
}
- (IBAction)btnHideSignatureView:(id)sender {
    // _signatureBoxView.hidden = YES;
    [checkFiled resignFirstResponder];
    
}
#pragma mark - TextField Methods


-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}


-(BOOL)checktextFields{
    
    BOOL willEnabled = NO;
    if (isSenderView) {
        
        if( [_tfFirstName.text length] >= 1 && [_tfLastName.text length] >= 1 &&[self emailValidate:_tfEmail.text] == YES && _signatureView.signatureImage!=nil ){
            willEnabled = YES;
        }
        if (_tfFirstName.text.length==0 || _tfLastName.text.length==0||_tfEmail.text.length==0) {
            [self showAlertMessage:@"Please enter valid informtation"];
            return NO;
        }
        else if (![self emailValidate:_tfEmail.text]) {
            [self showAlertMessage:@"Please enter valid email"];
            return NO;
        }
        else if (_signatureView.signatureImage == nil) {
            [self showAlertMessage:@"Enter a Vaild Signature"];
            return NO;
        }
    }
    else{
        if( [_tfFirstName.text length] >= 1 && [_tfLastName.text length] >= 1 &&[self emailValidate:_tfEmail.text] == YES ){
            willEnabled = YES;
        }
        if (_tfFirstName.text.length==0 || _tfLastName.text.length==0||_tfEmail.text.length==0) {
            [self showAlertMessage:@"Please enter valid informtation"];
            return NO;
        }
        else if (![self emailValidate:_tfEmail.text]) {
            [self showAlertMessage:@"Please enter valid email"];
            return NO;
        }
        
        
    }
    
    return willEnabled;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    checkFiled = textField;
    [self animateTextField:nil up:YES];
    
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    [self animateTextField:nil up:NO];
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
    if(!up){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:selectedSection];
        [_tblView scrollToRowAtIndexPath:indexPath
                        atScrollPosition:UITableViewScrollPositionTop
                                animated:YES];
    }
    
    
}
#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            if (isUserDirectSend) {
                [self   settupSenderInfo];
                isUserDirectSend = NO;
                
            }
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}

#pragma mark - API Calling

-(void) saveDocumentIntoDraf{
    [DocumentService saveFilledDocumentWithAnswersArray:_questionArray andTemplateId:_templete.template_id success:^(id data) {
        isPnading =NO;
        
        [self performSegueWithIdentifier:@"moveToDraft" sender:nil];
        
    } failure:^(NSError *error) {
        
    } ];
}

-(void) sendDocumentForSignature{
    if ([recFName length]>0&&[recLName length]>0&&[recEmail length]>0&& signatureImgSender!=nil) {
        
        [DocumentService saveFilledDocumentWithAnswersArray:_questionArray andTemplateId:_templete.template_id success:^(id data) {
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];

            [DocumentService sendContractWithSenderFirstName:recFName lastName:recLName userImg:signatureImgSender eamil:recEmail templateID:_templete.template_id success:^(id data) {
                isPnading = YES;
                [self performSegueWithIdentifier:@"moveToDraft" sender:nil];
                
            } failure:^(NSError *error) {
                [self showAlertMessage:error.localizedDescription];
            }];
            
            
        } failure:^(NSError *error) {
            
        } ];
    }
    else if (signatureImgSender==nil){
        isUserDirectSend = YES;

        [self showAlertMessage:@"Please enter your Signature"];
        
    }
    else {
        [self showAlertMessage:@"Please enter complete informtation of receiver"];
        
    }

}

-(void) settupSenderInfo{
    [self btnPreView:nil];
    
        CGPoint bottomOffset = CGPointMake(0,500);
        [_webScroller setContentOffset:bottomOffset animated:YES];
    [self btnTapOnePressed:nil];

}

-(void)sendContractWithSignature:(BOOL)state{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
        receiverInfoView = [ReceiverInfo loadWithReceiverInfoViewWithSignature:state andRecInfoArray:recInfoArray.mutableCopy];
    receiverInfoView.delegate = self;
    [self.view addSubview:receiverInfoView];

   
    [receiverInfoView show];

    
    
    

}

-(void) sendContractApi{

    [DocumentService saveFilledDocumentWithAnswersArray:_questionArray andTemplateId:_templete.template_id success:^(id data) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [DocumentService sendContractWithReceiverInfo:recInfoArray  userImg:signatureImgSender templateID:_templete.template_id success:^(id data) {
            
            isPnading = YES;
            [self performSegueWithIdentifier:@"moveToDraft" sender:nil];
            
        } failure:^(NSError *error) {
            [self showAlertMessage:error.localizedDescription];
        }];
        
    } failure:^(NSError *error) {
        
    } ];
}

-(void)userInfo:(NSArray *)infoArray withSignature:(UIImage *)signImg{
    _boxViewHeightConstrain.constant = 380 + (infoArray.count * 80);
    _userInfoBoxHeightConstrain.constant = (infoArray.count * 80) + 80 ;
    Receiver *obj = infoArray[0];
    recFName = obj.fName;
    recLName = obj.lName;
    recEmail = obj.email;
    signatureImgSender = signImg;
    recInfoArray = infoArray;
    [_infoTabl reloadData];
    [self sendContractApi];
 
}
-(void)cancelUserInfo:(NSMutableArray *)infoArray{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    _boxViewHeightConstrain.constant = 380 + (infoArray.count * 80);
    _userInfoBoxHeightConstrain.constant = (infoArray.count * 80) + 80 ;
    recInfoArray = infoArray;
    [_infoTabl reloadData];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
