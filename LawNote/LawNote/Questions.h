//
//  Questions.h
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"
@interface Questions : BaseEntity

@property (nonatomic, strong) NSString * question_id;
@property (nonatomic, strong) NSString * question_text;
@property (nonatomic, strong) NSString * question_type;
@property (nonatomic, strong) NSString * tag_id;
@property (nonatomic, strong) NSString * template_id;
@property (nonatomic, strong) NSString * answer;
@property (nonatomic, strong) NSString * question_description;

@property (nonatomic, strong) NSString * optionAnswerId;
@property (nonatomic, strong) NSMutableArray *checkeBoxOptionAnswerIdArray;

@property (nonatomic, strong) NSString * optionAnswerValue;

@property (nonatomic) BOOL isFilled;
@property (nonatomic, strong) NSArray * optionArray;




- (id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapQuestionsFromArray:(NSArray *) arrlist;

@end
