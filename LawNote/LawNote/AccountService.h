//
//  AccountService.h
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseService.h"

@interface AccountService : BaseService
+(void) loginUserWithEmail:(NSString *) email
                  password:(NSString *) pass
                   success:(serviceSuccess) success
                   failure:(serviceFailure) failure;


+(void)registerUserWithFirstName:(NSString *)fName
                        lastName:(NSString *)lName
                           email:(NSString *)email
                        password:(NSString *)password
                         success:(serviceSuccess)success
                         failure:(serviceFailure)failure;


+ (void) forgetPasswordWhereEmail : (NSString *) email
                           succuss:(serviceSuccess) success
                           failure:(serviceFailure) failure;


+(void) updateUserProfileWithFirstName:(NSString *)fName
                              lastName:(NSString *)lName
                               userImg:(UIImage *)img
                               success:(serviceSuccess)success
                               failure:(serviceFailure)failure;
+(void) updateUserPasswordWithPassword:(NSString *)pass
                               success:(serviceSuccess)success
                               failure:(serviceFailure)failure;
+(void) registerDeviceIdWithsuccess:(serviceSuccess)success
                            failure:(serviceFailure)failure;
@end
