//
//  EditViewController.h
//  LawNote
//
//  Created by Samreen Noor on 25/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPSSignatureView.h"
#import "Templates.h"
@interface EditViewController : UIViewController
@property (strong, nonatomic) NSMutableArray *questionArray;
@property (strong, nonatomic) Templates *templete;
@property (weak, nonatomic) IBOutlet UIView *boxView;
@property (weak, nonatomic) IBOutlet UIScrollView *previewScroll;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnPreview;
@property (nonatomic)  BOOL isChageDoc;
@property (nonatomic)  BOOL isFromPandig;
@property (weak, nonatomic) IBOutlet UILabel *lblReceiverPart;
@property (weak, nonatomic) IBOutlet UILabel *lblAuthorPart;

@property (weak, nonatomic) IBOutlet UIImageView *previewLine;

@property (weak, nonatomic) IBOutlet UIImageView *editLine;
@property (weak, nonatomic) IBOutlet UIScrollView *editScroll;
@property (weak, nonatomic) IBOutlet UIWebView *editWebView;
@property (weak, nonatomic) IBOutlet UILabel *lblRecName;
@property (weak, nonatomic) IBOutlet UILabel *lblRecEmail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *previewScrollHeightConstrain;
@property (weak, nonatomic) IBOutlet UIView *boxView2;
@property (weak, nonatomic) IBOutlet UIView *boxView1;
@property (nonatomic)  BOOL isfromDraft;
@property (weak, nonatomic) IBOutlet UIImageView *senderSignImg;
@property (weak, nonatomic) IBOutlet UILabel *lblPreviewSendeName;
@property (weak, nonatomic) IBOutlet UILabel *lblPreviewSenderEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblPreviewReceiverName;
@property (weak, nonatomic) IBOutlet UILabel *lblPreviewReceiverEmail;

@property (weak, nonatomic) IBOutlet UIButton *btnSendForSignature;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSenderSignature;
@property (weak, nonatomic) IBOutlet UIScrollView *signatureBoxView;

@property (weak, nonatomic) IBOutlet UITextField *tfFirstName;
@property (weak, nonatomic) IBOutlet UITextField *tfLastName;
@property (weak, nonatomic) IBOutlet PPSSignatureView *senderSignatureView;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxViewHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userInfoBoxHeightConstrain;
@property (weak, nonatomic) IBOutlet UITableView *infoTabl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxView2HeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userInfoBox2HeightConstrain;
@property (weak, nonatomic) IBOutlet UITableView *tblInfo2;
@end
