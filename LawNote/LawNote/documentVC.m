//
//  documentVC.m
//  LawNote
//
//  Created by Apple Txlabz on 03/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "documentVC.h"
#import "DocumentCell.h"
#import "Templates.h"
#import "DataManager.h"
#import "CreateDocumentVC.h"
#import "CategoryService.h"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"
@interface documentVC ()

@end

@implementation documentVC

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    Templates *temp = sender;
    
    if ([segue.identifier isEqualToString:@"createDoc" ]) {
        CreateDocumentVC  *createVC = segue.destinationViewController;
        createVC.questionArray = temp.questionArray.mutableCopy;
        createVC.templete = temp;
        createVC.isfromDraft = false;
    }
   else if ([segue.identifier isEqualToString:@"moveToDraft" ]) {
       
    }
   
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
      [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    _tempTemplateArray = _templateArray;
    _lblCatName.text = [[DataManager sharedManager].selectedCtaegory uppercaseString];
    [DataManager sharedManager].isChageDoc = NO;
    [self setupRefreshControl];
    if (_isFromCategory) {
        [self getTemplate];

    }

}
-(void)viewWillAppear:(BOOL)animated{
    if ([DataManager sharedManager].isChageDoc) {
        [self getTemplate];
    }
}

#pragma  mark - IBBAction
- (IBAction)btnMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}



#pragma mark - Searchbar delegates & helpers
- (IBAction)btnSearch:(id)sender {
 
    _searchBar.hidden=NO;
    _lblCatName.hidden = YES;
    _btnSearch.hidden = YES;
    _btnMenu.hidden = YES;
    _searchViewLeftConstrain.constant = 8;
    [UIView animateWithDuration:0.3f
                     animations:^{
                         
                         [self.view layoutIfNeeded];
                         
                     }];
    
    [_searchBar setShowsCancelButton:YES animated:YES];
    
    [_searchBar setAlpha:1.0];
    [_searchBar becomeFirstResponder];
}
-(void)searchFieldReset{
    if (_searchBar.hidden==NO) {
        
        [_searchViewLeftConstrain setConstant:230];
        
        [UIView animateWithDuration:0.3f
                         animations:^{
                             
                             [self.view layoutIfNeeded];
                             
                         }];
        _searchBar.text = @"";
        [_searchBar resignFirstResponder];
        
        [_searchBar setShowsCancelButton:NO animated:YES];
        _searchBar.hidden = YES;
        _btnSearch.hidden =NO;
        _lblCatName.hidden=NO;
        _btnMenu.hidden   = NO;
    }
    
    
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [_searchBar resignFirstResponder];
    
    [_searchViewLeftConstrain setConstant:230];
    [UIView animateWithDuration:0.3f
                     animations:^{
                         
                         [self.view layoutIfNeeded];
                         
                     }];
    _searchBar.text = @"";
    [_searchBar resignFirstResponder];
    
    [_searchBar setShowsCancelButton:NO animated:YES];
    _searchBar.hidden = YES;
    _btnSearch.hidden =NO;
    _lblCatName.hidden=NO;
    _btnMenu.hidden = NO;
    _templateArray = _tempTemplateArray;
    [_tblView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
//  NSString  *searchString = _searchBar.text;
//    if (![searchString isEqualToString:@""]) {
//        [self searchFieldReset];
//    }
    [_searchBar resignFirstResponder];
}


- (void)getActiveUserListForSearch

{
    // NSString *name;
    NSMutableArray *searchArray=[[NSMutableArray alloc]init];
    
    if (_searchBar.text) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.template_title CONTAINS[cd] %@",_searchBar.text];
        searchArray = [_tempTemplateArray filteredArrayUsingPredicate:predicate].mutableCopy;
        
        //[searchArray addObject:dic];
        
    }
    
    
    if (searchArray.count>0) {
        
        _templateArray = searchArray;
        
        [_tblView reloadData];
    }
    
    
}
-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString * searchStr = [_searchBar.text stringByReplacingCharactersInRange:range withString:text];

    if ([searchStr isEqualToString:@""]) {
        
        _templateArray = _tempTemplateArray;
        [_tblView reloadData];
    }
    else{
    [self performSelector:@selector(getActiveUserListForSearch) withObject:nil afterDelay:0.15];
    }
    return YES;
}

#pragma  mark - tableview Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPAD) {
        return 119;

    }
    else
    return 80;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _templateArray.count;
}






- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    Templates *temp = _templateArray[indexPath.row];
  
    
  DocumentCell      *cell = [tableView dequeueReusableCellWithIdentifier:
                          @"cell"];

    
    cell.lblDocName.text = [temp.template_title uppercaseString];
    cell.lblDescription.text = temp.template_description;
    
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Templates *temp = _templateArray[indexPath.row];

    [self performSegueWithIdentifier:@"createDoc" sender:temp];
    
    
    
}

#pragma mark - Helper Methods

- (void)setupRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor       = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(refreshTemplete:) forControlEvents:UIControlEventValueChanged];
    [_tblView addSubview:_refreshControl];
}

- (void)refreshTemplete:(id)sender
{
    [self getTemplate];
    
    
}
#pragma mark - Api Calling
-(void) getTemplate{
    [CategoryService getTemplateWithCategoryID:_category.catID success:^(id data) {
        [self.refreshControl endRefreshing];

        _templateArray = data;
        _tempTemplateArray = _templateArray;

        [_tblView reloadData];
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    } ];

}




//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        //add code here for when you hit delete
//        
//        NSLog(@"row deleted");
//        
//        
//        Group * group = groupArray[indexPath.row];
//        NSMutableSet *set =groupArray.mutableCopy;
//        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Exit!" message:@"Do you want to leave this group." delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Exit", nil];
//        
//        
//        [alert showWithBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//            
//            if (buttonIndex == 1) {
//                
//                [set removeObject:group];
//                groupArray = set.mutableCopy;
//                [_tblView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
//                [_tblView reloadData];
//                [self deleteGroupApi:group.groupId];
//            }
//            else{
//                
//            }
//        }] ;
//    
//        
//    }
//}
#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
