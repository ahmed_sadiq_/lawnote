//
//  CreateYourOwnAgreementVC.h
//  LawNote
//
//  Created by Samreen Noor on 08/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPSSignatureView.h"
#import "Templates.h"

@interface CreateYourOwnAgreementVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnCreate;
@property (weak, nonatomic) IBOutlet UITextField *tfFirstName;
@property (weak, nonatomic) IBOutlet UITextField *tfLastName;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UIImageView *senderSignatureView;
@property (nonatomic)  BOOL isChageDoc;
@property (nonatomic)  BOOL isfromDraft;
@property (nonatomic)  BOOL isfromPending;
@property (weak, nonatomic) IBOutlet UILabel *lblAuthorPart;
@property (weak, nonatomic) IBOutlet UILabel *lblReceiverPart;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoBox1HeightConstrain;
@property (weak, nonatomic) IBOutlet UIView *boxView;

@property (weak, nonatomic) IBOutlet UIButton *btnSendForsign;
@property (weak, nonatomic) IBOutlet UIImageView *editLine;
@property (weak, nonatomic) IBOutlet UIImageView *previewLine;
@property (strong, nonatomic) Templates *templete;

@property (weak, nonatomic) IBOutlet PPSSignatureView *signView;
@property (weak, nonatomic) IBOutlet UIButton *btnPreview;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UITextView *editTextView;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblReciverName;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblRecEmail;
@property (weak, nonatomic) IBOutlet UIScrollView *signatureBox;
@property (weak, nonatomic) IBOutlet UIButton *btnSendAgreement;
@property (weak, nonatomic) IBOutlet UITextField *tfDocName;
@property (weak, nonatomic) IBOutlet UIScrollView *previewScroll;
@property (weak, nonatomic) IBOutlet UILabel *lblDocName;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblsenderEmailPreview;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderNamePreview;
@property (weak, nonatomic) IBOutlet UILabel *lblRevEmailPreview;
@property (weak, nonatomic) IBOutlet UILabel *lblRecNamePreview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *DescriptionHeightConstrain;
@property (weak, nonatomic) IBOutlet UIScrollView *createScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoBox2HeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoBox2UperViewHeightConstrain;
@property (weak, nonatomic) IBOutlet UITableView *infoTbl2;
@property (weak, nonatomic) IBOutlet UITableView *infoTbl1;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoBox1UperViewHeightConstrain;
@end
