//
//  NotificationObject.h
//  LawNote
//
//  Created by Samreen Noor on 11/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "BaseEntity.h"

@interface NotificationObject : BaseEntity

@property (nonatomic, strong) NSString * catImg;
@property (nonatomic, strong) NSString * template_id;
@property (nonatomic, strong) NSString * template_title;
@property (nonatomic, strong) NSString * template_description;
@property (nonatomic, strong) NSString * message;
@property (nonatomic, strong) NSString * template_url_view;

@property (nonatomic, strong) NSString * flag;
- (id)initWithDictionary:(NSDictionary *) responseData;
+ (NSArray *)mapNotificationFromArray:(NSArray *)arrlist ;
@end
