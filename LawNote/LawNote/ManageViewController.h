//
//  ManageViewController.h
//  LawNote
//
//  Created by Samreen Noor on 03/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *draftArray;
@property (strong, nonatomic) NSArray *pendingArray;
@property (strong, nonatomic) NSArray *completedArray;
@property (nonatomic) BOOL isPanding;
@property (strong, nonatomic) UIRefreshControl *refreshDraftControl;
@property (strong, nonatomic) UIRefreshControl *refreshPendingControl;
@property (strong, nonatomic) UIRefreshControl *refreshCompletedControl;

@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tblDraft;
@property (weak, nonatomic) IBOutlet UITableView *tblPanding;
@property (weak, nonatomic) IBOutlet UITableView *tblCompleted;
@property (weak, nonatomic) IBOutlet UIButton *btnpanding;
@property (weak, nonatomic) IBOutlet UIButton *btnCompleted;
@property (weak, nonatomic) IBOutlet UIButton *btnDraft;
@property (weak, nonatomic) IBOutlet UIImageView *completedLine;
@property (weak, nonatomic) IBOutlet UIImageView *padingLine;
@property (weak, nonatomic) IBOutlet UIImageView *draftLine;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblDraftwidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblDraftHeight;

@end
