//
//  HomeViewController.h
//  LawNote
//
//  Created by Apple Txlabz on 13/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"
#import "CustomOverlayView.h"
#import "POPPropertyAnimation.h"
#import "RESideMenu.h"
#import "NavigationHandler.h"

@interface HomeViewController : UIViewController {
    
}


@property (strong, nonatomic) NSArray *categoryArray;
@property (weak, nonatomic) IBOutlet UIImageView *notificationCount;
@property (weak, nonatomic) IBOutlet UILabel *lblCat1;
@property (weak, nonatomic) IBOutlet UILabel *lblCat3;
@property (weak, nonatomic) IBOutlet UILabel *lblCat2;
@property (weak, nonatomic) IBOutlet UILabel *lblMoreCat;
@property (weak, nonatomic) IBOutlet UIImageView *img2;
@property (weak, nonatomic) IBOutlet UIImageView *img3;

@property (weak, nonatomic) IBOutlet UIImageView *img1;

-(void) setupSwipeView;
@end
