//
//  NavigationHandler.m
//  LawNote
//
//  Created by Samreen Noor on 09/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "NavigationHandler.h"
#import "DataManager.h"
#import "HomeViewController.h"
#import "AppDelegate.h"

@implementation NavigationHandler
@synthesize delegate;


static NavigationHandler *instance= NULL;

+(NavigationHandler *)getInstance
{
    if (instance == nil) {
        instance = [[super alloc] init];
    }

    return instance;
}


-(void)addReceivedTemplate:(NotificationObject *)temp{
    
    [[DataManager sharedManager].notificationArray insertObject:temp atIndex:0];
    [self.delegate updateCardData];
    
}
- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}



-(void) rotateScreenInLandScape{
    
    [self restrictRotation:@"1"];

//    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
//    {
        // code for Portrait orientation
        
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
   // }
    
}
-(void) rotateScreenPortrait{
//    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
//    {
        // code for Portrait orientation

        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
 //   }
    

}

-(void) restrictRotation:(NSString *) restriction
{
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = restriction;
}
@end
