//
//  Constant.h
//  LawNote
//
//  Created by Apple Txlabz on 12/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#pragma mark Screen Sizes
#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667)
#define IS_IPHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568)
#define IS_IPHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480)
#define IS_IPAD ([[UIScreen mainScreen] bounds].size.height == 1024)
#define IS_IPHONE_6Plus ([[UIScreen mainScreen] bounds].size.height == 736)

#define signatureColor  [UIColor colorWithRed:232.0 / 255.0  green:232/255.0 blue:232/255.0 alpha:1.0f]

/*  ------------------- BASE URL ------------------- */

#if DEBUG
#define BASE_URL            @"http://lawnotes.witsapplication.com"   // Stagging
#else
#define BASE_URL            @"http://lawnotes.witsapplication.com"  // Production
#endif

/* BASE URL END */

/*  ------------------ LOGIN ------------------ */

#define URI_SIGN_UP             @"/user/signup"
#define URI_SIGN_IN             @"/user/login"
#define URI_FORGET_PASSWORD     @"/user/forgotPassword"
#define URL_HOME                @"/user/getHomeContent"
#define URL_GET_CATEGORIES      @"/user/get_categories"
#define URL_GET_TEMPLATE      @"/user/get_templates_by_category_id"
#define URL_GET_RECEIVER_TEMPLATE      @"/user/getTemplateDetail"
#define URL_GET_RECEIVED      @"/User/getReceivedTemplates"
//#define URL_GET_RECEIVER_TEMPLATE      @"/user/getPendingTemplate"
#define URI_REGISTER_DEVICE             @"user/register_device"


#define URL_EDIT_PROFILE                @"/user/editProfile"
#define URL_MANAGE_TEMPLATE               @"/user/getManagedTemplates"

/*  ------------------ DOCUMENT ------------------ */
#define URL_SAVE_DOCUMENT                @"/user/saveAnswers"
#define URL_SEND_DOCUMENT                @"/user/sendContract"
#define URL_SEND_RECEIVER_SIGN                @"/user/saveSignature"
#define URL_DELETE_DOCUMENT                @"/user/deleteDraft"
#define URL_EDIT_PENDING_TEMPLATE                @"/user/editPendingTemplate"

#define URL_CREATE_OWN_AGREEMENT_SEND              @"/user/createOwnTemplete"
#define URL_CREATE_OWN_AGREEMENT_SAVE              @"user/saveOwnTemplete"


// SIGN UP AND SIGN IN
#define KEY_FULL_NAME               @"user_name"
#define KEY_EMAIL                   @"email"
#define KEY_PASSWORD                @"password"
#define KEY_FIRST_NAME                @"first_name"
#define KEY_LAST_NAME                @"last_name"
#define KEY_USER_ID                @"user_id"


#define KEY_PROFILE_IMG                    @"profile_image"


#endif /* Constant_h */
