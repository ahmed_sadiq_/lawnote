//
//  Templates.h
//  LawNote
//
//  Created by Apple Txlabz on 15/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseEntity.h"
#import "Receiver.h"

@interface Templates : BaseEntity
@property (nonatomic, strong) NSString * userId;

@property (nonatomic, strong) NSString * category_id;
@property (nonatomic, strong) NSString * catImg;

@property (nonatomic, strong) NSString * template_id;
@property (nonatomic, strong) NSString * template_title;
@property (nonatomic, strong) NSString * template_description;
@property (nonatomic, strong) NSString * template_url;
@property (nonatomic, strong) NSString * template_url_view;
@property (nonatomic, strong) NSString * sender_id;
@property (nonatomic, strong) NSString * sender_first_name;
@property (nonatomic, strong) NSString * sender_last_name;
@property (nonatomic, strong) NSString * sender_email;

@property (nonatomic, strong) NSString * is_created_by_user;
@property (nonatomic, strong) NSString * receiver_first_name;
@property (nonatomic, strong) NSString * receiver_last_name;
@property (nonatomic, strong) NSString * receiver_email;
@property (nonatomic, strong) NSString * sender_signature_image;
@property (nonatomic, strong) NSString * receiver_signature_image;
@property (nonatomic, strong) NSArray * receiverInfoArray;

@property (nonatomic, strong) NSString * date_time;
@property (nonatomic)  BOOL isReceiver;

@property (nonatomic, strong) NSArray * questionArray;


- (id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapTemplatesFromArray:(NSArray *) arrlist;

@end
