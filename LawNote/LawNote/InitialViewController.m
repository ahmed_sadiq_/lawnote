//
//  InitialViewController.m
//  LawNote
//
//  Created by Apple Txlabz on 19/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "InitialViewController.h"
#import "CategoryService.h"
@interface InitialViewController ()

@end

@implementation InitialViewController

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad {
    [super viewDidLoad];

    NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];
    if(![[[defaults dictionaryRepresentation] allKeys] containsObject:@"isFirstLogin"]){
         [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLogin"];
        NSLog(@"mykey found");
    }
    
    
    BOOL isFirstLogin = [[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstLogin"];

    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:@"logged_in"];
    
    if (isFirstLogin) {
        if (IS_IPAD) {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Ipad" bundle:nil];
            UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"navWalkthrough"];
            [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
        }
        else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"navWalkthrough"];
        [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
        }
    }
    
  else  if (isLogin) {
        
      [self setupCurrentUserInfo];
        [CategoryService getCategoriesWithsuccess:^(id data) {
            [DataManager sharedManager].categoryArray = data;
            if (IS_IPAD) {
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Ipad" bundle:nil];
                UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"navHome"];
                [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
            }
            else{

            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"navHome"];
            [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
            }
        } failure:^(NSError *error) {
        }];
        
    }
    else
    
    {
        if (IS_IPAD) {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Ipad" bundle:nil];
            UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"navLogin"];
            [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
        }
        else{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"navLogin"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
    }
    }

}

-(void) setupCurrentUserInfo{
    
    User * user = [User new];
    user.name = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"];
    user.email = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_email"];
    user.userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"];
    [[DataManager sharedManager] setCurrentUser:user];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
