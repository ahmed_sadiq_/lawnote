//
//  EditViewController.m
//  LawNote
//
//  Created by Samreen Noor on 25/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "EditViewController.h"
#import "DocumentService.h"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"
#import "ManageViewController.h"
#import "Questions.h"
#import "Options.h"
#import "CreateDocumentVC.h"
#import "UIImageView+URL.h"
#import "SVProgressHUD.h"
#import "ReceiverInfo.h"
#import "Receiver.h"
#import "InfoCell.h"
#import "CategoryService.h"
#import "NotificationObject.h"


@interface EditViewController ()<UITextFieldDelegate,UIWebViewDelegate,ReceiverInfoDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIImage *signatureImgSender;
    UIImage *signatureImgRec;
    BOOL isSaved;
    BOOL isUserDirectSend;
    ReceiverInfo *receiverInfoView;
    NSMutableArray *recInfoArray;
    BOOL isSenderView;
    int filledCount;
    NSString *senderFName;
    NSString *senderLName;
    NSString *recFName;
    NSString *recLName;
    NSString *senderEmail;
    NSString *recEmail;
    UITextField *checkFiled;
    BOOL isPnading;
    BOOL isDiscardContract;

}
@end

@implementation EditViewController


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    
    if ([segue.identifier isEqualToString:@"moveToDraft" ]) {
        ManageViewController  *manageVC = segue.destinationViewController;
        manageVC.isPanding = isPnading;
    }
    
    
}

-(void)setupView{
    _boxView.layer.cornerRadius = 5; // this value vary as per your desire
    _boxView.clipsToBounds = YES;
    _btnSenderSignature.layer.cornerRadius = 5; // this value vary as per your desire
    _btnSenderSignature.clipsToBounds = YES;
    _btnSendForSignature.layer.cornerRadius = 5; // this value vary as per your desire
    _btnSendForSignature.clipsToBounds = YES;
    self.senderSignatureView.layer.masksToBounds = NO;
    self.senderSignatureView.layer.shadowOffset = CGSizeMake(-3, 1);
    self.senderSignatureView.layer.shadowRadius = 2;
    self.senderSignatureView.layer.shadowOpacity = 0.2;
    _isChageDoc = NO;
    isUserDirectSend = NO;

    [DataManager sharedManager].isChageDoc = NO;
    
    senderFName = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"];
    senderLName = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_lastName"];
    
    senderEmail = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_email"];
    
    _lblSenderName.text =senderFName;
    _lblSenderEmail.text =senderEmail;
    _lblPreviewSendeName.text =senderFName;
    _lblPreviewSenderEmail.text =senderEmail;
    
    if (_isfromDraft) {
        if (![_templete.sender_signature_image isEqualToString:@""]) {
            [_senderSignImg setImageWithURL:_templete.sender_signature_image];

        }

        isSaved = YES;
        if (![_templete.receiver_first_name isEqualToString:@""]||![_templete.receiver_email isEqualToString:@""]) {
            
            recFName = _templete.receiver_first_name;
            recLName = _templete.receiver_last_name;
            recEmail = _templete.receiver_email;
            _lblRecName.text = recFName;
            _lblRecEmail.text = recEmail;
            _lblPreviewReceiverName.text =recFName;
            _lblPreviewReceiverEmail.text =recEmail;

            

        }
    
        
    }
    else{
        isSaved = NO;
        _isChageDoc = YES;
    }
   
    if(IS_IPHONE_6Plus)
    {
        self.webViewHeightConstrain.constant= 736-60;
        self.previewScrollHeightConstrain.constant= 736-60;

    }
    else if(IS_IPHONE_5)
    {

        self.webViewHeightConstrain.constant= 568-60;
        self.previewScrollHeightConstrain.constant= 568-60;

    }
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    for (UIViewController *aViewController in navigationArray) {
        if ([aViewController isKindOfClass:[CreateDocumentVC class]]) {
            [aViewController  removeFromParentViewController];
        }
        
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];

    [self setupView];
    if (_isFromPandig) {
        [self getNotifiTemplate];
//        if (![_templete.sender_signature_image isEqualToString:@""] && _templete.sender_signature_image != nil) {
//        _previewScroll.hidden = NO;
//        [_btnPreview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [_btnEdit setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
//        _previewLine.hidden = false;
//        _editLine.hidden = YES;
//            _btnSenderSignature.hidden = YES;
//            _btnSendForSignature.hidden = YES;
//        self.btnEdit.userInteractionEnabled = NO;
//        self.btnSenderSignature.enabled = NO;
//        [self setupPendingView];
//        }
    }
    if (_isfromDraft) {
        _previewScroll.hidden = NO;
        [_btnPreview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnEdit setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
        _previewLine.hidden = false;
        _editLine.hidden = YES;
        
    }
    
}

-(void) setupPendingView{

    
//    User *user = [DataManager sharedManager].currentUser;
//    if (![user.userID isEqualToString:_templete.sender_id]) {
//        if (![_templete.receiver_signature_image isEqualToString:@""]) {
//
//        [_senderSignImg setImageWithURL:_templete.receiver_signature_image];
//        }
//    _lblPreviewSendeName.text =_templete.receiver_first_name;
//    _lblPreviewSenderEmail.text =_templete.receiver_email;
//            _lblPreviewReceiverName.text =_templete.sender_first_name;
//        _lblPreviewReceiverEmail.text =_templete.sender_email;
//        _lblAuthorPart.text = @"Receiver Information";
//        _lblReceiverPart.text = @"Author Information";
//    }
//        
    


}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self deregisterForKeyboardNotifications];
}
- (void)deregisterForKeyboardNotifications {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [center removeObserver:self name:UIKeyboardWillHideNotification object:nil];
 
    [center removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [self setUpPreview];
    [SVProgressHUD dismiss];
    if ([webView isEqual: _webView]) {
        [self filledQuestionProgress];

    }
    _webViewHeightConstrain.constant = _editWebView.scrollView.contentSize.height + 20;
    _previewScrollHeightConstrain.constant = _webView.scrollView.contentSize.height + 20;


}
-(void)viewDidLayoutSubviews{
    //[self setUpPreview];
}

- (void)viewDidLoad {

    [super viewDidLoad];
    
    recInfoArray =[[NSMutableArray alloc]init];
    ////////////  ###### editable Web View #####
    NSURL *url = [NSURL URLWithString:_templete.template_url];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_editWebView loadRequest:requestObj];

    ////////////// ############# end
    
    ////////////  ###### Web View #####
    NSURL *url2 = [NSURL URLWithString:_templete.template_url_view];
    NSURLRequest *requestObj2 = [NSURLRequest requestWithURL:url2];
    [_webView loadRequest:requestObj2];
    
    ////////////// ############# end
    // Do any additional setup after loading the view.
    recInfoArray = _templete.receiverInfoArray.mutableCopy;
    [_infoTabl reloadData];
    [_tblInfo2 reloadData];
    
    CGSize tableViewSize = _infoTabl.contentSize;
    _boxView2HeightConstrain.constant = 520 + tableViewSize.height;
    _userInfoBox2HeightConstrain.constant = tableViewSize.height + 80 ;
    _boxViewHeightConstrain.constant = 420 + tableViewSize.height;
    _userInfoBoxHeightConstrain.constant = tableViewSize.height + 80 ;
}



#pragma mark -Keyboard Method
-(void)keyboardWillShow {
    _isChageDoc = YES;

}

-(void)keyboardWillHide {
    [self  setAnswerWithQuestion];
//    if (_signatureBoxView.hidden == YES) {
//        
//        [self filledQuestionProgress];
//    }
}
-(NSString *) getTextViewAnswersWithTag:(NSString *)tag{
    NSString* value;
    
    value = [_editWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementById('%@').value;",tag]];
    
    return value;
    
    
}
-(NSString *) getAnswersWithTag:(NSString *)tag{
    NSString* value;

    value = [_editWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementById('%@').value ;",tag]];
    
    return value;
    

}

-(NSString *) getAnswersWithTag:(NSString *)tag andOptionValue:(NSString *)value{
    int index = [value intValue];
  NSString  *ans = [_editWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat: @"document.getElementsByName('%@')[%d].checked;",tag,index-1]];
    return ans;

}
-(NSString *) getCheckBoxAnswersWithTag:(NSString *)tag andOptionValue:(NSString *)value{
    int index = [value intValue];
    NSString  *ans = [_editWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementsByName('%@')[%d].checked ",tag,index-1]];
    return ans;
}
-(void) setAnswerWithQuestion{
    for ( Questions *question  in _questionArray) {
        NSString *ans;
        if ([question.question_type isEqualToString:@"editTextArea"]) {
            ans =[self getTextViewAnswersWithTag:question.tag_id];
            [question setAnswer:ans];
            
            if (![ans isEqualToString:@""]) {
                [question setIsFilled:YES];
                _isChageDoc = YES;
            }
            else{
                [question setIsFilled:NO];
                _isChageDoc = YES;
            }
        }
       else if ([question.question_type isEqualToString:@"editText"]) {
            ans =[self getAnswersWithTag:question.tag_id];
            [question setAnswer:ans];

            if (![ans isEqualToString:@""]) {
                [question setIsFilled:YES];
                _isChageDoc = YES;
            }
            else{
                [question setIsFilled:NO];
                _isChageDoc = YES;
            }
        }
        else if ([question.question_type isEqualToString:@"date"]) {
            ans =[self getAnswersWithTag:question.tag_id];
            [question setAnswer:ans];

            if (![ans isEqualToString:@""]) {

                [question setIsFilled:YES];
                _isChageDoc = YES;

            }
            
        }
        else if ([question.question_type isEqualToString:@"radio"]) {
            for (Options *opt in question.optionArray) {
                ans=  [self getAnswersWithTag:question.tag_id andOptionValue:opt.option_value];
                if ([ans isEqualToString:@"true"]) {

                    [question setOptionAnswerId:opt.option_id];
                    [question setOptionAnswerValue:opt.option_value];
                    [question setIsFilled:YES];
                    _isChageDoc = YES;

                }
                else{
                    }
            }
 
        }
        else if ([question.question_type isEqualToString:@"radio_option"]) {
            for (Options *opt in question.optionArray) {
                ans=  [self getAnswersWithTag:question.tag_id andOptionValue:opt.option_value];
                if ([ans isEqualToString:@"true"]) {
                    [question setOptionAnswerId:opt.option_id];
                    [question setOptionAnswerValue:opt.option_value];
                    [question setIsFilled:YES];
                    _isChageDoc = YES;
                    for (Questions *q in opt.questionArray) {
                        ans =[self getAnswersWithTag:q.tag_id];
                        [q setAnswer:ans];
                        
                        if (![ans isEqualToString:@""]) {
                            [q setIsFilled:YES];
                            _isChageDoc = YES;
                        }
                        else{
                            [q setIsFilled:NO];
                            _isChageDoc = YES;
                        }
                    }
                    
                }
                else{
                }
            }
        }
        else if ([question.question_type isEqualToString:@"spinner"]) {
            for (Options *opt in question.optionArray) {
                ans=  [self getAnswersWithTag:question.tag_id];
                if ([ans isEqualToString:opt.option_value]) {
                    [question setOptionAnswerId:opt.option_id];
                    [question setOptionAnswerValue:opt.option_value];

                    [question setIsFilled:YES];
                    _isChageDoc = YES;


                }
                else{
                }
            }

        }
        else if ([question.question_type isEqualToString:@"checkbox"]) {
            NSString *answesStr = [[NSString alloc]init];
            NSMutableArray *items  = [[NSMutableArray alloc]init];
            [question setOptionAnswerId:answesStr];
            [question setCheckeBoxOptionAnswerIdArray:items];
            [question setIsFilled:NO];

            for (Options *opt in question.optionArray) {
                ans=  [self getAnswersWithTag:question.tag_id andOptionValue:opt.option_value];
                if ([ans isEqualToString:@"true"]) {
                    
                    [items addObject:opt.option_id];
                    [question setIsFilled:YES];
                    _isChageDoc = YES;


                }
                else{
                }
            }
            answesStr = [items componentsJoinedByString:@","];
            [question setOptionAnswerId:answesStr];
            [question setCheckeBoxOptionAnswerIdArray:items.copy];
            
            
            
        }
       
           }
  

}

-(void) setUpPreview{
    for (Questions *question in _questionArray) {
        if ([question.question_type isEqualToString:@"editTextArea"]) {
            
            // if (question.isFilled) {
            [self updateWebViewDetailTxtWithTag:question.tag_id andValue:question.answer];
            //}
            
        }
        if ([question.question_type isEqualToString:@"editText"]) {
            
           // if (question.isFilled) {
                [self updateWebViewWithTag:question.tag_id andValue:question.answer];
            //}
            
        }
        else if ([question.question_type isEqualToString:@"date"]) {
        
            if (question.isFilled) {
                [self updateWebViewWithTag:question.tag_id andValue:question.answer];
                
            }
            
        }
        else if ([question.question_type isEqualToString:@"radio"]) {
            
           // if (question.isFilled) {
                
                [self updateWebViewWithTag:question.tag_id andOptionValue:question.optionAnswerValue];
          //  }
        }
        else if ([question.question_type isEqualToString:@"radio_option"]) {
            
            
                [self updateWebViewWithTag:question.tag_id andOptionValue:question.optionAnswerValue];
                for (Options *opt in question.optionArray) {
                        for (Questions *ques in opt.questionArray) {
                            
                                [self updateWebViewWithTag:ques.tag_id andValue:ques.answer];
                            
                        }
                    
                }
            
        }
        
        else if ([question.question_type isEqualToString:@"spinner"]) {
            if (question.isFilled) {
                [self updateWebViewWithTag:question.tag_id andValue:question.optionAnswerValue];
            }
        }
        else if ([question.question_type isEqualToString:@"checkbox"]) {
            
            //if (question.isFilled) {
                for (Options *o in question.optionArray) {
                    BOOL isTheObjectThere = [question.checkeBoxOptionAnswerIdArray containsObject:o.option_id];
                    
                    if (isTheObjectThere) {
                        [self updateWebViewWithTag:question.tag_id andOptionValue:o.option_value];
                        
                    }
                    else{
                        [self uncheckWebViewWithTag:question.tag_id andOptionValue:o.option_value];
                    }
                }
            //}
        }
    }
}

#pragma mark - PREVIEW UPDATE METHODS
-(void) updateWebViewDetailTxtWithTag:(NSString *)tag andValue:(NSString *)value{
    NSString *script = [NSString stringWithFormat:@"document.getElementById('%@').innerHTML = '%@';",tag, value];
    [self.editWebView stringByEvaluatingJavaScriptFromString:script];
    [self.webView stringByEvaluatingJavaScriptFromString:script];
    
}
-(void) updateWebViewWithTag:(NSString *)tag andValue:(NSString *)value{
    NSString *script = [NSString stringWithFormat:@"document.getElementById('%@').value = '%@';",tag, value];
    
    [self.editWebView stringByEvaluatingJavaScriptFromString:script];
    [self.webView stringByEvaluatingJavaScriptFromString:script];

}
-(void) updateWebViewWithTag:(NSString *)tag andOptionValue:(NSString *)value{
    int index = [value intValue];
    NSString *script = [NSString stringWithFormat:                       @"document.getElementsByName('%@')[%d].checked = %@",tag,index-1, value];
    [self.editWebView stringByEvaluatingJavaScriptFromString:script];
    [self.webView stringByEvaluatingJavaScriptFromString:script];

}
-(void) uncheckWebViewWithTag:(NSString *)tag andOptionValue:(NSString *)value{
    int index = [value intValue];
    NSString *script = [NSString stringWithFormat:                       @"document.getElementsByName('%@')[%d].checked = false",tag,index-1];
    [self.editWebView stringByEvaluatingJavaScriptFromString:script];
    [self.webView stringByEvaluatingJavaScriptFromString:script];

}
- (IBAction)btnEdit:(id)sender {
    
    if (_isFromPandig) {
        if (![_templete.sender_signature_image isEqualToString:@""] && _templete.sender_signature_image != nil) {
            [self showWarningAlert];
        }
        else{
            _previewScroll.hidden = YES;
            [_btnEdit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_btnPreview setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
            _editLine.hidden = false;
            _previewLine.hidden = YES;
        }
        }
    else{
    _previewScroll.hidden = YES;
    [_btnEdit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnPreview setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    _editLine.hidden = false;
    _previewLine.hidden = YES;
    }
}
- (IBAction)btnPreview:(id)sender {
  [self  setAnswerWithQuestion];
    [self setUpPreview];
    _previewScroll.hidden = NO;
    [_btnPreview setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnEdit setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    _previewLine.hidden = false;
    _editLine.hidden = YES;
}

- (IBAction)btnBack:(id)sender {
    filledCount = 0;
    for (Questions *q in _questionArray) {
        if (q.isFilled) {
            filledCount++;
        }
    }
    if ( _isfromDraft || _isFromPandig) {

    if (filledCount>0 && _isChageDoc) {
            [self showSaveDocumentAlert];

        }
    else{
        [DataManager sharedManager].isChageDoc = NO;
        
        [self.navigationController popViewControllerAnimated:YES];
    }
        
        
    }
    else{
        if (isDiscardContract) {
            [DataManager sharedManager].isChageDoc = YES;

        }
        else
        [DataManager sharedManager].isChageDoc = NO;
        
        [self.navigationController popViewControllerAnimated:YES];
    }


}

- (IBAction)btnTapOnePressed:(id)sender {

    _signatureBoxView.hidden = NO;
    isSenderView = YES;
    _senderSignatureView.userInteractionEnabled = YES;
    _tfFirstName.text = senderFName;
    _tfLastName.text = senderLName;
    _tfEmail.text = senderEmail;
    //_senderSignatureView.signatureImage = signatureImgSender;
    [_senderSignatureView erase];

    
}
- (IBAction)btnTapTwoPressed:(id)sender {
    _signatureBoxView.hidden = NO;
    isSenderView = NO;
    _senderSignatureView.userInteractionEnabled = NO;
    _tfFirstName.text = recFName;
    _tfLastName.text = recLName;
    _tfEmail.text = recEmail;
    
    
}

- (IBAction)btnSendSignature:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Signature Options" message:@"Contract send for Signature" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Sign Now and Send" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self sendContractWithSignature:YES];
        
        // Distructive button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Send Now and Sign Later" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        [self sendContractWithSignature:NO];
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}


- (IBAction)btnClear:(id)sender {
    [_senderSignatureView erase];
    
}
- (IBAction)btnSaveContractInfo:(id)sender {
    if ([self checktextFields]) {
        
        if (isSenderView) {
            senderFName =_tfFirstName.text;
            senderLName = _tfLastName.text;
            senderEmail = _tfEmail.text;
            _lblSenderName.text = senderFName;
            _lblSenderEmail.text =senderEmail;
            _lblPreviewSendeName.text =senderFName;
            _lblPreviewSenderEmail.text =senderEmail;
            _senderSignImg.image = _senderSignatureView.signatureImage;
            signatureImgSender = _senderSignatureView.signatureImage;
            _tfFirstName.text = @"";
            _tfLastName.text = @"";
            _tfEmail.text = @"";
            [_senderSignatureView erase];
            
        }
        else{
            recFName =_tfFirstName.text;
            recLName = _tfLastName.text;
            recEmail = _tfEmail.text;
            _lblRecName.text = _tfFirstName.text;;
            _lblRecEmail.text =_tfEmail.text;
            _lblPreviewReceiverName.text =recFName;
            _lblPreviewReceiverEmail.text =recEmail;
            _tfFirstName.text = @"";
            _tfLastName.text = @"";
            _tfEmail.text = @"";
            
            
            
        }
        _signatureBoxView.hidden = YES;
        
        
    }
}
- (IBAction)btnCancelContractInfo:(id)sender {
    _signatureBoxView.hidden = YES;
    _tfFirstName.text = @"";
    _tfLastName.text = @"";
    _tfEmail.text = @"";
    [_senderSignatureView erase];
    [checkFiled resignFirstResponder];
}
- (IBAction)btnHideSignatureView:(id)sender {
    // _signatureBoxView.hidden = YES;
    [checkFiled resignFirstResponder];
    
}


-(void) filledQuestionProgress{

    filledCount = 0;
    for (Questions *q in _questionArray) {
        if (q.isFilled) {
            filledCount++;
        }
    }
    if (_signatureBoxView.hidden==YES && filledCount == _questionArray.count) {
        if (!_isFromPandig &&_isfromDraft ) {
           // [self  showSendDocumentAlert];
            [self showAlertMessage:@"Document is completed.Please click edit to view and finalize?"];

        }
    }
}

-(void) settupSenderInfo{
    
  //  CGPoint bottomOffset = CGPointMake(0,500);
   // [_editScroll setContentOffset:bottomOffset animated:YES];
    [self btnTapOnePressed:nil];
    
}
#pragma mark - TextField Methods


-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}


-(BOOL)checktextFields{
    
    BOOL willEnabled = NO;
    if (isSenderView) {
        
        if( [_tfFirstName.text length] >= 1 && [_tfLastName.text length] >= 1 &&[self emailValidate:_tfEmail.text] == YES && _senderSignatureView.signatureImage!=nil ){
            willEnabled = YES;
        }
        if (_tfFirstName.text.length==0 || _tfLastName.text.length==0||_tfEmail.text.length==0) {
            [self showAlertMessage:@"Please enter valid informtation"];
            return NO;
        }
        else if (![self emailValidate:_tfEmail.text]) {
            [self showAlertMessage:@"Please enter valid email"];
            return NO;
        }
        else if (_senderSignatureView.signatureImage == nil) {
            [self showAlertMessage:@"Enter a Vaild Signature"];
            return NO;
        }
    }
    else{
        if( [_tfFirstName.text length] >= 1 && [_tfLastName.text length] >= 1 &&[self emailValidate:_tfEmail.text] == YES ){
            willEnabled = YES;
        }
        if (_tfFirstName.text.length==0 || _tfLastName.text.length==0||_tfEmail.text.length==0) {
            [self showAlertMessage:@"Please enter valid informtation"];
            return NO;
        }
        else if (![self emailValidate:_tfEmail.text]) {
            [self showAlertMessage:@"Please enter valid email"];
            return NO;
        }
        
        
    }
    
    return willEnabled;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    checkFiled = textField;
    [self animateTextField:nil up:YES];
    
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    [self animateTextField:nil up:NO];
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
          
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}

-(void)showSaveDocumentAlert{
    [self setAnswerWithQuestion];

    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Save",@"Discard",@"Cancel" ,nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"Wait!" message:@"Do you want to save this agreement as a draft?"]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            if (_isfromDraft) {
                [self saveDocument];
                
            }
            else{
                [self saveDocumentIntoDraf];
            }
            
        }
        else if(buttonIndex == 1){
            [DataManager sharedManager].isChageDoc = YES;
            
            [self.navigationController popViewControllerAnimated:YES];
            
        } else{
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
    
}

-(void)showSendDocumentAlert{
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Edit",@"Cancel" ,nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"Wait!" message:@"Document is completed.Please click edit to view and finalize?"]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
           // [self setSignatureApiCall];
                [self   settupSenderInfo];
            
        }
        else if(buttonIndex == 1){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
    
}

#pragma mark - API Calling
-(void) saveDocument{
    [DocumentService saveFilledDocumentWithAnswersArray:_questionArray andTemplateId:_templete.template_id success:^(id data) {
        [DataManager sharedManager].isChageDoc = YES;
        [DataManager sharedManager].isFromDraftSend = NO;
        
        [self.navigationController popViewControllerAnimated:YES];
        
    } failure:^(NSError *error) {
        
    } ];
}
-(void) saveDocumentIntoDraf{
    [DocumentService saveFilledDocumentWithAnswersArray:_questionArray andTemplateId:_templete.template_id success:^(id data) {
        isPnading =NO;
        
        [self performSegueWithIdentifier:@"moveToDraft" sender:nil];
        
    } failure:^(NSError *error) {
        
    } ];
}
-(void) setSignatureApiCall{

    if ([recFName length]>0&&[recLName length]>0&&[recEmail length]>0) {
   
                [DocumentService saveFilledDocumentWithAnswersArray:_questionArray andTemplateId:_templete.template_id success:^(id data) {
                    [DocumentService sendContractWithSenderFirstName:recFName lastName:recLName userImg:signatureImgSender eamil:recEmail templateID:_templete.template_id success:^(id data) {
                        isPnading = YES;
                        
                        [self performSegueWithIdentifier:@"moveToDraft" sender:nil];
                        
                    } failure:^(NSError *error) {
                        [self showAlertMessage:error.localizedDescription];
                    }];
                    
                    
                } failure:^(NSError *error) {
                    
                } ];
                
        
    
          
    }
    else if (signatureImgSender==nil){
        
        [self showAlertMessage:@"Please enter your Signature"];
        
    }
    else {
        [self showAlertMessage:@"Please enter complete informtation of receiver"];
        
    }
    


}

-(void) sendContractApi{
    
        if (_isfromDraft) {
            [DocumentService saveFilledDocumentWithAnswersArray:_questionArray andTemplateId:_templete.template_id success:^(id data) {
                [SVProgressHUD show];
                [DocumentService sendContractWithReceiverInfo:recInfoArray userImg:signatureImgSender templateID:_templete.template_id success:^(id data) {
                    [self showAlertMessage:data[@"message"]];
                    
                    [DataManager sharedManager].isChageDoc = YES;
                    [DataManager sharedManager].isFromDraftSend=YES;
                    [self.navigationController popViewControllerAnimated:YES];
                    
                } failure:^(NSError *error) {
                    [self showAlertMessage:error.localizedDescription];
                }];
                
                
            } failure:^(NSError *error) {
                
            } ];
            
            
            
            
            
            
        }
        else{
            if (!isSaved) {
                [self setAnswerWithQuestion];
                [DocumentService saveFilledDocumentWithAnswersArray:_questionArray andTemplateId:_templete.template_id success:^(id data) {
                    [SVProgressHUD show];
                    
                    [DocumentService sendContractWithReceiverInfo:recInfoArray userImg:signatureImgSender templateID:_templete.template_id success:^(id data) {
                        isPnading = YES;
                        
                        [self performSegueWithIdentifier:@"moveToDraft" sender:nil];
                        
                    } failure:^(NSError *error) {
                        [self showAlertMessage:error.localizedDescription];
                    }];
                    
                    
                } failure:^(NSError *error) {
                    
                } ];
                
            }
            else{
                [DocumentService  sendContractWithReceiverInfo:recInfoArray userImg:signatureImgSender templateID:_templete.template_id success:^(id data) {
                    [self showAlertMessage:data[@"message"]];
                    //isPnading = YES;
                    [self performSegueWithIdentifier:@"moveToDraft" sender:nil];
                    
                } failure:^(NSError *error) {
                    [self showAlertMessage:error.localizedDescription];
                }];
            }
        }
    
    


}

-(void) editPendingTemplate{
    [DocumentService editPendingTemplateWithTemplateID:_templete.template_id success:^(id data) {
     
        [self setUpViewForEditing];
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    }];

}

-(void) setUpViewForEditing{
    [recInfoArray removeAllObjects];
    _templete.receiverInfoArray =  recInfoArray.mutableCopy;
    [_infoTabl reloadData];
    [_tblInfo2 reloadData];
    CGSize tableViewSize = _infoTabl.contentSize;
    _boxView2HeightConstrain.constant = 520 + tableViewSize.height;
    _userInfoBox2HeightConstrain.constant = tableViewSize.height + 80 ;
    _boxViewHeightConstrain.constant = 420 + tableViewSize.height;
    _userInfoBoxHeightConstrain.constant = tableViewSize.height + 80 ;
    signatureImgSender = nil;
    _previewScroll.hidden = YES;
    [_btnEdit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnPreview setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    _editLine.hidden = false;
    _previewLine.hidden = YES;
    _senderSignImg.image = [UIImage imageNamed:@""];
    _templete.sender_signature_image = @"";
    isDiscardContract = YES;
}


-(void)sendContractWithSignature:(BOOL)state{
   
    receiverInfoView = [ReceiverInfo loadWithReceiverInfoViewWithSignature:state andRecInfoArray:recInfoArray];
    receiverInfoView.delegate = self;
    [self.view addSubview:receiverInfoView];
    
    
    [receiverInfoView show];
    
    
    
    
    
}
///////////////////// Receiver Delegates ////////////////
-(void)userInfo:(NSArray *)infoArray withSignature:(UIImage *)signImg{

    Receiver *obj = infoArray[0];
    recFName = obj.fName;
    recLName = obj.lName;
    recEmail = obj.email;
    signatureImgSender = signImg;
    recInfoArray = infoArray.mutableCopy;
    [_tblInfo2 reloadData];
    [_infoTabl reloadData];
    CGSize tableViewSize = _infoTabl.contentSize;
    _boxView2HeightConstrain.constant = 520 + tableViewSize.height;
    _userInfoBox2HeightConstrain.constant = tableViewSize.height + 80 ;
    _boxViewHeightConstrain.constant = 420 + tableViewSize.height;
    _userInfoBoxHeightConstrain.constant = tableViewSize.height + 80 ;
    [self sendContractApi];
    
}
-(void)cancelUserInfo:(NSMutableArray *)infoArray{

    recInfoArray = infoArray.mutableCopy;
    [_tblInfo2 reloadData];
    [_infoTabl reloadData];
    CGSize tableViewSize = _infoTabl.contentSize;
    _boxView2HeightConstrain.constant = 520 + tableViewSize.height;
    _userInfoBox2HeightConstrain.constant = tableViewSize.height + 80 ;
    _boxViewHeightConstrain.constant = 420 + tableViewSize.height;
    _userInfoBoxHeightConstrain.constant = tableViewSize.height + 80 ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
        return 0;
 
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Receiver *rec = recInfoArray[indexPath.row];
    if (indexPath.row == 0) {
        if([rec.receiver_signature_image isEqualToString:@""] || rec.receiver_signature_image ==nil){
            return 70;
        }
        return 140;
        
    }
    else
     return 140;
        
    

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        return  recInfoArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        Receiver *rec = recInfoArray[indexPath.row];
        InfoCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
        if (cell==nil) {
            cell =[[InfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        cell.lblName.text = [NSString stringWithFormat:@"%@  %@",rec.fName,rec.lName];
        cell.lblEmail.text = rec.email;
  
    if([rec.receiver_signature_image isEqualToString:@""] || rec.receiver_signature_image ==nil){
        cell.signImg.image   = [UIImage imageNamed:@"no-sig"];
        
    }
    else{
        [cell.signImg setImageWithURL:rec.receiver_signature_image];
        
    }
    
    
        return cell;
        
}
-(void) getNotifiTemplate{
   
    [CategoryService getTemplateForReceiverSignatureWithTemplateId:_templete.template_id userID:@"" isCreatedBy:@"" success:^(id data) {
        for (int i=0 ; i< [DataManager sharedManager].notificationArray.count ; i++) {
            NotificationObject *notif  = [DataManager sharedManager].notificationArray[i];
            if ([_templete.template_id isEqualToString:notif.template_id]) {
                
                [[DataManager sharedManager].notificationArray removeObjectAtIndex:i];
                [DataManager sharedManager].isChangeInReceived = YES;

                break;
            }
        }
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    }];
}



-(void)showWarningAlert{
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Edit",@"Cancel" ,nil]];
    [customAlertView setContainerView:[self createFriendsDialog:@"Warning!" message:@"You have already signed the contract and marked it completed. If you wish to edit this, current version will be discarded." imageUrl:@""]];
         // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
            [self editPendingTemplate];
            
        }
      else{
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
    
}


-(UIView *)createFriendsDialog:(NSString *)title message:(NSString *)message imageUrl:(NSString *)imageUrl
{
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 170)];
    if([message isEqualToString:@""] || message == nil){
        demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    }
  
    UILabel *headingView = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 270, 20)];
    UILabel *descriptionView = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 270, 90)];
    headingView.text = title;
    descriptionView.text = message;
    descriptionView.minimumFontSize = 8;
    descriptionView.adjustsFontSizeToFitWidth = YES;
    descriptionView.numberOfLines = 0;
    descriptionView.textColor = [UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
    headingView.textColor = [UIColor blackColor];//colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
    descriptionView.textAlignment = NSTextAlignmentCenter;
    headingView.textAlignment = NSTextAlignmentCenter;
    headingView.font = [UIFont fontWithName:@"PassionOne-Bold" size:16];
    descriptionView.font = [UIFont fontWithName:@"ArchivoNarrow-Bold" size:14];
    [demoView addSubview:headingView];
    [demoView addSubview:descriptionView];
    
    

    return demoView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
