//
//  LoginVC.h
//  LawNote
//
//  Created by Apple Txlabz on 03/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ForgotView.h"
@interface LoginVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *boxView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordBGImg;

@property (weak, nonatomic) IBOutlet UIImageView *emailBGImg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxViewTopContrain;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@end
