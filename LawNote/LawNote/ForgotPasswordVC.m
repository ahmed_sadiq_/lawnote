//
//  ForgotPasswordVC.m
//  LawNote
//
//  Created by Samreen Noor on 08/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "AccountService.h"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"
@interface ForgotPasswordVC ()
{
    UIGestureRecognizer *tapper;

}
@end

@implementation ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self popBoxConfig];
    // Do any additional setup after loading the view.
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)forgotPassword:(id)sender {
    if ([self emailValidate:_tfEmail.text]) {
        [AccountService forgetPasswordWhereEmail:_tfEmail.text succuss:^(id data) {
            [self showAlertMessage:data[@"message"]];
            
        } failure:^(NSError *error) {
            [self showAlertMessage:error.localizedDescription];
            _emailBgImg.image = [UIImage imageNamed:@"txt_bgred"];

        }];
    }
    else{
        //[self showAlertMessage:@"Please enter valid email"];
        _emailBgImg.image = [UIImage imageNamed:@"txt_bgred"];

    }
    
    
    
}





-(void)popBoxConfig{
    _boxView.layer.cornerRadius = 3.0;
    _boxView.layer.masksToBounds = YES;
    _btnReset.layer.cornerRadius = 3.0;
    _btnReset.layer.masksToBounds = YES;
    
    self.tfEmail.text = @"";
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapper];
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.scrollView endEditing:YES];
}


#pragma mark - TextField Methods


-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{

    [self.scrollView setContentOffset:CGPointMake(0, 60 * textField.tag-1) animated:YES];


}
-(void)textFieldDidEndEditing:(UITextField *)textField{

    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    if (![_tfEmail.text isEqualToString:@""]) {
        
        if (![self emailValidate:_tfEmail.text]) {
            _emailBgImg.image = [UIImage imageNamed:@"txt_bgred"];
            
        }
        else{
            _emailBgImg.image = [UIImage imageNamed:@"Rectangle 233"];
            
            
        }
    }

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
