//
//  ProfileViewController.h
//  LawNote
//
//  Created by Samreen Noor on 07/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *tfFirstName;
@property (weak, nonatomic) IBOutlet UITextField *tfLastName;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UIView *chnagePasswordView;
@property ( nonatomic) BOOL ischnagePasswordView;
@property (weak, nonatomic) IBOutlet UIButton *btnResetPas;

@end
