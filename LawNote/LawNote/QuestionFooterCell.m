//
//  QuestionFooterCell.m
//  LawNote
//
//  Created by Samreen Noor on 25/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "QuestionFooterCell.h"

@implementation QuestionFooterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _btnSaveAgreement.layer.cornerRadius = 5; // this value vary as per your desire
    _btnSaveAgreement.clipsToBounds = YES;}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
