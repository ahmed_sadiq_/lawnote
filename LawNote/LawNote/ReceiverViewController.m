//
//  ReceiverViewController.m
//  LawNote
//
//  Created by Samreen Noor on 10/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ReceiverViewController.h"
#import "Constant.h"
#import "CategoryService.h"
#import "DataManager.h"
#import "Templates.h"
#import "Questions.h"
#import "UIImageView+URL.m"
#import "DocumentService.h"
#import "Options.h"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"
#import "InfoCell.h"
#import "Receiver.h"
#import "NavigationHandler.h"


@interface ReceiverViewController ()<UIWebViewDelegate>{
    Templates *template;
    NSString *temId;
    NSMutableArray *recInfoArray;
}
@end

@implementation ReceiverViewController
-(void)setupView{
  
    _btnSendSignature.layer.cornerRadius = 5; // this value vary as per your desire
    _btnSendSignature.clipsToBounds = YES;
    if (!_isFromReceiveTempVC && !_isCompleted) {
        
   NSString  *senderFName = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"];
      NSString  *senderLName = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_lastName"];
    
      NSString  *senderEmail = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_email"];
    
   // _lblSenderName.text =senderFName;
   // _lblSenderEmail.text =senderEmail;
    }
    ////////////  ###### Web View #####
    NSURL *url = [NSURL URLWithString:template.template_url_view];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
    ////////////// ############# end
    
    if(IS_IPHONE_6Plus)
    {
        self.webViewHeightConstrain.constant= 736-60;
        
    }
    else if(IS_IPHONE_5)
    {
        
        self.webViewHeightConstrain.constant= 568-60;
        
    }
   
    
    
}


-(void) resetCompletedContractView{
    _lblTitle.text = template.template_title;
    _lblRecName.text = template.receiver_first_name;
    _lblRecEmail.text =template.receiver_email;
    _lblSenderName.text = template.sender_first_name;
    _lblSenderEmail.text =template.sender_email;
    if (![template.sender_signature_image isEqualToString:@""]) {

    [_senderSignImg setImageWithURL:template.sender_signature_image];
    }
    if (_isCompleted) {
        [self resetReceiverInfoBoxFromCompletedScreen];
        Receiver *rec = recInfoArray[0];

        if (![rec.receiver_signature_image isEqualToString:@""]) {

        [_receiverImg setImageWithURL:rec.receiver_signature_image];
        }
        _receiverSignView.userInteractionEnabled=NO;
        _receiverSignView.backgroundColor = [UIColor whiteColor];
        _receiverSignView.hidden = YES;

        _receiverImg.hidden = NO;
        _btnClear.hidden=YES;
        _signViewHeightConstrain.constant = 0;
        _signatureSendView.hidden= YES;
        
        [DataManager sharedManager].isChageDoc = NO;
  
        _lblSenderName.text = template.sender_first_name;
        _lblSenderEmail.text =template.sender_email;

    }
  if(_isFromReceiveTempVC){
        _lblSenderName.text = template.sender_first_name;
        _lblSenderEmail.text =template.sender_email;
      [self resetReceiverInfoBox];
      
    
    }

    

}
-(void) resetReceiverInfoBoxFromCompletedScreen{
    recInfoArray = template.receiverInfoArray.mutableCopy;
    [_infoTabl reloadData];
    _infoTblTopConstrain.constant = 65;
    _recInfoBox.constant = _infoTabl.contentSize.height + 80;
    _UperBoxConstrain.constant = _infoTabl.contentSize.height + 400;
}
-(void) resetReceiverInfoBox{
    recInfoArray = template.receiverInfoArray.mutableCopy;
    [_infoTabl reloadData];
    Receiver *rec = recInfoArray[0];
    if (![rec.receiver_signature_image isEqualToString:@""] && rec.receiver_signature_image != nil) {
        
        _infoTblTopConstrain.constant = 65;
        _recInfoBox.constant = _infoTabl.contentSize.height + 80;
        _UperBoxConstrain.constant = _infoTabl.contentSize.height + 450;
        _receiverSignView.hidden = YES;
        _btnClear.hidden = YES;
        _btnSendSignature.hidden = YES;
    }
    else{
      
        _infoTblTopConstrain.constant = 65;
        _signViewHeightConstrain.constant = 10;
        //////////////////////////////////////
        _recInfoBox.constant = _infoTabl.contentSize.height + 80 ;
        _UperBoxConstrain.constant = _infoTabl.contentSize.height + 450;
        if ([template.sender_signature_image isEqualToString:@""] ||template.sender_signature_image == nil ) {
            [_receiverSignView setUserInteractionEnabled:NO];
            _btnSendSignature.hidden = YES;
        }
        else{
            _lblReceIverPart.text = @"Tap To Sign";
            _btnTabReceiverSign.hidden = NO;
            _btnSendSignature.hidden = NO;
            _UperBoxConstrain.constant = _infoTabl.contentSize.height + 500;

        }
    }
  

}
-(void) resetReceiveContractView{
    _lblTitle.text = template.template_title;
    _lblSenderName.text = template.receiver_first_name;
    _lblSenderEmail.text =template.receiver_email;
    if (![template.receiver_signature_image isEqualToString:@""]) {

    [_senderSignImg setImageWithURL:template.receiver_signature_image];
    }
    _lblRecName.text =template.sender_first_name;
    _lblRecEmail.text =template.sender_email;
    _lblAuthorPart.text = @"Receiver Signature and Information";
    _lblReceIverPart.text = @"Author Signature and Information";
}


-(void) setupViewFromEmail{
    User *user = [DataManager sharedManager].currentUser;
    if ([user.userID isEqualToString:template.sender_id]) {
        
        [self resetReceiveContractView];
        [self setUpPreview];
        
    }
    else{
        [self resetCompletedContractView];
        [self setUpPreview];
    }

}
-(void)viewWillAppear:(BOOL)animated{
    
    [self setupView];
    [self setUpPreview];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    recInfoArray =[[NSMutableArray alloc]init];
    template = [[Templates alloc]init];
    if (_isCompleted) {
        template = _templ;
        [self getNotifiTemplate];

       // [self resetCompletedContractView];
       // [self setUpPreview];
    }
    else if(_isFromReceiveTempVC){
        template = _templ;

        temId = template.template_id;

        [self getNotifiTemplate];
//        User *user = [DataManager sharedManager].currentUser;
//        if ([user.userID isEqualToString:_templ.sender_id]) {
//            template = _templ;
//            
//            temId = template.template_id;
//            [self resetReceiveContractView];
//            [self setUpPreview];
//
//        }
//        else{
//        template = _templ;
//            temId = template.template_id;
//
//        [self resetCompletedContractView];
//        [self setUpPreview];
//        }
    
    }
    else{
    template = [DataManager sharedManager].receiverTemplate;
    temId = template.template_id;
        
    [self getTemplate];
    }
    
}
-(void) setupReceiveCompletedTemplate{
    if (_isCompleted) {
        template = _templ;
        
        [self resetCompletedContractView];
        [self setUpPreview];
    }
    else if(_isFromReceiveTempVC){
        template = _templ;
        
        temId = template.template_id;
        
        User *user = [DataManager sharedManager].currentUser;
        if ([user.userID isEqualToString:_templ.sender_id]) {
            template = _templ;
            
            temId = template.template_id;
            [self resetReceiveContractView];
            [self setUpPreview];
            
        }
        else{
            template = _templ;
            temId = template.template_id;
            
            [self resetCompletedContractView];
            [self setUpPreview];
        }
        
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnReload:(id)sender {
    [self setUpPreview];

}
- (IBAction)btnMoveToHome:(id)sender {
    if (_isCompleted||_isFromReceiveTempVC) {
        [self.navigationController popViewControllerAnimated:YES];

    }
    else{
    [self performSegueWithIdentifier:@"home" sender:nil];
    }

}
- (IBAction)btnSendSignature:(id)sender {
    if (_receiverSignView.signatureImage!=nil) {
        
    [DocumentService sendReceiverSignatureWithTemplateID:temId userSignImg:_receiverSignView.signatureImage success:^(id data) {
       if(_isFromReceiveTempVC){
           [DataManager sharedManager].isChageDoc= YES;

           [self.navigationController popViewControllerAnimated:YES];

       }else{
           [DataManager sharedManager].isCompleted= YES;

        [self performSegueWithIdentifier:@"home" sender:nil];
       }
        
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    } ];
    }
    else{
        [self showAlertMessage:@"Please enter valid signature"];

    }
}

-(void)viewDidLayoutSubviews{
   // if (!_isCompleted) {
        if ([template.is_created_by_user isEqualToString:@"1"]) {

    CGSize size = [template.template_description sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    
    _webViewHeightConstrain.constant = size.height+80.0;
        }
   // }
}
-(void) getTemplate{

    [CategoryService getTemplateForReceiverSignatureWithTemplateId:template.template_id userID:template.userId isCreatedBy:template.is_created_by_user success:^(id data) {
        template = data;
//        NSURL *url = [NSURL URLWithString:template.template_url];
//        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//        [_webView loadRequest:requestObj];
        [self setUpPreview];
        [self setupViewFromEmail];
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    }];
}
-(void) getNotifiTemplate{
    
    [CategoryService getTemplateForReceiverSignatureWithTemplateId:template.template_id userID:template.userId isCreatedBy:template.is_created_by_user success:^(id data) {
        template = data;
        _templ = template;
        [self setupReceiveCompletedTemplate];
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    }];
}
#pragma mark -WebView Setup Methods
-(void) setUpPreview{
    for (Questions *question in template.questionArray) {
        if ([question.question_type isEqualToString:@"editTextArea"]) {
            
            [self updateWebTextDetailViewWithTag:question.tag_id andValue:question.answer];
            
            
        }
        if ([question.question_type isEqualToString:@"editText"]) {
            
                [self updateWebViewWithTag:question.tag_id andValue:question.answer];
            
            
        }
        else if ([question.question_type isEqualToString:@"date"]) {
            
                [self updateWebViewWithTag:question.tag_id andValue:question.answer];
                
            
            
        }
        else if ([question.question_type isEqualToString:@"radio"]) {
            
            
                [self updateWebViewWithTag:question.tag_id andOptionValue:question.optionAnswerValue];
                
            
            
            
        }
        else if ([question.question_type isEqualToString:@"radio_option"]) {
            
            
            [self updateWebViewWithTag:question.tag_id andOptionValue:question.optionAnswerValue];
            for (Options *opt in question.optionArray) {
                for (Questions *ques in opt.questionArray) {
                    
                    [self updateWebViewWithTag:ques.tag_id andValue:ques.answer];
                    
                }
                
            }
            
        }
        
        else if ([question.question_type isEqualToString:@"spinner"]) {
                [self updateWebViewWithTag:question.tag_id andValue:question.optionAnswerValue];
                
            
            
        }
        else if ([question.question_type isEqualToString:@"checkbox"]) {
            if (question.isFilled) {
                for (Options *o in question.optionArray) {
                    BOOL isTheObjectThere = [question.checkeBoxOptionAnswerIdArray containsObject:o.option_id];
                    
                    if (isTheObjectThere) {
                        [self updateWebViewWithTag:question.tag_id andOptionValue:o.option_value];
                        
                    }
                  
                }
                
            }
               // [self updateWebViewWithTag:question.tag_id andOptionValue:question.optionAnswerValue];
                
            
            
        }
        
        
        
    }
    
    
}
-(void) updateWebTextDetailViewWithTag:(NSString *)tag andValue:(NSString *)value{
    NSString *script = [NSString stringWithFormat:@"document.getElementById('%@').innerHTML = '%@';",tag, value];
    
    [self.webView stringByEvaluatingJavaScriptFromString:script];
    
}
-(void) updateWebViewWithTag:(NSString *)tag andValue:(NSString *)value{
    NSString *script = [NSString stringWithFormat:@"document.getElementById('%@').value = '%@';",tag, value];
    
    [self.webView stringByEvaluatingJavaScriptFromString:script];
    
}
-(void) updateWebViewWithTag:(NSString *)tag andOptionValue:(NSString *)value{
    int index = [value intValue];
    NSString *script = [NSString stringWithFormat:                       @"document.getElementsByName('%@')[%d].checked = %@",tag,index-1, value];
    [self.webView stringByEvaluatingJavaScriptFromString:script];
}

#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"finish");
    [self setUpPreview];
    _webViewHeightConstrain.constant = _webView.scrollView.contentSize.height + 20;

 

}
- (IBAction)btnClear:(id)sender {
    [_receiverSignView erase];
    

}
- (IBAction)btnReceiverTabToSign:(id)sender {
    [[NavigationHandler getInstance] rotateScreenInLandScape];
    _signatureBoxView.hidden = NO;
}
- (IBAction)btnSaveReceiverSign:(id)sender {
    if (_receiverSignView.signatureImage != nil) {
        _signatureBoxView.hidden = YES;
        [[NavigationHandler getInstance] restrictRotation:@"2"];
        [[NavigationHandler getInstance] rotateScreenPortrait];
        [[NavigationHandler getInstance] restrictRotation:@"0"];

    }

}
- (IBAction)btnCancelRecSign:(id)sender {
    [[NavigationHandler getInstance] restrictRotation:@"2"];
    [[NavigationHandler getInstance] rotateScreenPortrait];

    [_receiverSignView erase];
    _signatureBoxView.hidden = YES;
    [[NavigationHandler getInstance] restrictRotation:@"0"];
    

}

#pragma mark - UITableView Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{   Receiver *rec = recInfoArray[indexPath.row];
    if (indexPath.row == 0) {
        if([rec.receiver_signature_image isEqualToString:@""] || rec.receiver_signature_image ==nil){
            return 70;
        }
        return 140;

    }
    else
        
        return 140;
    
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  recInfoArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Receiver *rec = recInfoArray[indexPath.row];
    InfoCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if (cell==nil) {
        cell =[[InfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }

    cell.lblName.text = [NSString stringWithFormat:@"%@  %@",rec.fName,rec.lName];
    cell.lblEmail.text = rec.email;
    if([rec.receiver_signature_image isEqualToString:@""] || rec.receiver_signature_image ==nil){
        cell.signImg.image   = [UIImage imageNamed:@"no-sig"];

    }
    else{
        [cell.signImg setImageWithURL:rec.receiver_signature_image];

    }
    
    
    return cell;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
