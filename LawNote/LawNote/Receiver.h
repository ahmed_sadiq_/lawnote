//
//  Receiver.h
//  LawNote
//
//  Created by Samreen on 02/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface Receiver : BaseEntity
@property (nonatomic, strong) NSString * fName;
@property (nonatomic, strong) NSString * lName;
@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * receiver_signature_image;

- (id)initWithDictionary:(NSDictionary *) responseData;
+ (NSArray *)mapReceiverInfoFromArray:(NSArray *)arrlist;

@end
