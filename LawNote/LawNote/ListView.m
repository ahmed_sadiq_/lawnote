//
//  ListView.m
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ListView.h"
#import "Options.h"
#import "RCEasyTipView.h"
@implementation ListView
{
    NSString *returnStr;
    NSUInteger currentSelectedIndex;
    Questions *question;
}

- (IBAction)btnInfo:(id)sender {
    
    RCEasyTipPreferences *preferences = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferences.drawing.backgroundColor = [UIColor  colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
    preferences.animating.showDuration = 1.5;
    preferences.animating.dismissDuration = 1.5;
    preferences.animating.dismissTransform = CGAffineTransformMakeTranslation(0, 100);
    preferences.animating.showInitialTransform = CGAffineTransformMakeTranslation(0, -100);
    preferences.shouldDismissOnTouchOutside = YES;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferences];
    tipView.text =question.question_description;
    tipView.delegate = self;
    [tipView showAnimated:YES forView:sender withinSuperView:nil];
    
    
    
    
}

-(void) setupView{
    
    if ([question.question_description isEqualToString:@""]||[question.question_description isEqual:nil]) {
        _btnInfo.hidden = YES;
        _infoImg.hidden = YES;
    }
    returnStr=@"";
    self.dropDownView.dataSource = self;
    self.dropDownView.delegate = self;
    self.lblTitle.text = question.question_text;
    if (question.isFilled) {
       

        if (![question.answer isEqualToString:@""]) {
            for (Options *o in question.optionArray) {
                if ([o.option_id isEqualToString:question.answer]) {
                    _lblSelectOption.text = o.option_text;

                }
            }
        }
        

    }
}
+ (id) loadWithNibForListView:(Questions *)question andQuestionNumber:(NSString *)questionNumber{
    //int device = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)?0:1;
    
    
    ListView *view = (ListView *)[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ListView class]) owner:nil options:nil][0];
    
    [view setFrame:[UIScreen mainScreen].bounds];
    
    view.alpha = 0.0;
    view.lblQuestionCount.text = questionNumber;
    view->question = question;
    [view setupView];
    return view;
}

-(void) show{
       
        self.alpha = 1.0;
        
 
}
- (IBAction)btnHideList:(id)sender {
    
//    [UIView animateWithDuration:0.5 animations:^{
//        self.topConstrain.constant = -135;
//    } completion:^(BOOL finished) {
//        self.tblView.hidden = YES;
//        
//    }];
    self.superview.layer.borderWidth = 1.5;
    self.superview.layer.borderColor = [[UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0] CGColor];
    
}

#pragma mark - TableView Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 30;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return question.optionArray.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     Options *option = [question.optionArray objectAtIndex:indexPath.row];
    
    static NSString *cellIdentifier = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    
    cell.textLabel.text = option.option_text;
    [ cell.textLabel setFont: [cell.textLabel.font fontWithSize: 13]];

    cell.backgroundColor = [UIColor colorWithRed:221.0/255.0 green:221.0/255.0 blue:221.0/255.0 alpha:1];
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Options *option = [question.optionArray objectAtIndex:indexPath.row];

    _lblSelectOption.text = option.option_text;
    [question setIsFilled:YES];
    [question setAnswer:option.option_text];
    [question setOptionAnswerId:option.option_id];
    [question setOptionAnswerValue:option.option_value];

    [self.delegate selectedOption:question];
    self.tblView.hidden = YES;
        self.topConstrain.constant = -135;
  
    

}






- (IBAction)btnSelection:(id)sender {
    if (_tblView.hidden == YES) {
        self.tblView.hidden = NO;

        [UIView animateWithDuration:0.5 animations:^{
            self.topConstrain.constant = 0;
            
        } completion:^(BOOL finished) {
            
        }];
    }
    else{

        [UIView animateWithDuration:0.5 animations:^{
            self.topConstrain.constant = -135;
        } completion:^(BOOL finished) {
            self.tblView.hidden = YES;

        }];

    }
    
}



#pragma mark - MKDropdownMenuDataSource

- (NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu {
    return 1;
}

- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component {
    return  question.optionArray.count ;
}

#pragma mark - MKDropdownMenuDelegate
- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu widthForComponent:(NSInteger)component {
    return MAX(dropdownMenu.bounds.size.width/3, 125);
}

- (BOOL)dropdownMenu:(MKDropdownMenu *)dropdownMenu shouldUseFullRowWidthForComponent:(NSInteger)component {
         [self btnHideList:nil];
 
    return NO;
}
- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu rowHeightForComponent:(NSInteger)component {
    return 40;
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForComponent:(NSInteger)component {
    return [[NSAttributedString alloc] initWithString:returnStr
                                           attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightLight],
                                                        NSForegroundColorAttributeName: [UIColor blackColor]}];
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    Options *option = [question.optionArray objectAtIndex:row];

    NSMutableAttributedString *string =
    [[NSMutableAttributedString alloc] initWithString: option.option_text
                                           attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14 weight:UIFontWeightLight],
                                                        NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
   
    return string;
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    Options *option = [question.optionArray objectAtIndex:row];

    _lblSelectOption.text = option.option_text;
    [question setIsFilled:YES];
    [question setAnswer:option.option_text];
    [question setOptionAnswerId:option.option_id];
    [question setOptionAnswerValue:option.option_value];
    
    [self.delegate selectedOption:question];
}

- (UIColor *)dropdownMenu:(MKDropdownMenu *)dropdownMenu backgroundColorForRow:(NSInteger)row forComponent:(NSInteger)component {
    return nil;//[UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
}

- (UIColor *)dropdownMenu:(MKDropdownMenu *)dropdownMenu backgroundColorForHighlightedRowsInComponent:(NSInteger)component {
    return [UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:0.5];
}


@end
