//
//  DetailTextBox.h
//  LawNote
//
//  Created by Samreen Noor on 02/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Questions.h"
#import "DetailTextView.h"

@protocol DetailTextBoxDelegate

@optional
-(void) fillDetailQuestion:(Questions *)ques;
//- delegate method;

@end

@interface DetailTextBox : UIView <DetailTextViewDelegate>
@property (assign)id<DetailTextBoxDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnInfo;
@property (weak, nonatomic) IBOutlet UIImageView *infoImg;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet UILabel *txtView;
+ (id) loadWithNibForEditTextWithId:(id) uiviewcolroler questionView:(Questions *)question andQuestionNumber:(NSString *)questionNumber;
-(void) show;
@end
