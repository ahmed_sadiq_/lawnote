//
//  OptionsQuestion.m
//  LawNote
//
//  Created by Samreen Noor on 05/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "OptionsQuestion.h"

@implementation OptionsQuestion
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.question_id = [NSString stringWithFormat:@"%@",[self validStringForObject:responseData[@"question_id"]]];
        self.question_text = [self validStringForObject:responseData[@"question_text"]];
        self.question_type =[self validStringForObject:responseData[@"question_type"]];
        self.option_id =[self validStringForObject:responseData[@"option_id"]];
        self.tag_id =[self validStringForObject:responseData[@"tag_id"]];
        self.answer =[self validStringForObject:responseData[@"answer"]];
        if (![self.answer isEqualToString:@""]) {
            self.isFilled = YES;
        }
    }
    return self;
}




+(NSArray *) mapQuestionsFromArray:(NSArray *) arrlist{
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[OptionsQuestion alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}


@end
