//
//  ListView.h
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Questions.h"
#import "MKDropdownMenu.h"
@protocol ListViewDelegate

@optional
-(void) selectedOption:(Questions *)question;
//- delegate method;

@end
@interface ListView : UIView<UITableViewDataSource,UITableViewDelegate,MKDropdownMenuDelegate,MKDropdownMenuDataSource>
@property (weak, nonatomic) IBOutlet MKDropdownMenu *dropDownView;


@property (weak, nonatomic) IBOutlet UIButton *btnInfo;
@property (weak, nonatomic) IBOutlet UIImageView *infoImg;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstrain;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectOption;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (assign)id<ListViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestionCount;

+ (id) loadWithNibForListView:(Questions *)question andQuestionNumber:(NSString *)questionNumber;
-(void) show;

-(void) hide;
@end
