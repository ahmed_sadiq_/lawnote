//
//  QuestionViewCell.h
//  LawNote
//
//  Created by Samreen Noor on 23/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *QuestionBox;
@property (weak, nonatomic) IBOutlet UIView *boxView;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestionTxt;

@end
