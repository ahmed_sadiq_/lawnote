//
//  CheckBoxView.h
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Questions.h"
@protocol CheckBoxViewDelegate

@optional
-(void) filledCheckBox:(Questions *)question;
//- delegate method;

@end
@interface CheckBoxView : UIView<UITabBarDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *btnInfo;
@property (weak, nonatomic) IBOutlet UIImageView *infoImg;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestionCount;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) Questions *question;



@property (assign)id<CheckBoxViewDelegate> delegate;

+ (id) loadWithNibForCheckBox:(Questions *)question andQuestionNumber:(NSString *)questionNumber;
-(void) show;

-(void) hide;
@end
