//
//  QuestionFooterCell.h
//  LawNote
//
//  Created by Samreen Noor on 25/01/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionFooterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnSaveAgreement;

@end
