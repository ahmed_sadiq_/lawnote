//
//  SignUpVC.m
//  LawNote
//
//  Created by Apple Txlabz on 03/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "SignUpVC.h"
#import "AccountService.h"
#import "UIView+AlertView.h"
#import "CustomIOSAlertView.h"
@interface SignUpVC ()
{
    UIGestureRecognizer *tapper;
    BOOL isAgree;
}
@end

@implementation SignUpVC

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
-(void) setUpView{
    isAgree =NO;
    _boxView.layer.masksToBounds = true;
    _boxView.layer.cornerRadius = 3;
    _btnRegister.layer.masksToBounds = true;
    _btnRegister.layer.cornerRadius = 3.0;
    if (IS_IPHONE_6Plus) {
 
    _viewTopContrain.constant=40;
    }
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapper];
    
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.scrollView setContentOffset:CGPointMake(0, 0 ) animated:YES];

    [self.scrollView endEditing:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
}
#pragma mark - IBActions
- (IBAction)btnRegister:(id)sender {
    if (self.checktextFields) {
        [self registerAPI];  //api calling
    }
}
- (IBAction)btnBackToLogin:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
- (IBAction)btnAgree:(id)sender {
 UIImage *chkImg =   [UIImage imageNamed:@"Fill"];
    if ([_imgbox.image isEqual:chkImg]) {
        _imgbox.image =[UIImage imageNamed:@"check-box"];
        isAgree=NO;
    }
    else{
        _lblTermsCondition.textColor = [UIColor lightGrayColor];

    _imgbox.image =[UIImage imageNamed:@"Fill"];
        isAgree =YES;
    }
}

#pragma mark - TextField Methods


-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}


-(BOOL)checktextFields{
    
    BOOL willEnabled = NO;
    if( [_tfFirstName.text length] >= 1 && [_tfLastName.text length] >= 1 &&[self emailValidate:_tfEmail.text] == YES && [_tfPassword.text length] >= 5 && isAgree){
        willEnabled = YES;
    }
    if (_tfFirstName.text.length==0 && _tfLastName.text.length==0 && _tfPassword.text.length==0 &&_tfEmail.text.length==0 && !isAgree) {
       // [self showAlertMessage:@"Please enter data carefully"];
        return NO;
    }
    else if (_tfFirstName.text.length==0 ){
       // [self showAlertMessage:@"Please enter First Name"];
        _bgFirstImg.image = [UIImage imageNamed:@"txt_bgred"];

    }
    else if (_tfLastName.text.length==0 ){
        //[self showAlertMessage:@"Please enter Last Name"];
        _bgLastIMg.image = [UIImage imageNamed:@"txt_bgred"];

    }
   else if (![self emailValidate:_tfEmail.text]) {
      //  [self showAlertMessage:@"Please enter valid email"];
       _bgEmailImg.image = [UIImage imageNamed:@"txt_bgred"];

        return NO;
    }
   else if (_tfPassword.text.length <5) {
       _bgPasswordImg.image = [UIImage imageNamed:@"txt_bgred"];

        //[self showAlertMessage:@"Choose a Strong password"];
        return NO;
    }
   else if(!isAgree){
       _lblTermsCondition.textColor = [UIColor redColor];
      // [self showAlertMessage:@"You must agree to the Terms and Conditions to continue."];

   }
 
    return willEnabled;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{

    
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self.scrollView setContentOffset:CGPointMake(0, 60 * textField.tag-1) animated:YES];
    

    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField isEqual:_tfPassword]){
        [self.scrollView setContentOffset:CGPointMake(0, 0 ) animated:YES];
         if (_tfPassword.text.length <5) {
             //_bgPasswordImg.image = [UIImage imageNamed:@"txt_bgred"];//Rectangle 233
        }
         else{
                     _bgPasswordImg.image = [UIImage imageNamed:@"Rectangle 233"];

         }
    }
   else if ([_tfEmail isEqual:textField]) {
       if (![_tfEmail.text isEqualToString:@""]) {

        if (![self emailValidate:_tfEmail.text]) {
            _bgEmailImg.image = [UIImage imageNamed:@"txt_bgred"];

        }
        else{
            _bgEmailImg.image = [UIImage imageNamed:@"Rectangle 233"];

        
        }
       }

    }
   else if ([_tfFirstName isEqual:textField]) {
       if ([_tfFirstName.text isEqualToString:@""]) {
           //_bgFirstImg.image = [UIImage imageNamed:@"txt_bgred"];

       }
       else{
           _bgFirstImg.image = [UIImage imageNamed:@"Rectangle 233"];
       }
    }
   else if ([_tfLastName isEqual:textField]) {
       if ([_tfLastName.text isEqualToString:@""]) {
         //  _bgLastIMg.image = [UIImage imageNamed:@"txt_bgred"];

       }
       else{
           _bgLastIMg.image = [UIImage imageNamed:@"Rectangle 233"];

       }
   }
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual:_tfPassword]) {
   
    if (_tfPassword.text.length >5) {
   
        _bgPasswordImg.image = [UIImage imageNamed:@"Rectangle 233"];
        
    }
    else{
        _bgPasswordImg.image = [UIImage imageNamed:@"txt_bgred"];

    }
    
    }
    return YES;

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    if ([textField isEqual:_tfFirstName]) {
        
        [_tfLastName becomeFirstResponder];
    }
    if ([textField isEqual:_tfLastName]) {
        
        [_tfEmail becomeFirstResponder];
    }
    if ([textField isEqual:_tfEmail]) {
        
        [_tfPassword becomeFirstResponder];
    }
   
    else
        [_tfPassword resignFirstResponder];
    
    return YES;
}
//////
#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}

-(void) registerAPI{

    [AccountService registerUserWithFirstName:_tfFirstName.text
                                     lastName:_tfLastName.text
                                        email:_tfEmail.text
                                     password:_tfPassword.text
                                      success:^(id data) {
                                          [DataManager sharedManager].categoryArray = data;
                                          if ([DataManager sharedManager].receiverTemplate.isReceiver) {
                                              [self performSegueWithIdentifier:@"receiver" sender:nil];
                                              
                                          }
                                          else{
                                     [self performSegueWithIdentifier:@"register" sender:nil];
                                          }
                     } failure:^(NSError *error) {
                                 [self showAlertMessage:error.localizedDescription];

    }];
    

    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
