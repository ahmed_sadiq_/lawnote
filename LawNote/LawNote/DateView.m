//
//  DateView.m
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "DateView.h"
#import "DataManager.h"
#import "RCEasyTipView.h"
@implementation DateView 

{
    NSString *returnStr;
    NSUInteger currentSelectedIndex;
    Questions *question;
}
- (IBAction)btnChnageBGView:(id)sender {
    self.superview.layer.borderWidth = 1.5;
    self.superview.layer.borderColor = [[UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0] CGColor];
    
}

- (IBAction)btnInfo:(id)sender {
    
    RCEasyTipPreferences *preferences = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferences.drawing.backgroundColor = [UIColor  colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
    preferences.animating.showDuration = 1.5;
    preferences.animating.dismissDuration = 1.5;
    preferences.animating.dismissTransform = CGAffineTransformMakeTranslation(0, 100);
    preferences.animating.showInitialTransform = CGAffineTransformMakeTranslation(0, -100);
    preferences.shouldDismissOnTouchOutside = YES;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferences];
    tipView.text =question.question_description;
    tipView.delegate = self;
    [tipView showAnimated:YES forView:sender withinSuperView:nil];
    
    [self btnChnageBGView:nil];
    
    
}

- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
   NSString *dateSelect = [dateFormat stringFromDate:datePicker.date];

    dateFormat.dateStyle = NSDateFormatterMediumStyle;
    self.tfDate.text = [dateFormat stringFromDate:datePicker.date];
    [question setIsFilled:YES];
    [question setAnswer:dateSelect];
    [self.delegate selectedDate:question];
    [self btnChnageBGView:nil];


}
-(void) dismissPicker{
    if ([_tfDate.text isEqualToString:@"Select Date"]|| [_tfDate.text isEqualToString:@""]) {
        [self onDatePickerValueChanged:self.dataPicker];
    }
    [_tfDate resignFirstResponder];
}

-(void) setupView{
    
    
    if ([question.question_description isEqualToString:@""]||[question.question_description isEqual:nil]) {
        _btnInfo.hidden = YES;
        _infoImg.hidden = YES;
    }
    self.lblTitle.text = question.question_text;
    if (question.isFilled) {
        self.tfDate.text = question.answer;
    }
    
    self.dataPicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    [self.dataPicker setDatePickerMode:UIDatePickerModeDate];
    [self.dataPicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    self.tfDate.inputView = self.dataPicker;
    UIToolbar *toolbar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.frame.size.width,44)];
    toolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissPicker)];
    
    [toolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];
    self.tfDate.inputAccessoryView = toolbar;
//////////////// to adjust view movement on keyboard show/////////
    UITextView *utxt = [[UITextView alloc]init];
    [utxt setTag:0];
    [DataManager sharedManager].selectedView  = utxt;
}
+ (id) loadWithNibForDate:(Questions *)question andQuestionNumber:(NSString *)questionNumber{
    //int device = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)?0:1;
    
    
    DateView *view = (DateView *)[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([DateView class]) owner:nil options:nil][0];
    
    [view setFrame:[UIScreen mainScreen].bounds];
    
    view.alpha = 0.0;
    
    view->question = question;
    view.lblQuestionCount.text = questionNumber;
    [view setupView];
    return view;
}

-(void) show{
     self.alpha = 1.0;
        

}





@end
