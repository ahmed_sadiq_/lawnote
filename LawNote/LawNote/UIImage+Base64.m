//
//  UIImage+Base64.m
//  LawNote
//
//  Created by Samreen on 09/05/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "UIImage+Base64.h"

@implementation UIImage (Base64)

- (NSString *)base64String
{
    
    NSString *base64Str =[UIImagePNGRepresentation(self) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    return base64Str;
    
}
@end
