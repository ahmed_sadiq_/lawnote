//
//  Options.h
//  LawNote
//
//  Created by Samreen Noor on 25/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseEntity.h"

@interface Options : BaseEntity
@property (nonatomic, strong) NSString * option_id;
@property (nonatomic, strong) NSString * option_text;
@property (nonatomic, strong) NSString * option_value;
@property (nonatomic, strong) NSArray * questionArray;

@property (nonatomic) BOOL isSelected;





- (id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapOptionsFromArray:(NSArray *) arrlist;

@end
