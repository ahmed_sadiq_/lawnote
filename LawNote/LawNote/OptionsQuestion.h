//
//  OptionsQuestion.h
//  LawNote
//
//  Created by Samreen Noor on 05/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "BaseEntity.h"

@interface OptionsQuestion : BaseEntity

@property (nonatomic, strong) NSString * question_id;
@property (nonatomic, strong) NSString * question_text;
@property (nonatomic, strong) NSString * question_type;
@property (nonatomic, strong) NSString * answer;
@property (nonatomic, strong) NSString * option_id;
@property (nonatomic, strong) NSString * tag_id;


@property (nonatomic) BOOL isFilled;




- (id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapQuestionsFromArray:(NSArray *) arrlist;
@end
