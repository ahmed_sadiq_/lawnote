//
//  LoginVC.m
//  LawNote
//
//  Created by Apple Txlabz on 03/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "LoginVC.h"
#import "AccountService.h"
#import "HomeViewController.h"
#import "DataManager.h"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"
#import <Crashlytics/Crashlytics.h>

@interface LoginVC ()
{
    ForgotView *forgetView;
    
        UIGestureRecognizer *tapper;
        
    
}
@end

@implementation LoginVC
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
//    if ([segue.identifier isEqualToString:@"home" ]) {
//       HomeViewController  *homeVC = segue.destinationViewController;
//        homeVC.categoryArray = [DataManager sharedManager].categoryArray;
//        
//    }
    
}



#pragma mark - Controller View setup
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void) setUpView{
    
    //_scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 1000);

    _boxView.layer.masksToBounds = true;
    _boxView.layer.cornerRadius = 3.0;
    _btnLogin.layer.masksToBounds = true;
    _btnLogin.layer.cornerRadius = 3.0;
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapper];
    
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.scrollView endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];

    // Do any additional setup after loading thview.
}

- (IBAction)crashButtonTapped:(id)sender {
    [[Crashlytics sharedInstance] crash];
}

#pragma mark - IBAction
- (IBAction)btnSignUp:(id)sender {
    
    [self performSegueWithIdentifier:@"signUp" sender:nil];

}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}


- (IBAction)btnLogin:(id)sender {
    if ([self checktextFields]) {
        [AccountService loginUserWithEmail:_tfEmail.text password:_tfPassword.text success:^(id data) {
           
            [DataManager sharedManager].categoryArray = data;
            if ([DataManager sharedManager].receiverTemplate.isReceiver) {
                [self performSegueWithIdentifier:@"receiver" sender:nil];

            }
            else{
            [self performSegueWithIdentifier:@"home" sender:nil];
            }

        } failure:^(NSError *error) {
            [self showAlertMessage:error.localizedDescription];

        }];
    }

}
- (IBAction)btnForgotPassword:(id)sender {
   
    if (IS_IPAD) {
        
    if (forgetView == nil) {
         forgetView = [ForgotView loadWithNibForgotPassword];
    }
    [forgetView show];
    }

    
}
#pragma mark - TextField Methods


-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}


-(BOOL)checktextFields{
    
    BOOL willEnabled = NO;
    if( [_tfPassword.text length] >= 1 && [self emailValidate:_tfEmail.text] == YES ){
        willEnabled = YES;
    }
    if (![self emailValidate:_tfEmail.text]) {
        _emailBGImg.image = [UIImage imageNamed:@"txt_bgred"];

        //[self showAlertMessage:@"Please enter valid email"];
        return NO;
    }
    if (_tfPassword.text.length == 0 ||_tfEmail.text.length == 0) {
       // [self showAlertMessage:@"Please enter valid password"];
        _passwordBGImg.image = [UIImage imageNamed:@"txt_bgred"];

        return NO;
    }
  
    
    return willEnabled;
}



-(void)textFieldDidBeginEditing:(UITextField *)textField{

    [self.scrollView setContentOffset:CGPointMake(0, 60 * textField.tag) animated:YES];

}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self.scrollView setContentOffset:CGPointMake(0, 60 * textField.tag) animated:YES];

    
    
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self.scrollView setContentOffset:CGPointMake(0, 0 ) animated:YES];
    if (_tfPassword.text.length >0) {

        _passwordBGImg.image = [UIImage imageNamed:@"Rectangle 233"];
        
    }
else if ([_tfEmail isEqual:textField]) {
    if (![_tfEmail.text isEqualToString:@""]) {
        
        if (![self emailValidate:_tfEmail.text]) {
            _emailBGImg.image = [UIImage imageNamed:@"txt_bgred"];
            
        }
        else{
            _emailBGImg.image = [UIImage imageNamed:@"Rectangle 233"];
            
            
        }
    }
    
}

    

    
}
- (void) drawPlaceholderInRect:(CGRect)rect {
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
  
    if ([textField isEqual:_tfEmail]) {
        
        [_tfPassword becomeFirstResponder];
    }
  
    else
        [_tfPassword resignFirstResponder];
    
    return YES;
}

#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
