//
//  ProfileViewController.m
//  LawNote
//
//  Created by Samreen Noor on 07/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ProfileViewController.h"
#import "UIImageView+URL.h"
#import "AccountService.h"
#import "Constant.h"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"
@interface ProfileViewController ()<UIImagePickerControllerDelegate>
{
    UIGestureRecognizer *tapper;
    UITextField *checkField;
}
@end

@implementation ProfileViewController

-(void)setUpView{
    _userImg.layer.cornerRadius = 60 ;
    _userImg.layer.masksToBounds = YES;
    
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"];
    NSString *userLastName = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_lastName"];

    NSString *userEmial = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_email"];
    NSString *img = [[NSUserDefaults standardUserDefaults] objectForKey:@"profilePic"];
    [_userImg setImageWithURL:img];
    _tfFirstName.text =  userName;
    _tfLastName.text =userLastName;
    _btnSubmit.layer.cornerRadius = 5; // this value vary as per your desire
    _btnSubmit.clipsToBounds = YES;
    _btnResetPas.layer.cornerRadius = 5; // this value vary as per your desire
    _btnResetPas.clipsToBounds = YES;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    _ischnagePasswordView = [DataManager sharedManager].ischnagePasswordView;
    [self setUpView];
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
}
-(void)viewWillAppear:(BOOL)animated{
    if (_ischnagePasswordView) {
        _chnagePasswordView.hidden = NO;
    }
    else{
        _chnagePasswordView.hidden = YES;

    }
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [checkField resignFirstResponder];
    
}
- (IBAction)btnResetPssword:(id)sender {
    if ([_tfPassword.text  length]<1) {
        [self showAlertMessage:@"Please enter some valid password"];
       // [self.navigationController popViewControllerAnimated:YES];

    }
    else{
    [AccountService updateUserPasswordWithPassword:_tfPassword.text success:^(id data) {
        [self logOut];

        [self showAlertMessage:data[@"message"]];
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    }];
    }
}
-(void) logOut{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"logged_in"];
    if (IS_IPAD) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Ipad" bundle:nil];
        UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"navLogin"];
        [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
    }
    else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"navLogin"];
        [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_name"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_lastName"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_email"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"profilePic"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[DataManager sharedManager].receiverTemplate setIsReceiver:NO];
    [DataManager sharedManager].isCompleted= NO;

}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
- (IBAction)btnUpdateProfilePic:(id)sender {
    
    UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:@"Choose Media"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"Camera", @"Gallery", nil];
    
    [sheet showInView:self.view];
    
}

#pragma mark -  Camera Methods



-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        
        if (buttonIndex == 2) {
            return;
        }
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        
        if (buttonIndex == 1) {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
                imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        else
        {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    }];
    
}




-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSLog(@"image dictonary : %@", info);
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    
    if (image == nil){
        NSLog(@"Image with some error.");
    }
    
    
    
    //_profileImage.image  = [image imageWithScaledToSize:CGSizeMake(204, 204)];
    
    if (image) {
        
        [_userImg setImage:image];
        
    }
    
}


- (IBAction)btnSubmit:(id)sender {
    if ([_tfLastName.text length]>=1 &&[_tfFirstName.text length]>=1) {
        [AccountService updateUserProfileWithFirstName:_tfFirstName.text lastName:_tfLastName.text userImg:_userImg.image success:^(id data) {
            [self showAlertMessage:data[@"message"]];
        } failure:^(NSError *error) {
            [self showAlertMessage:error.localizedDescription];
        }];
    }
    else{
        [self showAlertMessage:@"Fill data carefully!"];
    }
}

#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITextField

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    checkField = textField;
    if (![textField isEqual:_tfPassword]) {
        [self animateTextField:nil up:YES];

    }
    
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (![textField isEqual:_tfPassword]) {

    [self animateTextField:nil up:NO];
    }
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
