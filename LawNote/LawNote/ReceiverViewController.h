//
//  ReceiverViewController.h
//  LawNote
//
//  Created by Samreen Noor on 10/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPSSignatureView.h"
#import "Templates.h"
@interface ReceiverViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (nonatomic)  BOOL isCompleted;
@property (nonatomic)  BOOL isFromReceiveTempVC;
@property (weak, nonatomic) IBOutlet UITableView *infoTabl;
@property (weak, nonatomic) IBOutlet UIButton *btnTabReceiverSign;

@property (weak, nonatomic) IBOutlet UIImageView *receiverImg;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet UIView *signatureSendView;
@property (weak, nonatomic) IBOutlet UILabel *lblAuthorPart;
@property (weak, nonatomic) IBOutlet UILabel *lblReceIverPart;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signViewHeightConstrain;

@property (weak, nonatomic) IBOutlet UIScrollView *previewScroll;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeightConstrain;
@property (weak, nonatomic) IBOutlet UIImageView *senderSignImg;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblSenderEmail;
@property (weak, nonatomic) IBOutlet PPSSignatureView *receiverSignView;
@property (weak, nonatomic) IBOutlet UILabel *lblRecName;
@property (weak, nonatomic) IBOutlet UILabel *lblRecEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSendSignature;
@property (strong, nonatomic) Templates *templ;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoTblTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recInfoBox;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *UperBoxConstrain;
@property (weak, nonatomic) IBOutlet UIView *signatureBoxView;

@end
