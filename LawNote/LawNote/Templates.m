//
//  Templates.m
//  LawNote
//
//  Created by Apple Txlabz on 15/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Templates.h"
#import "Questions.h"
@implementation Templates
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.category_id = [NSString stringWithFormat:@"%@",[self validStringForObject:responseData[@"category_id"]]];
        self.catImg = [self completeImageURL:responseData[@"category_icon"]];

        self.template_id = [self validStringForObject:responseData[@"template_id"]];
        self.template_title =[self validStringForObject:responseData[@"template_title"]];
        self.template_url =[self validStringForObject:responseData[@"template_url"]];
        self.template_url_view =[self validStringForObject:responseData[@"template_url_view"]];
        self.sender_id =[self validStringForObject:responseData[@"sender_id"]];
        self.sender_first_name =[self validStringForObject:responseData[@"sender_first_name"]];
        self.sender_last_name =[self validStringForObject:responseData[@"sender_last_name"]];
        self.sender_email =[self validStringForObject:responseData[@"sender_email"]];

        self.is_created_by_user =[self validStringForObject:responseData[@"is_created_by_user"]];
        self.receiver_email =[self validStringForObject:responseData[@"receiver_email"]];
        self.receiver_first_name =[self validStringForObject:responseData[@"receiver_first_name"]];
        self.receiver_last_name =[self validStringForObject:responseData[@"receiver_last_name"]];
        self.sender_signature_image =[self validStringForObject:responseData[@"sender_signature_image"]];
    self.receiver_signature_image =[self validStringForObject:responseData[@"receiver_signature_image"]];
        self.date_time =[self validStringForObject:responseData[@"date_time"]];

        self.template_description =[self validStringForObject:responseData[@"template_description"]];
        self.questionArray = [Questions mapQuestionsFromArray:responseData[@"questions"]];

        self.receiverInfoArray =[Receiver mapReceiverInfoFromArray:responseData[@"receivers"]];
        
    }
    
    return self;
}




+ (NSArray *)mapTemplatesFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Templates alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}

@end
