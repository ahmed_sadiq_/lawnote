//
//  MutipleOptionTextView.h
//  LawNote
//
//  Created by Samreen Noor on 05/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Questions.h"

@protocol MutipleOptionTextViewDelegate

@optional
-(void) filledRadioMultilevelBox:(Questions *)question;
//- delegate method;

@end

@interface MutipleOptionTextView : UIView<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (assign)id<MutipleOptionTextViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnInfo;
@property (weak, nonatomic) IBOutlet UIImageView *infoImg;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
+ (id) loadWithNibForRadioBox:(Questions *)question andQuestionNumber:(NSString *)questionNumber;
-(void) show;
@end
