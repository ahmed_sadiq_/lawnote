//
//  ManageViewController.m
//  LawNote
//
//  Created by Samreen Noor on 03/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ManageViewController.h"
#import "Constant.h"
#import "DocumentService.h"
#import "Templates.h"
#import "ManageCell.h"
#import "CreateDocumentVC.h"
#import "ManageHeaderCell.h"
#import "CreateYourOwnAgreementVC.h"
#import "ReceiverViewController.h"
#import "CustomIOSAlertView.h"
#import "UIView+AlertView.h"
#import "EditViewController.h"
#import "SVProgressHUD.h"
@interface ManageViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    int currentState;
    NSArray *documentArray;
    CGPoint _lastContentOffset;
    BOOL iscompletd;
    BOOL isPendingClicked;
    BOOL isDraftClicked;
    NSInteger selectedIndex;

}
@end

@implementation ManageViewController


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    Templates *temp = sender;
    
    if ([segue.identifier isEqualToString:@"createDoc" ]) {
        CreateDocumentVC  *createVC = segue.destinationViewController;
        createVC.questionArray =  temp.questionArray.mutableCopy;
        createVC.templete = temp;
        createVC.isfromDraft = YES;
        
    }
    else  if ([segue.identifier isEqualToString:@"edit" ]) {
        EditViewController  *editVC = segue.destinationViewController;
        editVC.questionArray =  temp.questionArray.mutableCopy;
        editVC.templete = temp;
        editVC.isfromDraft = YES;
        editVC.isFromPandig = isPendingClicked;
        
    }
    else if ([segue.identifier isEqualToString:@"completed" ]) {
            ReceiverViewController  *recVC = segue.destinationViewController;
            recVC.templ = temp;
            recVC.isCompleted = YES;
            

    }
    else  if ([segue.identifier isEqualToString:@"home" ]) {

    
    }
    else{
    
        CreateYourOwnAgreementVC  *createOwnVC = segue.destinationViewController;
        createOwnVC.templete = temp;
        createOwnVC.isfromDraft = YES;
        createOwnVC.isfromDraft = isDraftClicked;
        createOwnVC.isfromPending=isPendingClicked;
    }
    
    
}
#pragma  mark - setupView Methods

-(void) setHeaderView{
    [self.navigationController setNavigationBarHidden:YES animated:YES];

    self.headerView.layer.masksToBounds = NO;
    self.headerView.layer.shadowOffset = CGSizeMake(-3, 1);
    self.headerView.layer.shadowRadius = 3;
    self.headerView.layer.shadowOpacity = 0.4;
}

-(void) setupView{
    
    if(IS_IPHONE_6Plus)
    {
        
        self.tblDraftwidth.constant = 414;
        self.tblDraftHeight.constant= 736-100;
        
        
        
    }
    else if(IS_IPHONE_5)
    {
        //        self.tblDraft.frame = CGRectMake(self.tblDraft.frame.origin.x,  self.tblDraft.frame.origin.y, 320, 568);
          //  [_scrollView setContentSize:CGSizeMake(960, 568-100)];
        self.tblDraftwidth.constant = 320;
        self.tblDraftHeight.constant= 568-100;
        
    }
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    documentArray = [[NSArray alloc]init];
    _draftArray = [[NSMutableArray alloc]init];
    _pendingArray = [[NSArray alloc]init];
    _completedArray = [[NSArray alloc]init];
    [SVProgressHUD show];
    _tblDraft.hidden = YES;
    
    [self setHeaderView];
    [self setupRefreshControl];

    [self getManagedDocument];
    
    [DataManager sharedManager].isChageDoc = NO;
    [DataManager sharedManager].isFromDraftSend=NO;
    

}
-(void)viewWillAppear:(BOOL)animated{
       if ([DataManager sharedManager].isChageDoc) {
        [self getManagedDocument];

   }

}
-(void)viewDidLayoutSubviews{
    [self setupView];
//    if (_isPanding) {
//        currentState = 2;
//        CGRect frame = _scrollView.frame;
//        frame.origin.x = frame.size.width * 1;
//        frame.origin.y = 0;
//        [_scrollView scrollRectToVisible:frame animated:YES];
//        [_tblPanding reloadData];
//        
//    }

   // _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, 100);
    self.automaticallyAdjustsScrollViewInsets = NO;

}
#pragma  mark - IBButon Action
- (IBAction)btnDeleteDraft:(UIButton *)sender {
    selectedIndex = sender.tag;
    Templates *temp  =_draftArray[selectedIndex];
    [DocumentService deleteDocumentWithTemplateID:temp.template_id success:^(id data) {
        [_draftArray removeObjectAtIndex:selectedIndex];
        selectedIndex = _draftArray.count;
        [_tblDraft reloadData];
    } failure:^(NSError *error) {
        
    }];
}

- (IBAction)btnPanding:(id)sender {
    currentState = 2;
    CGRect frame = _scrollView.frame;
    frame.origin.x = frame.size.width * 1;
    frame.origin.y = 0;
    [_scrollView scrollRectToVisible:frame animated:YES];
   
}
- (IBAction)btnDraft:(id)sender {
    
    CGRect frame = _scrollView.frame;
    frame.origin.x = frame.size.width * 0;
    frame.origin.y = 0;
    currentState = 0;
    
    [_scrollView scrollRectToVisible:frame animated:YES];

   

}
- (IBAction)btnCompleted:(id)sender {
   
    currentState = 3;
    CGRect frame = _scrollView.frame;
    frame.origin.x = frame.size.width * 2;
    frame.origin.y = 0;
    [_scrollView scrollRectToVisible:frame animated:YES];
    
 

}
#pragma  mark - Scroller delagates

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
}


- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
     if(scrollView.tag==1){
        int page = scrollView.contentOffset.x / scrollView.frame.size.width;
        
        if(page == 0){
            [_btnDraft setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_btnpanding setTitleColor:[UIColor colorWithRed:221.0/256.0 green:221.0/256.0 blue:221.0/256.0 alpha:1] forState:UIControlStateNormal];
            [_btnCompleted setTitleColor:[UIColor  colorWithRed:221.0/256.0 green:221.0/256.0 blue:221.0/256.0 alpha:1] forState:UIControlStateNormal];
            currentState = 0;
            _draftLine.hidden      = false;
            _padingLine.hidden   = true;
            _completedLine.hidden  = true;
            // documentArray = _draftArray;
            [_tblDraft reloadData];
        }
        else if (page == 1) {
            [_btnpanding setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_btnCompleted setTitleColor:[UIColor  colorWithRed:221.0/256.0 green:221.0/256.0 blue:221.0/256.0 alpha:1] forState:UIControlStateNormal];
            [_btnDraft setTitleColor:[UIColor  colorWithRed:221.0/256.0 green:221.0/256.0 blue:221.0/256.0 alpha:1] forState:UIControlStateNormal];
            currentState = 2;
            _padingLine.hidden      = false;
            _draftLine.hidden   = true;
            _completedLine.hidden  = true;
            // documentArray = _pendingArray;
            [_tblPanding reloadData];
            _tblPanding.hidden = NO;
        }
        else {
            currentState = 3;
            [_btnCompleted setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [_btnDraft setTitleColor:[UIColor  colorWithRed:221.0/256.0 green:221.0/256.0 blue:221.0/256.0 alpha:1] forState:UIControlStateNormal];
            [_btnpanding setTitleColor:[UIColor  colorWithRed:221.0/256.0 green:221.0/256.0 blue:221.0/256.0 alpha:1] forState:UIControlStateNormal];
            _draftLine.hidden      = true;
            _padingLine.hidden   = true;
            _completedLine.hidden  = false;
            //documentArray = _completedArray;
            [_tblCompleted reloadData];
            _tblCompleted.hidden = NO;
            
        }
        
    }
    
    
}



- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
   }

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
   }

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate
{
    //    if(scrollView.tag ==10) {
    //        if (!decelerate) {
    //            [self stoppedScrolling];
    //        }
    //    }
    //
}
- (void)stoppedScrolling
{
  
    //[self getHomeContent];
}

#pragma  mark - tableview Methods

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    static NSString *HeaderCellIdentifier = @"Header";
//    ManageHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:HeaderCellIdentifier];
//
//    
//    if (cell == nil) {
//        cell = [[ManageHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HeaderCellIdentifier];
//    }
//    
//
//    return cell;
    ManageHeaderCell  *header;
    if(tableView.tag==10&&_draftArray.count==0){
    header = [[[NSBundle mainBundle] loadNibNamed:@"ManageHeaderCell" owner:self options:nil] objectAtIndex:0];
    }
    else if(tableView.tag==20&&_pendingArray.count==0){
        header = [[[NSBundle mainBundle] loadNibNamed:@"ManageHeaderCell" owner:self options:nil] objectAtIndex:0];
    }
    else if(tableView.tag==30&&_completedArray.count==0){
        header = [[[NSBundle mainBundle] loadNibNamed:@"ManageHeaderCell" owner:self options:nil] objectAtIndex:0];
    }
     [header.btnNewAggrement addTarget:self action:@selector(createNewDoc:) forControlEvents:UIControlEventTouchUpInside];
    return header;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(tableView.tag==10&&_draftArray.count==0){
        return 150;

    }
    else if(tableView.tag==20&&_pendingArray.count==0){
        return 150;

    }
    else if(tableView.tag==30&&_completedArray.count==0){
        return 150;

    }

    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPAD) {
        return 119;
        
    }
    else
        return 80;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==10) {
        return _draftArray.count;
    }
    else if (tableView.tag==20){
        return _pendingArray.count;

    }
    else{
    return _completedArray.count;
    }
   // return documentArray.count;
}






- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Templates *temp;// = documentArray[indexPath.row];

    ManageCell   *cell;
    if (tableView.tag==10) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"draftCell"];
        temp = _draftArray[indexPath.row];
        UILongPressGestureRecognizer* longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
        [cell addGestureRecognizer:longPressRecognizer];
        [longPressRecognizer.view setTag:indexPath.row];
        if (selectedIndex == indexPath.row) {
           // cell.btnDelete.hidden = NO;
        }
        else{
            cell.btnDelete.hidden = YES;

        }
        cell.btnDelete.tag = indexPath.row;

    }
    else if (tableView.tag==20){
        cell = [tableView dequeueReusableCellWithIdentifier:@"pendingCell"];

        temp = _pendingArray[indexPath.row];

    }
    else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"completedCell"];

        temp = _completedArray[indexPath.row];

    }
    
    

    
    
    cell.lblName.text = [temp.template_title uppercaseString];
    cell.lblDescription.text = temp.template_description;
    cell.lblDate.text = temp.date_time;
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Templates *temp;
    
    if (tableView.tag==10) {
        temp = _draftArray[indexPath.row];
        iscompletd = NO;
        isDraftClicked=YES;
        isPendingClicked=NO;

    }
    else if (tableView.tag==20){
        
        temp = _pendingArray[indexPath.row];
        iscompletd = NO;
        isDraftClicked=NO;
        isPendingClicked=YES;
    }
    else{
        
        temp = _completedArray[indexPath.row];
        iscompletd = YES;
    }
    if (iscompletd) {
        [self performSegueWithIdentifier:@"completed" sender:temp];

    }
    else{
    if ([temp.is_created_by_user isEqualToString:@"1"]) {
        [self performSegueWithIdentifier:@"CreatedOwnPreview" sender:temp];

    }
    else{
    
  //  [self performSegueWithIdentifier:@"createDoc" sender:temp];
        [self performSegueWithIdentifier:@"edit" sender:temp];

    }
    }
    
    
    
}
-(void)onLongPress:(UILongPressGestureRecognizer*)pGesture
{
    selectedIndex = pGesture.view.tag;

    if (pGesture.state == UIGestureRecognizerStateEnded)
    {

        [_tblDraft reloadData];
    }
}

-(void) getManagedDocument{

    int page = _scrollView.contentOffset.x / _scrollView.frame.size.width;

    [DocumentService getManagedDocumentWithsuccess:^(id data) {
        
        NSLog(@"%@",data);
        _completedArray = [Templates mapTemplatesFromArray:data[@"completed"]];
        _draftArray = [Templates mapTemplatesFromArray:data[@"drafts"]].mutableCopy;
        _pendingArray = [Templates mapTemplatesFromArray:data[@"pending"]];
        //documentArray = _draftArray;
        if(page == 0){
        [_tblDraft reloadData];
        _tblDraft.hidden = NO;
        }
        else if(page == 1){
            //documentArray = _pendingArray;
        [_tblPanding reloadData];
        _tblPanding.hidden = NO;
        }
        else{
           // documentArray = _completedArray;
        [_tblCompleted reloadData];
        _tblCompleted.hidden   = NO;
        }
        if ( [DataManager sharedManager].isFromDraftSend) {
            _isPanding=YES;
        }
       

        if (_isPanding) {
            [self btnPanding:nil];
            
        }
        if ([DataManager sharedManager].isCompleted) {
            [self btnCompleted:nil];
        }

        [SVProgressHUD dismiss];
        selectedIndex = _draftArray.count;
        
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    }];
}

#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    CustomIOSAlertView *customAlertView = [[CustomIOSAlertView alloc] init];
    [customAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK",nil]];
    [customAlertView setContainerView:[customAlertView createDemoView:@"LawNote!" message:message]];
    // You may use a Block, rather than a delegate.
    [customAlertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if(buttonIndex == 0){
            
        }
        
        [customAlertView close];
    }];
    [customAlertView setUseMotionEffects:true];
    // And launch the dialog
    [customAlertView show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)createNewDoc:(UIButton*)sender{
    [DataManager sharedManager].isChangeInReceived = NO;

    [self performSegueWithIdentifier:@"home" sender:nil];

}

#pragma mark - Helper Methods

- (void)setupRefreshControl
{
              ///////////  ####### Refresh Draft ##########

    self.refreshDraftControl = [[UIRefreshControl alloc] init];
    self.refreshDraftControl.backgroundColor = [UIColor clearColor];
    self.refreshDraftControl.tintColor       = [UIColor blackColor];
    [self.refreshDraftControl addTarget:self action:@selector(refreshDraftTemplete:) forControlEvents:UIControlEventValueChanged];
    [_tblDraft addSubview:_refreshDraftControl];
             ///////////  ####### Refresh Pending ##########

    self.refreshPendingControl = [[UIRefreshControl alloc] init];
    self.refreshPendingControl.backgroundColor = [UIColor clearColor];
    self.refreshPendingControl.tintColor       = [UIColor blackColor];
    [self.refreshPendingControl addTarget:self action:@selector(refreshPendingTemplete:) forControlEvents:UIControlEventValueChanged];
    [_tblPanding addSubview:_refreshPendingControl];
    
       ///////////  ####### Refresh Completed ##########

    self.refreshCompletedControl = [[UIRefreshControl alloc] init];
    self.refreshCompletedControl.backgroundColor = [UIColor clearColor];
    self.refreshCompletedControl.tintColor       = [UIColor blackColor];
    [self.refreshCompletedControl addTarget:self action:@selector(refreshCompletedTemplete:) forControlEvents:UIControlEventValueChanged];
    [_tblCompleted addSubview:_refreshCompletedControl];
}

- (void)refreshDraftTemplete:(id)sender
{
    [self getManagedRefreshDraftDocument];
    
    
}
- (void)refreshPendingTemplete:(id)sender
{
    [self getManagedRefreshPendingDocument];
    
    
}- (void)refreshCompletedTemplete:(id)sender
{
    [self getManagedRefreshCompletedDocument];
    
    
}
-(void) getManagedRefreshDraftDocument{
    
    
    
    [DocumentService getManagedDocumentWithsuccess:^(id data) {
        [self.refreshDraftControl endRefreshing];

    _draftArray = [Templates mapTemplatesFromArray:data[@"drafts"]];
            [_tblDraft reloadData];
            _tblDraft.hidden = NO;
        
        
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    }];
}
-(void) getManagedRefreshPendingDocument{
    
    
    
    [DocumentService getManagedDocumentWithsuccess:^(id data) {
        [self.refreshPendingControl endRefreshing];

        _pendingArray = [Templates mapTemplatesFromArray:data[@"pending"]];
        [_tblPanding reloadData];
        _tblPanding.hidden = NO;
        
        
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    }];
}
-(void) getManagedRefreshCompletedDocument{
    
    
    
    [DocumentService getManagedDocumentWithsuccess:^(id data) {
        [self.refreshCompletedControl endRefreshing];

        _completedArray = [Templates mapTemplatesFromArray:data[@"completed"]];
      
        [_tblCompleted reloadData];
        _tblCompleted.hidden = NO;
        
        
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
