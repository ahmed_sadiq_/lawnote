//
//  EditText.m
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "EditText.h"
#import "RCEasyTipView.h"
#import "DataManager.h"
@implementation EditText
{
    NSString *returnStr;
    Questions *question;
    
}
- (IBAction)btnInfo:(id)sender {
    
        RCEasyTipPreferences *preferences = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
        preferences.drawing.backgroundColor = [UIColor  colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
        preferences.animating.showDuration = 1.5;
        preferences.animating.dismissDuration = 1.5;
        preferences.animating.dismissTransform = CGAffineTransformMakeTranslation(0, 100);
        preferences.animating.showInitialTransform = CGAffineTransformMakeTranslation(0, -100);
        preferences.shouldDismissOnTouchOutside = YES;
        
        RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferences];
    tipView.text =question.question_description;

        tipView.delegate = self;
        [tipView showAnimated:YES forView:sender withinSuperView:nil];
        
    [self btnHideKeyboard:nil];

    
    
}

- (IBAction)btnHideKeyboard:(id)sender {
//    [UIView animateWithDuration:0.5 animations:^{
//        _boxViewTopConstrain.constant = 20 ;
//    } completion:^(BOOL finished) {
//        
//    }];
//    [_editTxtView resignFirstResponder];
    
    self.superview.layer.borderWidth = 1.5;
    self.superview.layer.borderColor = [[UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0] CGColor];

}

-(void) setupView{

    CALayer *viewLayer = self.editTxtView.layer;
    [viewLayer setCornerRadius:5.0];
    [viewLayer setBorderWidth:1];
    viewLayer.borderColor=[[UIColor lightGrayColor] CGColor];
    
    _lblQuestion.text = question.question_text;
    _editTxtView.text = question.answer;
    [DataManager sharedManager].selectedView = _editTxtView;
    if ([question.question_description isEqualToString:@""]||[question.question_description isEqual:nil]) {
        _btnInfo.hidden = YES;
        _infoImg.hidden = YES;
    }
}
+ (id) loadWithNibForEditText:(Questions *)question andQuestionNumber:(NSString *)questionNumber
{
   // int device = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)?0:1;
    
    
    EditText *view = (EditText *)[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([EditText class]) owner:nil options:nil][0];
    
    [view setFrame:[UIScreen mainScreen].bounds];
    
    view.alpha = 0.0;
    
    view->question = question;
   view.lblCharacterCount.text = questionNumber;
    [view setupView];
    return view;
}

-(void) show{
   
        self.alpha = 1.0;

}



#pragma mark - Text VIEW Delegate Methods
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self btnHideKeyboard:nil];

    if (IS_IPHONE_6Plus || IS_IPAD) {
        
    }
    else if (IS_IPHONE_5){
        [UIView animateWithDuration:0.5 animations:^{
            _boxViewTopConstrain.constant = -110;
        } completion:^(BOOL finished) {
            
        }];
    }
    else{
    /*CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        textView.frame = newFrame;
        [UIView animateWithDuration:0.5 animations:^{
            
            _boxViewTopConstrain.constant = 20 - newFrame.size.height;
        } completion:^(BOOL finished) {
            
        }];*/
    }
    }
-(void)textViewDidChange:(UITextView *)textView
{
    if (IS_IPHONE_6Plus || IS_IPAD) {
        
    }
    else if (IS_IPHONE_5){
    
    }
    else{
    
  /*  CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    if (textView.frame.size.height <150) {
        
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
        [UIView animateWithDuration:0.5 animations:^{
        
            _boxViewTopConstrain.constant = 20 - newFrame.size.height;
        } completion:^(BOOL finished) {
            
        }];
    }*/
    }
}
-(void)textViewDidChangeSelection:(UITextView *)textView
{
  
    /*YOUR CODE HERE*/
}
-(void)textViewDidEndEditing:(UITextView *)textView
{  if (IS_IPHONE_6Plus || IS_IPAD) {
    
}

else{
    _boxViewTopConstrain.constant = 20 ;

}
    [textView resignFirstResponder];
    if (![textView.text isEqualToString:@""]) {
    [question setAnswer:_editTxtView.text];
        [question setIsFilled:YES];
    [self.delegate fillQuestion:question];

    }
    else{
        [question setAnswer:_editTxtView.text];
        [question setIsFilled:NO];
        [self.delegate fillQuestion:question];

    }

    /*YOUR CODE HERE*/
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        
        [UIView animateWithDuration:0.5 animations:^{
            if (IS_IPHONE_6Plus || IS_IPAD) {
                
            }
            else
            _boxViewTopConstrain.constant = 20 ;
        } completion:^(BOOL finished) {
            
        }];
        return NO;
    }
    
    return YES;
}

- (void)didShowTip:(RCEasyTipView *)tipView {
}


@end
