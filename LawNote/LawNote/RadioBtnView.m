
//
//  RadioBtnView.m
//  LawNote
//
//  Created by Samreen Noor on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "RadioBtnView.h"
#import "RadioCell.h"
#import "Options.h"
#import "RCEasyTipView.h"
@implementation RadioBtnView
{
    NSString *returnStr;
    NSUInteger currentSelectedIndex;
    Questions *question;
}
- (IBAction)btnChnageBGview:(id)sender {
    
    self.superview.layer.borderWidth = 1.5;
    self.superview.layer.borderColor = [[UIColor colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0] CGColor];
}

- (IBAction)btnInfo:(id)sender {
    
    RCEasyTipPreferences *preferences = [[RCEasyTipPreferences alloc] initWithDefaultPreferences];
    preferences.drawing.backgroundColor = [UIColor  colorWithRed:36.0/255.0 green:161.0/255.0 blue:199.0/255.0 alpha:1.0];
    preferences.animating.showDuration = 1.5;
    preferences.animating.dismissDuration = 1.5;
    preferences.animating.dismissTransform = CGAffineTransformMakeTranslation(0, 100);
    preferences.animating.showInitialTransform = CGAffineTransformMakeTranslation(0, -100);
    preferences.shouldDismissOnTouchOutside = YES;
    
    RCEasyTipView *tipView = [[RCEasyTipView alloc] initWithPreferences:preferences];
    tipView.text = question.question_description;
    tipView.delegate = self;
    [tipView showAnimated:YES forView:sender withinSuperView:nil];
    
     [self btnChnageBGview:nil];
    
    
}

-(void) setupView{
    self.lblTitle.text = question.question_text;
    if ([question.question_description isEqualToString:@""]||[question.question_description isEqual:nil]) {
        _btnInfo.hidden = YES;
        _infoImg.hidden = YES;
    }
}
+ (id) loadWithNibForRadioBox:(Questions *)question andQuestionNumber:(NSString *)questionNumber{
   // int device = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)?0:1;
    
    
    RadioBtnView *view = (RadioBtnView *)[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([RadioBtnView class]) owner:nil options:nil][0];
    
    [view setFrame:[UIScreen mainScreen].bounds];
    
    view.alpha = 0.0;
    
    view->question = question;
    view.lblQuestionCount.text = questionNumber;

    [view setupView];
    return view;
}

-(void) show{
   
    
        self.alpha = 1.0;
        
 
}



#pragma mark - TableView Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 45;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return question.optionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Options *option = [question.optionArray objectAtIndex:indexPath.row];
    
    RadioCell * cell = (RadioCell *)[_tblView dequeueReusableCellWithIdentifier:@"RadioCell"];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"RadioCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    cell.lblOption.text = option.option_text;
    [cell.btnRadio addTarget:self action:@selector(selectedOption:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnRadio.tag = indexPath.row;
    if (option.isSelected) {
        [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"RadioSect"] forState:UIControlStateNormal];

    }
    else{
        [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"Radio"] forState:UIControlStateNormal];

    }
    
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void) selectedOption : (UIButton *) sender{
         [self btnChnageBGview:nil];
    currentSelectedIndex = sender.tag;
    Options *option = [question.optionArray objectAtIndex:currentSelectedIndex];
    option.isSelected = YES;
    for (Options *o in question.optionArray) {
        if (! [o.option_id isEqualToString:option.option_id]) {
            o.isSelected = NO;

        }
    }
    [_tblView reloadData];
    [question setIsFilled:YES];
    [question setAnswer:option.option_text];
    [question setOptionAnswerValue:option.option_value];

    [question setOptionAnswerId:option.option_id];
    [self.delegate filledRadioBox:question];
    
}




@end
